<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>Cập nhật khóa học - System Admin</title>
<jsp:include page="/WEB-INF/view/admin/include/css.jsp" />
<jsp:include page="/WEB-INF/view/admin/include/js.jsp" />
<link rel="stylesheet" href="<c:url value="admin/css/checkbox.css"/>">
<link rel="stylesheet" href="admin/css/bootstrap-select.min.css">
<link rel="stylesheet" href="admin/css/ajax-bootstrap-select.min.css">
<link rel="stylesheet" href="admin/css/trash.css">
<link rel="stylesheet" href="admin/css/validetta.css">
<script type="text/javascript" src="admin/js/bootstrap-select.min.js"></script>
<script type="text/javascript"
	src="admin/js/ajax-bootstrap-select.min.js"></script>
<script type="text/javascript" src="admin/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="admin/js/validetta.js"></script>
</head>
<body>
	<div id="wrapper">
		<%@include file="/WEB-INF/view/admin/include/header.jsp"%>
		<!-- Page Content -->
		<div id="page-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						<h1 class="page-header">Cập nhật khóa học</h1>
					</div>
				</div>
				<div class="row">
					<form action="updateCourse.ad" method="post"
						enctype="multipart/form-data" id="update-course-myform">
						<div class="col-lg-6" style="overflow-y: auto; height: 500px">
							<h4>Thông tin khóa học</h4>
							<input type="hidden" name="courseId" value="${course.courseId}">
							<div class="form-group">
								<label>Tên khóa học :</label> <input type="text"
									class="form-control" data-validetta="required,maxLength[255]"
									name="title" value='<c:out value="${course.title}"></c:out>'>
							</div>
							<div class="form-group">
								<label>Mô tả :</label>
								<textarea rows="3" name="description" class="form-control"><c:out
										value="${course.description}"></c:out></textarea>
							</div>
							<div class="form-group">
								<label>Thời gian học (ngày) :</label> <input type="number"
									class="form-control" name="time-study"
									data-validetta="required,regExp[numberInteger]"
									value='<c:out value="${course.timeStudy}"></c:out>'>
							</div>
							<div class="form-group">
								<label>Giáo viên :</label> <select
									class="selectpicker form-control with-ajax"
									data-validetta="required" data-live-search="true"
									name="teacher" id="teacher"><option
										value="${course.userTeacher}">${course.userTeacher}</option></select>
							</div>
							<div class="form-group">
								<label>Giá (đồng):</label> <input type="number"
									class="form-control" name="price"
									data-validetta="required,regExp[numberInteger]"
									value='<c:out value="${course.price}"></c:out>'>
							</div>
							<div class="form-group">
								<div class="checkbox checbox-switch switch-primary">
									<label><b>Giảm giá : </b><input type="checkbox"
										name="check-sale" value="sale"
										<c:if test="${course.sale > 0}">checked</c:if> /> <span></span>
									</label>
								</div>
							</div>
							<div id="sale" class="sale"
								<c:if test="${course.sale <= 0}">style="display: none"</c:if>>
								<div class="form-group">
									<label> Giảm (%):</label> <input type="number"
										class="form-control" name="sale-course" id="sale-course"
										value="<fmt:formatNumber value="${course.sale}" type="number" pattern="#"/>">
								</div>
								<div class="form-group">
									<label> Ngày bắt đầu giảm giá :</label> <input
										class="form-control" type="date"
										value="<fmt:formatDate
											pattern="yyyy-MM-dd" value="${course.startSale}" />"
										name="start-date-sale" id="start-date-sale">
								</div>
								<div class="form-group">
									<label> Ngày kết thúc giảm giá :</label> <input
										class="form-control" type="date"
										value="<fmt:formatDate
											pattern="yyyy-MM-dd" value="${course.endSale}" />"
										name="end-date-sale" id="end-date-sale">
								</div>
							</div>
						</div>
						<div class="col-lg-6" style="overflow-y: auto; height: 500px">
							<h4>Danh sách bài học</h4>
							<div class="table-responsive">
								<table class="table table-hover">
									<thead>
										<tr>
											<th>Tên</th>
											<th>File <span
												style="float: right; padding-right: 10px; font-size: 20px; color: green;"
												class="fa fa-plus add-lession"></span></th>
										</tr>
									</thead>
									<tbody id="tb-lession">
										<c:if test="${course.lessions != null}">
											<c:forEach var="lession" items="${course.lessions}"
												varStatus="status">
												<tr>
													<td><input type="hidden" name="lessionId"
														value="${lession.lessionId}"><input type="text"
														name="title-lession" disabled
														class="form-control ${status.index + 1} title-lession"
														value='<c:out value="${lession.title}"></c:out>'></td>
													<td><input type="file" name="fileUpload"
														class="form-control fileUpload"
														accept="video/mp4,video/x-m4v,video/*"
														indexRow="${status.index + 1}"
														value='<c:out value="${lession.url}"></c:out>'></td>
													<td><i class="fa fa-trash" aria-hidden="true"></i></td>
												</tr>
											</c:forEach>
										</c:if>
									</tbody>
								</table>
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-lg-12">
								<input type="submit" class="btn btn-success"
									value="Cập nhật khóa học"> <input type="reset"
									class="btn btn-primary" value="Hủy">
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="admin/js/update-course.js"></script>
</body>
</html>
