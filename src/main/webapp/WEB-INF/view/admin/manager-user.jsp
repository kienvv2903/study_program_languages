<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Quản lý người dùng - System Admin</title>
<jsp:include page="/WEB-INF/view/admin/include/css.jsp" />
<link href="<c:url value="admin/css/manage-user.css"/>" rel="stylesheet">
<jsp:include page="/WEB-INF/view/admin/include/js.jsp" />
<%@ page import="util.Constant"%>
<style type="text/css">
.boder-error {
	border: 1px solid red;
}

span.error-money {
	display: none;
	color: red;
	font-size: 14px;
}

.modal {
	text-align: center;
	overflow: auto;
}

@media screen and (min-width: 768px) {
	.modal:before {
		display: inline-block;
		vertical-align: middle;
		content: " ";
		height: 100%;
	}
}

.modal-dialog {
	display: inline-block;
	text-align: left;
	vertical-align: middle;
}
</style>
</head>
<body>
	<div id="wrapper">
		<%@include file="/WEB-INF/view/admin/include/header.jsp"%>
		<div id="page-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						<form action="managerUser.ad" method="get">
							<input type="hidden" name="type" value="search">
							<h1 class="page-header">Quản lý người dùng</h1>
							<div class="row">
								<div class="col-lg-6">
									<label> Tổng kết quả : <c:out value="${countUser}" />
									</label>
								</div>
								<div class="col-lg-6">
									<div class="input-group">
										<input type="text" class="form-control" name="keySearch"
											placeholder="Tìm kiếm người dùng (tên tài khoản, email)"
											value='<c:out value="${keySearch}"/>'> <span
											class="input-group-btn">
											<button class="btn btn-default" type="submit">
												<i class="fa fa-search"></i>
											</button>
										</span>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="form-group col-lg-3">
									<label>Hiển thị số người dùng/bản ghi </label> <select
										name="limit" class="form-control">
										<option value="10" <c:if test="${limit == 10}">selected</c:if>>10</option>
										<option value="25" <c:if test="${limit == 25}">selected</c:if>>25</option>
										<option value="50" <c:if test="${limit == 50}">selected</c:if>>50</option>
									</select>
								</div>
							</div>
						</form>
						<div class="panel panel-default">
							<div class="panel-heading">Danh sách người dùng</div>
							<!-- /.panel-heading -->
							<div class="panel-body">
								<c:choose>
									<c:when test="${error == false}">
										<c:choose>
											<c:when test="${listUsers != null}">
												<c:choose>
													<c:when test="${listUsers.size() > 0}">
														<div class="table-responsive">
															<table class="table table-hover">
																<thead>
																	<tr>
																		<th>STT</th>
																		<th>Tài khoản</th>
																		<th>Email</th>
																		<th>Số dư</th>
																		<th>Tính năng</th>
																	</tr>
																</thead>
																<tbody>
																	<c:if test="${listUsers != null}"></c:if>
																	<c:forEach var="user" items="${listUsers}"
																		varStatus="status">
																		<tr>
																			<td>${(currentPage - 1 )*limit + status.index + 1}</td>
																			<td><img class="img-circle img-user"
																				src='<c:out value="${user.urlImg}"/>'> <span><c:out
																						value="${user.username}" /></span></td>
																			<td><c:out value="${user.email}" /></td>
																			<td><c:out value="${user.balance}" /></td>
																			<td><a><i class="fa fa-plus add-blance"
																					title="Số dư tài khoản" username="${user.username}"></i></a>
																				<a><i class="fa fa-info-circle edit-user"
																					title="Thông tin chi tiết tài khoản"
																					username="${user.username}"></i></a></td>
																		</tr>
																	</c:forEach>
																</tbody>
															</table>
														</div>
													</c:when>
													<c:otherwise>
														Không có người dùng phù hợp
													</c:otherwise>
												</c:choose>

											</c:when>
											<c:otherwise>
											</c:otherwise>
										</c:choose>
									</c:when>
									<c:otherwise>
										Trang hiển thị không tồn tại
									</c:otherwise>
								</c:choose>
								<!-- /.table-responsive -->
							</div>
							<!-- /.panel-body -->
						</div>
						<div class="paging text-center">
							<nav>
								<ul class="pagination">
									<c:if test="${listPagings != null}">
										<c:forEach var="page" items="${listPagings}"
											varStatus="status">
											<c:if test="${status.index == 0 && currentPage >= page}">
												<c:if test="${listPagings.size() > 1}">
													<li class="page-item"><a class="page-link"
														href="managerUser.ad?type=paging&page=1">Đầu</a></li>
												</c:if>
												<c:if test="${currentPage > 1}">
													<li class="page-item"><a class="page-link"
														href="managerUser.ad?type=paging&page=${currentPage-1}"
														aria-label="Previous"> <span aria-hidden="true">&laquo;</span>
															<span class="sr-only">Previous</span></a></li>
												</c:if>
											</c:if>
											<li
												class="page-item <c:if test="${currentPage == page}">active</c:if>"><a
												class="page-link"
												href="managerUser.ad?type=paging&page=${page}"><c:out
														value="${page}"></c:out></a></li>
											<c:if
												test="${status.index == listPagings.size() - 1 && page < totalPage}">
												<li class="page-item"><a class="page-link"
													href="managerUser.ad?type=paging&page=${currentPage+1}"
													aria-label="Next"> <span aria-hidden="true">&raquo;</span>
														<span class="sr-only">Next</span></a></li>
												<li class="page-item"><a class="page-link"
													href="managerUser.ad?type=paging&page=${totalPage}">Cuối</a></li>
											</c:if>
										</c:forEach>
									</c:if>
								</ul>
							</nav>
						</div>
						<!-- /.panel -->
					</div>
				</div>
			</div>
			<!-- /.container-fluid -->
		</div>
		<!-- /#page-wrapper -->
	</div>
	<!-- /#wrapper -->

	<div class="modal" id="addBalance">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<a href="#close" rel="modal:close"><button type="button"
							class="close" data-dismiss="modal">&times;</button></a>
					<h4 class="modal-title">Cập nhật số dư tài khoản</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Tên tài khoản : </label> <input type="text" id="us"
							class="form-control" disabled="disabled" value="">
					</div>
					<div class="form-group">
						<label>Nạp :</label> <input type="number" id="recharge"
							class="form-control" value="0"> <span class="error-money">*
							Lỗi định dạng</span>
					</div>
					<div class="text-center">
						<img style="display: none" class="loading" alt="Loading"
							src='<c:out value="user/img/gif/ajax-loader.gif"></c:out>'>
					</div>
				</div>
				<div class="modal-footer">
					<button disabled type="button" class="btn btn-success"
						id="recharge-btn"
						onclick="recharge($('#us').val(),$('#recharge').val())">Nạp</button>
					<a href="#close" rel="modal:close"><button type="button"
							class="btn btn-default">Close</button></a>
				</div>
			</div>
		</div>
	</div>

	<div class="modal" id="loadingGif">
		<img style="display: none" class="loadingGif"
			src='<c:out value="user/img/gif/ajax-loader.gif"></c:out>'>
	</div>

	<div class="modal" id="inforUser"></div>
</body>
<script type="text/javascript">
	$("#recharge").change(function() {
		var money = $("#recharge").val();
		var regex = new RegExp('^\\d+$');
		var regexZero = new RegExp('^0+$');
		if (!regex.test(money) || regexZero.test(money)) {
			$("#recharge").addClass('boder-error');
			$("span.error-money").show();
			$("#recharge-btn").attr("disabled", true);
		} else {
			$("#recharge").removeClass('boder-error');
			$("span.error-money").hide();
			$("#recharge-btn").attr("disabled", false);
		}
	});

	function recharge(username, balance) {
		var recharge = confirm("Bạn có muốn nạp " + Number(balance)
				+ " đ vào tài khoản " + username + " không ?");
		if (recharge) {
			$.ajax({
				type : 'POST',
				url : 'updateBalance.ad',
				data : {
					username : $("#us").val(),
					balance : $("#recharge").val()
				},
				beforeSend : function() {
					$('.loading').show();
				},
				complete : function() {
					setTimeout(function() {
						$(".loading").hide();
					}, 500);
				},
				success : function(data) {
					switch (data) {
					case "updateBalanceSuccess":
						setTimeout(function() {
							$.modal.close();
						}, 500);
						setTimeout(function() {
							swal({
								title : "Nạp tiền !",
								text : "Đã nạp thành công " + Number(balance)
										+ " đ vào tài khoản " + username,
								icon : "success",
								button : false,
							});
						}, 600);
						updateBalance(username, balance);
						break;
					default:
						window.location.href = "errorPage";
						break;
					}
				}
			});
		}
		return false;
	}

	$(".add-blance").click(function() {
		$("#addBalance").modal({
			escapeClose : false,
			clickClose : false,
			showClose : false,
			fadeDuration : 300,
			fadeDelay : 0.50
		});
		$("#us").val($(this).attr("username"));
		return false;
	});

	$(".edit-user").click(function() {
		$("#loadingGif").modal({
			escapeClose : false,
			clickClose : false,
			showClose : false,
			fadeDuration : 300,
			fadeDelay : 0.50
		});
		$.ajax({
			type : 'POST',
			url : 'inforUser.ad',
			data : {
				username : $(this).attr("username")
			},
			beforeSend : function() {
				$(".loadingGif").show();
			},
			complete : function() {
				setTimeout(function() {
					$(".loadingGif").hide();
				}, 500);
			},
			success : function(data) {
				$("#inforUser").empty();
				setTimeout(function() {
					$("#inforUser").append(data);
					$("#inforUser").modal({
						escapeClose : false,
						clickClose : false,
						showClose : false,
						fadeDuration : 300,
						fadeDelay : 0.50
					});
				}, 500);
			}
		});
		return false;
	});

	$(".add-blance").tooltip({
		placement : "bottom"
	});
	$(".edit-user").tooltip({
		placement : "bottom"
	});

	function updateBalance(username, balance) {
		var rows = $('tbody').find('tr').each(function() {
			var columns = $(this).children('td');
			if (columns.eq(1).children('span').text() == username) {
				var balanceCurrent = columns.eq(3).text();
				columns.eq(3).text(Number(balanceCurrent) + Number(balance));
				return;
			}
		});
	}
</script>
</html>
