<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Quản lý khóa học - System Admin</title>
<jsp:include page="/WEB-INF/view/admin/include/css.jsp" />
<jsp:include page="/WEB-INF/view/admin/include/js.jsp" />
<link rel="stylesheet" href="<c:url value="user/css/linearicons.css"/>">
<link rel="stylesheet" href="<c:url value="user/css/main.css"/>">
<link rel="stylesheet" href="<c:url value="user/css/course.css"/>">
<jsp:useBean id="now" class="java.util.Date" />
</head>
<body>
	<div id="wrapper">
		<%@include file="/WEB-INF/view/admin/include/header.jsp"%>
		<div id="page-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						<form action="managerCourse.ad" method="get">
							<input type="hidden" name="type" value="search">
							<h1 class="page-header">Khóa học</h1>
							<div class="row">
								<div class="col-lg-6">
									<label style="padding-left: 20px;"> Tổng kết quả : <c:out
											value="${countCourse}" /></label>
								</div>
								<div class="col-lg-6">
									<div class="input-group">
										<input type="text" class="form-control" name="keySearch"
											placeholder="Tìm kiếm khóa học(tiêu đề khóa học)"
											value='<c:out value="${keySearch}"/>'> <span
											class="input-group-btn">
											<button class="btn btn-default" type="submit">
												<i class="fa fa-search"></i>
											</button>
										</span>
									</div>
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-lg-12">
									<c:choose>
										<c:when test="${!error}">
											<c:if test="${listCourses != null}">
												<c:forEach var="course" items="${listCourses}">
													<div class="single-popular-carusel col-lg-4 col-md-8">
														<div class="panel panel-green">
															<div class="panel-body">
																<div class="thumb-wrap relative">
																	<div class="thumb relative">
																		<div class="overlay overlay-bg"></div>
																		<c:if
																			test="${course.endSale != null && now < course.endSale}">
																			<img
																				style="width: 32px; height: 32px; position: absolute; right: 0; z-index: 2"
																				src="user/img/sale.png" alt="Giảm giá">
																		</c:if>
																		<img class="img-fluid img-course"
																			src='<c:out value="${course.urlImage}"></c:out>'
																			alt="">
																	</div>
																	<div class="meta d-flex justify-content-between">
																		<p>
																			<span class="lnr lnr-users"></span>${course.sumVote}<span
																				class="lnr lnr-bubble"></span>${course.sumVote} <span
																				style="float: right;"> <c:choose>
																					<c:when
																						test="${course.endSale != null && now < course.endSale}">
																						<fmt:formatNumber type="number"
																							value="${course.price - course.price * course.sale / 100}" /> đ
																</c:when>
																					<c:otherwise>
																						<fmt:formatNumber type="number"
																							value="${course.price}" /> đ
																</c:otherwise>
																				</c:choose>
																			</span>
																		</p>
																	</div>
																</div>
															</div>
															<div class="panel-footer dropup">
																<span
																	class=" dropdown-toggle glyphicon glyphicon-option-vertical"
																	data-toggle="dropdown"></span>
																<ul class="dropdown-menu" style="position: absolute;">
																	<li><a
																		href="updateCourse.ad?courseId=${course.courseId}">Cập
																			nhật khóa học</a></li>
																	<li><a href="#">Xóa khóa học</a></li>
																</ul>
																<c:out value="${course.title}" />
															</div>
														</div>
													</div>
												</c:forEach>
											</c:if>
										</c:when>
										<c:otherwise>
											Trang hiển thị không tồn tại
										</c:otherwise>
									</c:choose>
								</div>
							</div>
							<div class="row">
								<div class="paging text-center">
									<nav>
										<ul class="pagination">
											<c:if test="${listPagings != null}">
												<c:forEach var="page" items="${listPagings}"
													varStatus="status">
													<c:if test="${status.index == 0 && currentPage >= page}">
														<c:if test="${listPagings.size() > 1 && currentPage > 1}">
															<li class="page-item"><a class="page-link"
																href="managerCourse.ad?type=paging&page=1">Đầu</a></li>
														</c:if>
														<c:if test="${currentPage > 1}">
															<li class="page-item"><a class="page-link"
																href="managerCourse.ad?type=paging&page=${currentPage-1}"
																aria-label="Previous"> <span aria-hidden="true">&laquo;</span>
																	<span class="sr-only">Previous</span></a></li>
														</c:if>
													</c:if>
													<li
														class="page-item <c:if test="${currentPage == page}">active</c:if>"><a
														class="page-link"
														href="managerCourse.ad?type=paging&page=${page}"><c:out
																value="${page}"></c:out></a></li>
													<c:if
														test="${status.index == listPagings.size() - 1 && page < totalPage}">
														<li class="page-item"><a class="page-link"
															href="managerCourse.ad?type=paging&page=${currentPage+1}"
															aria-label="Next"> <span aria-hidden="true">&raquo;</span>
																<span class="sr-only">Next</span></a></li>
														<li class="page-item"><a class="page-link"
															href="managerCourse.ad?type=paging&page=${totalPage}">Cuối</a></li>
													</c:if>
												</c:forEach>
											</c:if>
										</ul>
									</nav>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /#wrapper -->
</body>
</html>
