<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Education - System Admin</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
<link rel="icon" type="image/png"
	href="<c:url value="admin/img/favicon.ico"/>" />
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="<c:url value="admin/css/bootstrap.min.css"/>">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="<c:url value="admin/css/vendor/animate.css"/>">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="<c:url value="admin/fonts/font-awesome.css"/>">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="<c:url value="admin/css/vendor/hamburgers.min.css"/>">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="<c:url value="admin/css/vendor/select2.min.css"/>">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="<c:url value="admin/css/vendor/util.css"/>">
<link rel="stylesheet" type="text/css"
	href="<c:url value="admin/css/vendor/main.css"/>">
<!--===============================================================================================-->
</head>
<body>

	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<div class="login100-pic js-tilt" data-tilt>
					<img src="admin/img/img-01.png" alt="IMG">
				</div>

				<form class="login100-form validate-form" action="adminLogin"
					method="post">
					<span class="login100-form-title"> Hệ thống quản trị trang
						web </span>
					<div class="wrap-input100 validate-input" data-validate = "Tài khoản trống">
						<input class="input100" type="text" name="username"
							placeholder="Tên tài khoản" value='<c:out value="${username}"/>'>
						<span class="focus-input100"></span> <span class="symbol-input100">
							<i class="fa fa-user" aria-hidden="true"></i>
						</span>
					</div>

					<div class="wrap-input100 validate-input"
						data-validate="Mật khẩu trống">
						<input class="input100" type="password" name="password"
							placeholder="Mật khẩu"> <span class="focus-input100"></span>
						<span class="symbol-input100"> <i class="fa fa-lock"
							aria-hidden="true"></i>
						</span>
					</div>
					<c:if test="${error != null}">
						<span style="color: red">* ${error}</span>
					</c:if>

					<div class="container-login100-form-btn">
						<button class="login100-form-btn">Đăng nhập</button>
					</div>
				</form>
			</div>
		</div>
	</div>




	<!--===============================================================================================-->
	<script src="<c:url value="admin/js/vendor/jquery-3.2.1.min.js"/>"></script>
	<!--===============================================================================================-->
	<script src="<c:url value="admin/js/vendor/popper.js"/>"></script>
	<script src="<c:url value="admin/js/vendor/bootstrap.min.js"/>"></script>
	<!--===============================================================================================-->
	<script src="<c:url value="admin/js/vendor/select2.min.js"/>"></script>
	<!--===============================================================================================-->
	<script src="<c:url value="admin/js/vendor/tilt.jquery.min.js"/>"></script>
	<script>
		$('.js-tilt').tilt({
			scale : 1.1
		})
	</script>
	<!--===============================================================================================-->
	<script src="<c:url value="admin/js/main.js"/>"></script>

</body>
</html>