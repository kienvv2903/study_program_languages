<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>Đăng thông báo - Bootstrap Admin Theme</title>
<jsp:include page="/WEB-INF/view/admin/include/css.jsp" />
<jsp:include page="/WEB-INF/view/admin/include/js.jsp" />
<link rel="stylesheet" href="admin/css/validetta.css">
<script type="text/javascript" src="admin/js/validetta.js"></script>
<script src="user/js/sweetalert.min.js"></script>
</head>
<body>

	<div id="wrapper">
		<%@include file="/WEB-INF/view/admin/include/header.jsp"%>
		<div id="page-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						<h1 class="page-header">Đăng thông báo</h1>
					</div>
				</div>
				<div class="row">
					<form action="postNotice.ad" method="post" id="post-notice-form">
						<div class="col-lg-12">
							<div class="form-group">
								<label>Nội dung thông báo :</label>
								<textarea rows="5" class="form-control" name="content"
									data-validetta="required,minLength[30]"></textarea>
							</div>
						</div>
						<img style="display: none" class="loading" alt="Loading"
							src='<c:out value="user/img/gif/ajax-loader.gif"></c:out>'>
						<br>
						<div class="row text-center">
							<div class="col-lg-12">
								<input type="submit" class="btn btn-success" id="post-notice"
									value="Đăng thông báo">
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$("#post-notice-form").validetta({
			bubblePosition : "bottom",
			bubbleGapTop : 10,
			bubbleGapLeft : 10,
		}, {
			required : 'Trường thông tin rỗng',
			minLength : 'Độ dài tối thiểu 30 ký tự'
		});
	</script>
</body>
</html>
