<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Tạo khóa học - System Admin</title>
<jsp:include page="/WEB-INF/view/admin/include/css.jsp" />
<jsp:include page="/WEB-INF/view/admin/include/js.jsp" />
<link rel="stylesheet" href="<c:url value="admin/css/checkbox.css"/>">
<link rel="stylesheet" href="admin/css/validetta.css">
<link rel="stylesheet" href="admin/css/bootstrap-select.min.css">
<link rel="stylesheet" href="admin/css/ajax-bootstrap-select.min.css">
<link rel="stylesheet" href="admin/css/trash.css">
<script type="text/javascript" src="admin/js/bootstrap-select.min.js"></script>
<script type="text/javascript"
	src="admin/js/ajax-bootstrap-select.min.js"></script>
</head>
<body>
	<div id="wrapper">
		<%@include file="/WEB-INF/view/admin/include/header.jsp"%>
		<!-- Page Content -->
		<div id="page-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						<h1 class="page-header">Tạo khóa học</h1>
					</div>
				</div>
				<div class="row">
					<form action="creatCourse.ad" method="post"
						enctype="multipart/form-data" id="create-course-myform">
						<div class="col-lg-4" style="overflow-y: auto; height: 500px">
							<h4>Thông tin khóa học</h4>
							<div class="form-group">
								<label>Tên khóa học :</label> <input type="text"
									data-validetta="required,maxLength[255]" name="title"
									id="title" class="form-control">
							</div>
							<div class="form-group">
								<label>Mô tả :</label>
								<textarea rows="3" class="form-control" name="description"
									id="description"></textarea>
							</div>
							<div class="form-group">
								<label>Ảnh mô tả :</label> <input type="file"
									name="image-course" class="form-control"
									accept="image/x-png,image/gif,image/jpeg"
									data-validetta="required,regExp[fileImage]">
							</div>
							<div class="form-group">
								<label>Thời gian học (ngày) :</label> <input type="number"
									class="form-control" value="10" name="time-study"
									id="time-study" data-validetta="required,regExp[numberInteger]">
							</div>
							<div class="form-group">
								<label>Giáo viên :</label> <select
									class="selectpicker form-control with-ajax"
									data-validetta="required" data-live-search="true"
									name="teacher" id="teacher">
								</select>
							</div>
							<div class="form-group">
								<label>Giá (đồng):</label> <input type="number"
									class="form-control" value="300000" name="price" id="price"
									data-validetta="required,regExp[numberInteger]">
							</div>
							<div class="form-group">
								<div class="checkbox checbox-switch switch-primary">
									<label><b>Giảm giá : </b><input type="checkbox"
										value="sale" name="check-sale" id="check-sale" /> <span></span>
									</label>
								</div>
							</div>
							<div id="sale" class="sale" style="display: none">
								<div class="form-group">
									<label> Giảm (%):</label> <input type="number"
										class="form-control" value="" name="sale-course"
										id="sale-course">
								</div>
								<div class="form-group">
									<label> Ngày bắt đầu giảm giá :</label> <input
										class="form-control" type="date" name="start-date-sale"
										id="start-date-sale">
								</div>
								<div class="form-group">
									<label> Ngày kết thúc giảm giá :</label> <input
										class="form-control" type="date" name="end-date-sale"
										id="end-date-sale">
								</div>
							</div>
						</div>
						<div class="col-lg-8" style="overflow-y: auto; height: 500px">
							<h4>Danh sách bài học</h4>
							<div class="table-responsive">
								<table class="table table-hover">
									<thead>
										<tr>
											<th>Tên</th>
											<th>File</th>
											<th>Loại <span
												style="float: right; padding-right: 10px; font-size: 20px; color: green;"
												class="fa fa-plus add-lession"></span></th>
										</tr>
									</thead>
									<tbody id="tb-lession">
										<tr>
											<td><input type="text" name="title-lession"
												class="form-control 1 title-lession"></td>
											<td><input type="file" name="fileUpload"
												class="form-control fileUpload"
												accept="video/mp4,video/x-m4v,video/*" indexRow="1"></td>
											<td><select class="form-control" name="type-lession">
													<option value="0">Miễn phí</option>
													<option value="1">Trả phí</option>
											</select></td>
										</tr>
										<tr>
											<td><input type="text" name="title-lession"
												class="form-control 2 title-lession"></td>
											<td><input type="file" name="fileUpload"
												class="form-control fileUpload"
												accept="video/mp4,video/x-m4v,video/*" indexRow="2"></td>
											<td><select class="form-control" name="type-lession">
													<option value="0">Miễn phí</option>
													<option value="1">Trả phí</option>
											</select></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-lg-12">
								<input type="submit" class="btn btn-success"
									value="Tạo khóa học">
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</body>
<script type="text/javascript"
	src='<c:url value="admin/js/jquery.validate.min.js"></c:url>'></script>
<script type="text/javascript"
	src='<c:url value="admin/js/validetta.js"></c:url>'></script>
<script type="text/javascript">
	$("input[type='checkbox']").change(
			function() {
				if ($(this).is(":checked")) {
					$("#sale").show();
					$("input[type='date']").prop("required", true);
					$("#sale-course").attr("data-validetta",
							"required,regExp[numberInteger]");
				} else {
					$("#sale").hide();
					$("input[type='date']").removeAttr("required");
					$("#sale-course").removeAttr("data-validetta");
				}
			});

	$("#create-course-myform").validetta({
		bubblePosition : "bottom",
		bubbleGapTop : 10,
		bubbleGapLeft : 80,
		validators : {
			regExp : {
				numberInteger : {
					pattern : /^[\+][0-9]*[1-9][0-9]*?$|^[0-9]*[1-9][0-9]*?$/,
					errorMessage : 'Giá trị phải là số nguyên dương !'
				},

				fileVideo : {
					pattern : /mp4$|mpeg$/,
					errorMessage : 'Lỗi định dạng file'
				},

				fileImage : {
					pattern : /jpg$|png$|gif$/,
					errorMessage : 'Lỗi định dạng ảnh'
				}
			}
		}
	}, {
		required : 'Trường thông tin rỗng',
		maxLength : 'Độ dài không vượt quá 255'
	});

	$(function() {
		$('.selectpicker').selectpicker({
			noneSelectedText : 'Tìm kiếm giáo viên',
			noneResultsText : 'Không có giáo viên phù hợp với {0}'
		});
	});
</script>
<script type="text/javascript">
	var options = {
		ajax : {
			url : 'searchTeacher.ad',
			type : 'POST',
			dataType : 'json',
			data : {
				keySearch : '{{{q}}}'
			}
		},
		locale : {
			emptyTitle : 'Tìm kiếm và chọn giáo viên',
			currentlySelected : 'Lựa chọn gần đây',
			statusNoResults : 'Không có giáo viên phù hợp',
			searchPlaceholder : 'Tìm kiếm',
			statusInitialized : false
		},
		preprocessData : function(data) {
			var i, l = data.length, array = [];
			if (l) {
				for (i = 0; i < l; i++) {
					array.push($.extend(true, data[i], {
						text : data[i].username,
						value : data[i].username,
						data : {
							subtext : data[i].name
						}
					}));
				}
			}
			return array;
		}
	};

	$('.selectpicker').selectpicker().filter('.with-ajax').ajaxSelectPicker(
			options);
</script>
<script type="text/javascript">
	$(document).ready(function() {
		var now = new Date();
		var endDate = new Date(now.getTime() + 30 * 24 * 3600 * 1000);
		$("#start-date-sale").attr("value", formatDate(now));
		$("#end-date-sale").attr("value", formatDate(endDate));
	});

	function formatDate(date) {
		var month = (date.getMonth() + 1);
		var day = date.getDate();
		if (month < 10)
			month = "0" + month;
		if (day < 10)
			day = "0" + day;
		var today = date.getFullYear() + '-' + month + '-' + day;
		return today;
	}
</script>
<script type="text/javascript">
	var selectFile;
	$(document).on('change', '.fileUpload', function() {
		selectFile = event.target.files[0];
		var indexRow = $(this).attr("indexRow");
		if (typeof (selectFile) === "undefined") {
			$("." + indexRow).removeAttr("data-validetta");
		} else {
			var fileName = selectFile.name;
			if (!fileName.endsWith(".mp4") && !fileName.endsWith(".mpeg")) {
				$(this).attr("data-validetta", "regExp[fileVideo]");
			} else {
				$("." + indexRow).attr("data-validetta", "required");
			}
		}
	});
</script>
<script type="text/javascript">
	$(document).on('click', '.fa-trash', function() {
		var numberRow = $("tbody > tr").length;
		$(this).parent().parent().remove();
	});

	$(document)
			.on(
					'click',
					'.add-lession',
					function() {
						var numberRow = $("tbody > tr").length + 1;
						var lession = $("<tr><td><input type=\"text\" class=\"form-control "+ numberRow+" title-lession \" name=\"title-lession\"></td><td><input type=\"file\" name=\"fileUpload\" accept=\"video/mp4,video/x-m4v,video/*\" class=\"form-control fileUpload\" indexRow = \""+numberRow +"\"></td><td><select class=\"form-control\" name=\"type-lession\"><option value=\"0\">Miễn phí</option><option value=\"1\">Trả phí</option></select></td><td><i class=\"fa fa-trash\" aria-hidden=\"true\"></i></td></tr>");
						$("#tb-lession").append(lession);
					});
</script>
<!-- Nếu tạo khóa học thành công -->
<c:if test="${creatCourseSuccess != null}">
	<script type="text/javascript">
		swal({
			title : "Tạo khóa học mới",
			text : "${creatCourseSuccess}",
			icon : "success",
			button : false,
		});
	</script>
</c:if>
</html>
