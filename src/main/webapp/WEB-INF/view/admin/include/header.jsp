<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	<div class="navbar-header">
		<a class="navbar-brand" href="home.ad"><i class="fa fa-home fa-fw"></i>Trang
			chủ</a>
	</div>

	<button type="button" class="navbar-toggle" data-toggle="collapse"
		data-target=".navbar-collapse">
		<span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span>
		<span class="icon-bar"></span> <span class="icon-bar"></span>
	</button>

	<ul class="nav navbar-right navbar-top-links">
		<li class="dropdown navbar-inverse"><a class="dropdown-toggle"
			data-toggle="dropdown" href="#"> <i class="fa fa-bell fa-fw"></i>
				<b class="caret"></b>
		</a>
			<ul class="dropdown-menu dropdown-alerts">
				<li><a href="#">
						<div>
							<i class="fa fa-comment fa-fw"></i> New Comment <span
								class="pull-right text-muted small">4 minutes ago</span>
						</div>
				</a></li>
				<li><a href="#">
						<div>
							<i class="fa fa-twitter fa-fw"></i> 3 New Followers <span
								class="pull-right text-muted small">12 minutes ago</span>
						</div>
				</a></li>
				<li><a href="#">
						<div>
							<i class="fa fa-envelope fa-fw"></i> Message Sent <span
								class="pull-right text-muted small">4 minutes ago</span>
						</div>
				</a></li>
				<li><a href="#">
						<div>
							<i class="fa fa-tasks fa-fw"></i> New Task <span
								class="pull-right text-muted small">4 minutes ago</span>
						</div>
				</a></li>
				<li><a href="#">
						<div>
							<i class="fa fa-upload fa-fw"></i> Server Rebooted <span
								class="pull-right text-muted small">4 minutes ago</span>
						</div>
				</a></li>
				<li class="divider"></li>
				<li><a class="text-center" href="#"> <strong>See
							All Alerts</strong> <i class="fa fa-angle-right"></i>
				</a></li>
			</ul></li>
		<li class="dropdown"><a class="dropdown-toggle"
			data-toggle="dropdown" href="#"> <i class="fa fa-user fa-fw"></i>
				<c:out value="${admin}" /> <b class="caret"></b>
		</a>
			<ul class="dropdown-menu dropdown-user">
				<li><a href="#"><i class="fa fa-user fa-fw"></i> Thông tin
						cá nhân</a></li>
				<li><a href="#"><i class="fa fa-gear fa-fw"></i> Cài đặt</a></li>
				<li class="divider"></li>
				<li><a href="logoutAdmin"><i class="fa fa-sign-out fa-fw"></i>
						Đăng xuất</a></li>
			</ul></li>
	</ul>

	<div class="navbar-default sidebar" role="navigation">
		<div class="sidebar-nav navbar-collapse">
			<ul class="nav" id="side-menu">
				<li class="sidebar-search">
					<div class="input-group custom-search-form">
						<input type="text" class="form-control" placeholder="Search...">
						<span class="input-group-btn">
							<button class="btn btn-primary" type="button">
								<i class="fa fa-search"></i>
							</button>
						</span>
					</div>
				</li>
				<li><a href="postNotice.ad" class="active"><i
						class="fa fa-bell fa-fw"></i> Đăng thông báo</a></li>
				<li><a href="managerUser.ad"><i class="fa fa-user fa-fw"></i>
						Quản lý người dùng</a></li>
				<li><a href="#"><i class="fa fa-book fa-fw"></i> Quản lý
						khóa học<span class="fa arrow"></span></a>
					<ul class="nav nav-second-level">
						<li><a href="managerCourse.ad">Danh sách khóa học</a></li>
						<li><a href="creatCourse.ad">Tạo khóa học mới</a></li>
					</ul></li>
			</ul>
		</div>
	</div>
</nav>