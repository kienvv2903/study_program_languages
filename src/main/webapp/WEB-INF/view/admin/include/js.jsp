<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<!-- jQuery -->
<script src="<c:url value="admin/js/vendor/jquery-3.2.1.min.js"/>"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<c:url value="admin/js/bootstrap.min.js"/>"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="<c:url value="admin/js/metisMenu.min.js"/>"></script>

<script src="<c:url value="admin/js/jquery.modal.min.js"/>"></script>
<!-- Custom Theme JavaScript -->
<script src="<c:url value="admin/js/startmin.js"/>"></script>
<!-- Sweet Alert Js -->
<script src="user/js/sweetalert.min.js"></script>