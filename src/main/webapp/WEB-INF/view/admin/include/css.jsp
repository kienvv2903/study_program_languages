<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<!-- Bootstrap Core CSS -->
<link rel="shortcut icon" href="<c:url value="admin/img/favicon.ico"/>">
<link href="<c:url value="admin/css/bootstrap.min.css"/>"
	rel="stylesheet">

<!-- MetisMenu CSS -->
<link href="<c:url value="admin/css/metisMenu.min.css"/>"
	rel="stylesheet">

<!-- Timeline CSS -->
<link href="<c:url value="admin/css/timeline.css"/>" rel="stylesheet">

<!-- Custom CSS -->
<link href="<c:url value="admin/css/startmin.css"/>" rel="stylesheet">

<!-- Morris Charts CSS -->
<link href="<c:url value="admin/css/morris.css"/>" rel="stylesheet">

<!-- Custom Fonts -->
<link href="<c:url value="admin/css/font-awesome.min.css"/>"
	rel="stylesheet" type="text/css">