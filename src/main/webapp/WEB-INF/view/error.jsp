<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>Education</title>
<link rel="shortcut icon" href="<c:url value="user/img/logo.png"/>">

<!-- Bootstrap Core CSS -->
<link href="<c:url value="user/css/error/style_page_error.css"/>"
	rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Montserrat:300,700"
	rel="stylesheet">
</head>
<body>
	<div id="notfound">
		<div class="notfound">
			<h2>
				<c:out value="${error}" />
			</h2>
			<div class="notfound-404">
				<h1>
					4<span></span>4
				</h1>
			</div>
			<a href="home">Trở về trang chủ</a>
		</div>
	</div>
</body>
</html>