<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<!-- Mobile Specific Meta -->
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- Author Meta -->
<meta name="author" content="colorlib">
<!-- Meta Description -->
<meta name="description" content="">
<!-- Meta Keyword -->
<meta name="keywords" content="">
<!-- Site Title -->
<title>Education</title>
<jsp:include page="/WEB-INF/view/user/include/css.jsp" />
<link rel="stylesheet"
	href="<c:url value="user/css/starrr/starrr.css"/>">
<!-- Link css phần comment -->
<link rel="stylesheet" href="<c:url value="user/css/comment.css"/>">
<style type="text/css">
div.comment-list img {
	width: 60px;
	height: 60px;
}
</style>
</head>
<body>
	<!-- Start header -->
	<%@include file="/WEB-INF/view/user/include/header.jsp"%>
	<!-- End header -->
	<!-- start banner Area -->
	<section class="banner-area relative about-banner" id="home">
		<div class="overlay overlay-bg"></div>
		<div class="container">
			<div class="row d-flex align-items-center justify-content-center">
				<div class="about-content col-lg-12">
					<h1 class="text-white">Chi tiết khóa học</h1>
					<p class="text-white link-nav">
						<a href="home">Trang chủ </a> <span class="lnr lnr-arrow-right"></span>
						<a href="course"> Khóa học</a>
					</p>
				</div>
			</div>
		</div>
	</section>
	<!-- End banner Area -->

	<!-- Start course-details Area -->
	<section class="course-details-area pt-120">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 left-contents">
					<div class="main-image">
						<img class="img-fluid" src="user/img/m-img.jpg" alt="">
					</div>
					<div class="jq-tab-wrapper" id="horizontalTab">
						<div class="jq-tab-menu">
							<div class="jq-tab-title active" data-tab="1">Giới thiệu</div>
							<div class="jq-tab-title" data-tab="2">Danh sách bài học</div>
							<div class="jq-tab-title" data-tab="3">Bình luận</div>
							<div class="jq-tab-title review" data-tab="4" id="review">Đánh
								giá</div>
						</div>
						<div class="jq-tab-content-wrapper">
							<div class="jq-tab-content active" data-tab="1">
								<c:out value="${course.description}" />
							</div>
							<div class="jq-tab-content" data-tab="2">
								<c:set var="lessions" value="${course.lessions}"></c:set>
								<c:choose>
									<c:when test="${lessions.size() > 0}">
										<dl class="accordion">
											<c:forEach var="lession" items="${lessions}"
												varStatus="status">
												<dt>
													<a href=""><c:out value="${status.index + 1}"></c:out>
														. <c:out value="${lession.title}"></c:out></a>
												</dt>
												<dd>
													<c:choose>
														<c:when
															test="${lession.typeLession == 0 || (checkRegistedCourse == true && lession.typeLession == 1)}">
															<div class="embed-responsive embed-responsive-21by9">
																<video width="500" height="322" controls
																	controlslist="nodownload">
																	<source type="video/mp4"
																		src='<c:out value="${lession.url}"></c:out>'>
																</video>
															</div>
														</c:when>
														<c:otherwise>Vui lòng đăng ký khóa học để được xem video</c:otherwise>
													</c:choose>
												</dd>
											</c:forEach>
										</dl>
									</c:when>
									<c:otherwise>
										Các video khóa học đang được cập nhật.
									</c:otherwise>
								</c:choose>
							</div>
							<div class="jq-tab-content comment-wrap" data-tab="3">
								<div class="comments-area">
									<c:if test="${listComments != null}">
										<c:forEach var="comment" items="${listComments}">
											<c:set var="stt"
												value="${course.courseId}_${comment.username}"></c:set>
											<div class="comment-list" id="${stt}">
												<div class="single-comment justify-content-between d-flex">
													<div class="user justify-content-between d-flex">
														<div class="thumb ">
															<img src='<c:out value="${comment.user.urlImg}"></c:out>'
																alt="">
														</div>
														<div class="desc">
															<h5>
																<a href="#"><c:out value="${comment.username}"></c:out></a>
															</h5>
															<p class="date">
																<fmt:formatDate type="both" dateStyle="medium"
																	timeStyle="short" value="${comment.creatAt}" />
															</p>
															<p class="comment">
																<c:out value="${comment.content}"></c:out>
															</p>
														</div>
													</div>
													<div class="reply-btn">
														<a href="" class="btn-reply text-uppercase">reply</a>
													</div>
												</div>
											</div>
										</c:forEach>
										<img style="display: none" class="loading" alt="Loading"
											src='<c:out value="user/img/gif/ajax-loader.gif"></c:out>'>
									</c:if>
								</div>
								<c:choose>
									<c:when test="${userLogin == null}">
										<hr>
										<h4 style="color: red">Đăng nhập để có thể bình luận về
											khóa học</h4>
									</c:when>
									<c:otherwise>
										<div class="input-group mb-3">
											<input type="text" class="form-control"
												placeholder="Nhập nội dung bình luận" id="content_comment">
											<div class="input-group-prepend">
												<a class="btn btn-success" id="sendComment">Gửi</a>
											</div>
										</div>
									</c:otherwise>
								</c:choose>
							</div>
							<div class="jq-tab-content review" data-tab="4">
								<div class="review-top row pt-40">
									<div class="col-lg-3">
										<div class="avg-review">
											Điểm <br>
											<c:choose>
												<c:when test="${countRating > 0}">
													<fmt:formatNumber type="number" maxFractionDigits="2"
														value="${sumRating/countRating}" />
												</c:when>
												<c:otherwise>
													0
												</c:otherwise>
											</c:choose>
											<span class="fa fa-star checked"></span> <br> (
											<c:out value="${countRating}"></c:out>
											Ratings)
										</div>
									</div>
									<div class="col-lg-9">
										<div class="rating_score_card">
											<c:forEach var="rate" items="${listRates}" varStatus="status">
												<div class="row">
													<div class="pull-left">
														<div class="pull-left"
															style="width: 35px; line-height: 1;">
															<div style="height: 9px; margin: 5px 0;">
																<c:out value="${rate.numberStar}"></c:out>
																<span class="fa fa-star checked"></span>
															</div>
														</div>
														<div class="pull-left" style="width: 180px;">
															<div class="progress" style="height: 9px; margin: 8px 0;">
																<div class="progress-bar progress-bar-success"
																	role="progressbar"
																	aria-valuenow="<c:out value="${rate.numberStar}"></c:out>"
																	aria-valuemin="0" aria-valuemax="5"
																	style="width: ${rate.countRate*100/countRating}%"></div>
															</div>
														</div>
														<c:if test="${rate.countRate != 0}">
															<div class="pull-right" style="margin-left: 10px;">
																<c:out value="${rate.countRate}"></c:out>
															</div>
														</c:if>
													</div>
												</div>
											</c:forEach>
										</div>
									</div>
								</div>
								<hr>
								<div class="feedeback">
									<form method="post" action="rating.us"
										onsubmit="return ratingCourse();">
										<h4 class="mb-20">Đánh giá của bạn về khóa học</h4>
										<div class="alert alert-success notification"
											id="alert-success">Cảm ơn đánh giá của bạn</div>
										<div class="alert alert-danger emptyScore">Mức đánh giá
											sao từ 1-5 sao</div>
										<input name="courseId" type="hidden"
											value="${course.courseId}"> <input class="score"
											name="score" type="hidden" value="">
										<div class="d-flex flex-row reviews">
											<span>Chất lượng</span>
											<div class='starrr' id='star1'></div>
											<div>
												&nbsp; <span class='your-choice-was' style='display: none;'>
													Your rating was <span class='choice'></span>.
												</span>
											</div>
										</div>
										<br>
										<h4 class="pb-20">Phản hồi của bạn</h4>
										<textarea id="feedback" name="feedback" class="form-control"
											cols="10" rows="10"></textarea>
										<button class="mt-20 primary-btn text-right text-uppercase">Gửi
											đánh giá</button>
									</form>
								</div>
								<div class="comments-area mb-30">
									<c:if test="${listVotes != null}">
										<c:forEach items="${listVotes}" var="vote" varStatus="status">
											<div class="comment-list">
												<div
													class="single-comment single-reviews justify-content-between d-flex">
													<div class="user justify-content-between d-flex">
														<div class="thumb">
															<img class="img-comment"
																src='<c:out value="${vote.user.urlImg}"></c:out>'
																alt="Avatar">
														</div>
														<div class="desc">
															<h5>
																<a href="#"><c:out value="${vote.user.name}"></c:out></a>
																<div class="star">
																	<c:set var="score" value="${vote.score}"></c:set>
																	<c:forEach var="i" begin="1" end="5">
																		<span
																			class="fa fa-star <c:if test='${score >= i}'>checked</c:if>"></span>
																	</c:forEach>
																</div>
															</h5>
															&nbsp;&nbsp;&nbsp;&nbsp;
															<c:out value="${vote.timeVote}"></c:out>
															<p class="comment">
																<c:out value="${vote.feedBack}"></c:out>
															</p>
														</div>
													</div>
												</div>
											</div>
											<c:if test="${status.index != listVotes.size()-1}">
												<hr>
											</c:if>
										</c:forEach>
									</c:if>
								</div>
							</div>
						</div>
					</div>
				</div>
				<jsp:useBean id="now" class="java.util.Date" />
				<div class="col-lg-4 right-contents">
					<c:if test="${checkRegistedCourse == false}">
						<form action="registeCourse.us" method="post">
					</c:if>
					<input name="courseId" type="hidden" value="${course.courseId}">
					<ul>
						<li><a class="justify-content-between d-flex" href="#">
								<p>Tên khóa học</p> <span class="or"><c:out
										value="${course.title}" /></span>
						</a></li>
						<li><a class="justify-content-between d-flex" href="#">
								<p>Giáo viên giảng dạy</p> <span><c:out
										value="${course.user.name}" /></span>
						</a></li>
						<li><a class="justify-content-between d-flex" href="#">
								<p>Số lượng bài học</p> <span><c:out
										value="${course.lessions.size()}" /></span>
						</a></li>
						<c:if test="${course.endSale != null && now < course.endSale}">
							<li><a class="justify-content-between d-flex" href="#">
									<p>Giảm giá</p> <span><fmt:formatNumber type="number"
											value="${course.sale}" />%</span>
							</a></li>
						</c:if>
						<li><a class="justify-content-between d-flex" href="#">
								<p>Giá</p> <span> <c:if
										test="${course.endSale != null && now < course.endSale}">
										<strike><fmt:formatNumber type="number"
												value="${course.price -course.price * course.sale / 100}" /></strike>
									</c:if> <fmt:formatNumber type="number" value="${course.price}" /> đ
							</span>
						</a></li>
						<li><a class="justify-content-between d-flex" href="#">
								<p>Thời lượng học</p> <span><c:out
										value="${course.timeStudy}" /> ngày</span>
						</a></li>
						<li><a class="justify-content-between d-flex" href="#">
								<p>Số lượng đăng ký</p> <span><c:out
										value="${course.courseDetails.size()}" /></span>
						</a></li>
					</ul>
					<c:choose>
						<c:when test="${checkRegistedCourse == false}">
							<button type="submit" class="primary-btn text-uppercase"
								<c:if test="${userLogin != null}">
									onclick="return confirm('Bạn có muốn đăng ký khóa học này không?');"
								</c:if>>Đăng
								ký khóa học</button>
							</form>
						</c:when>
						<c:otherwise>
							<button class="primary-btn text-uppercase">Đã đăng ký</button>
						</c:otherwise>
					</c:choose>
				</div>
			</div>
		</div>
	</section>
	<!-- End course-details Area -->


	<!-- Start popular-courses Area -->
	<section class="popular-courses-area section-gap courses-page">
		<div class="container">
			<div class="row d-flex justify-content-center">
				<div class="menu-content pb-70 col-lg-8">
					<div class="title text-center">
						<h1 class="mb-10">Các khóa học liên quan</h1>
						<p>Đây là danh sách các khóa học có lĩnh vực liên quan hoặc
							cùng giáo viên giảng dạy.</p>
					</div>
				</div>
			</div>
			<div class="row">
				<c:if test="${listCoursesConcern != null}">
					<c:forEach var="course" items="${listCoursesConcern}">
						<div class="single-popular-carusel col-lg-3 col-md-6">
							<div class="thumb-wrap relative">
								<div class="thumb relative">
									<div class="overlay overlay-bg"></div>
									<c:if test="${course.endSale != null && now < course.endSale}">
										<img
											style="width: 32px; height: 32px; position: absolute; right: 0; z-index: 2"
											src="user/img/sale.png" alt="Giảm giá">
									</c:if>
									<img class="img-fluid" src="user/img/p1.jpg" alt="">
								</div>
								<div class="meta d-flex justify-content-between">
									<p>
										<span class="lnr lnr-users"></span> ${course.sumVote}<span
											class="lnr lnr-bubble"></span>${course.sumVote}
									</p>
									<h4>
										<c:choose>
											<c:when
												test="${course.endSale != null && now < course.endSale}">
												<fmt:formatNumber type="number"
													value="${course.price - course.price * course.sale / 100}" />
											đ
												</c:when>
											<c:otherwise>
												<fmt:formatNumber type="number" value="${course.price}" />
											đ
												</c:otherwise>
										</c:choose>
									</h4>
								</div>
							</div>
							<div class="details">
								<a
									href="course-details?courseId=<c:out value="${course.courseId}" />">
									<h4>
										<c:out value="${course.title}" />
									</h4>
								</a>
								<p>
									<c:out value="${course.description}" />
								</p>
							</div>
						</div>
					</c:forEach>
				</c:if>
			</div>
			<div class="row">
				<a href="#" class="primary-btn text-uppercase mx-auto">Xem thêm</a>
			</div>
		</div>
	</section>
	<!-- End popular-courses Area -->

	<!-- start footer Area -->
	<%@include file="/WEB-INF/view/user/include/footer.jsp"%>
	<!-- End footer Area -->

	<!-- Link js -->
	<script src="user/js/sweetalert.min.js"></script>
	<!-- Nếu đăng ký khóa học thành công -->
	<c:if test="${registeCourseSuccess != null}">
		<script type="text/javascript">
			swal({
				title : "Đăng ký khóa học",
				text : "Bạn đã đăng ký thành công !",
				icon : "success",
				button : false,
			});
		</script>
	</c:if>
	<!-- Nếu tài khoản không đủ đăng ký -->
	<c:if test="${notMoneyPayCourse != null}">
		<script type="text/javascript">
			swal({
				title : "Đăng ký khóa học",
				text : "Tài khoản của bạn không đủ để đăng ký.\nVui lòng nạp thêm !",
				icon : "error",
				button : false,
			});
		</script>
	</c:if>

	<jsp:include page="/WEB-INF/view/user/include/js.jsp" />
	<script src="user/js/starrr/starrr.js"></script>
	<script type="text/javascript">
		$('#star1').starrr({
			change : function(e, value) {
				if (value) {
					$('.choice').text(value);
					$('.score').val(value);
				} else {
					$('.your-choice-was').hide();
				}
			}
		});
	</script>
	<script type="text/javascript">
		function ratingCourse() {
			var score = $(".choice").text();
			if (score == '') {
				$(".emptyScore").show();
				setTimeout(function() {
					$(".emptyScore").hide();
				}, 3000);
				return false;
			}
		}
	</script>

	<!-- Code js hiển thị phần đánh giá (rating) -->
	<c:if test="${ratingShow != null}">
		<script type="text/javascript">
			$(document).ready(function() {
				var tabs = $(".jq-tab-title");
				$.each(tabs, function() {
					if ($(this).attr('data-tab') == '4') {
						$(this).attr("class", "jq-tab-title review active");
					} else {
						$(this).attr("class", "jq-tab-title");
					}
				});

				var divs = $(".jq-tab-content");
				$.each(divs, function() {
					if ($(this).attr('data-tab') == '4') {
						$(".review").show();
					} else {
						$(".jq-tab-content").hide();
					}
				});
			});
		</script>
	</c:if>
	<c:if test="${notifi != null}">
		<script type="text/javascript">
			$(".notification").show();
			setTimeout(function() {
				$(".notification").hide();
			}, 5000);
		</script>
	</c:if>
	<script type="text/javascript">
		var index = 0;
		$("#sendComment").click(function() {
			$.ajax({
				type : 'POST',
				url : 'commentCourse.us',
				data : {
					courseId : '${course.courseId}',
					content : $("#content_comment").val(),
					userLogin : '${userLogin}'
				},
				beforeSend : function() {
					$('.loading').show();
				},
				complete : function() {
					setTimeout(function() {
						$(".loading").hide();
					}, 500);
				},
				success : function(data) {
					$("#content_comment").text('');
					$(".comments-area").append(data);
				}
			});
			return false;
		});
	</script>
</body>
</html>