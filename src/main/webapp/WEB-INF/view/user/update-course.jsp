<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="zxx" class="no-js">
<head>
	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Favicon-->
	<link rel="shortcut icon" href="img/fav.png">
	<!-- Author Meta -->
	<meta name="author" content="colorlib">
	<!-- Meta Description -->
	<meta name="description" content="">
	<!-- Meta Keyword -->
	<meta name="keywords" content="">
	<!-- meta character set -->
	<meta charset="UTF-8">
	<!-- Site Title -->
	<title>Education</title>

	<jsp:include page="/WEB-INF/view/user/include/css.jsp" />
</head>
<body>	
  	!-- Start header -->
	<%@include file="/WEB-INF/view/user/include/header.jsp"%>
	<!-- End header -->
  
	<!-- start banner Area -->
	<section class="banner-area relative about-banner" id="home">	
		<div class="overlay overlay-bg"></div>
		<div class="container">				
			<div class="row d-flex align-items-center justify-content-center">
				<div class="about-content col-lg-12">
					<h1 class="text-white">
						Gallery				
					</h1>	
					<p class="text-white link-nav"><a href="index.html">Home </a>  <span class="lnr lnr-arrow-right"></span>  <a href="gallery.html"> Gallery</a></p>
				</div>	
			</div>
		</div>
	</section>
	<!-- End banner Area -->	
		
	<!-- Start gallery Area -->
	<section class="gallery-area section-gap">
		<div class="container">
			<div class="row">
				<div class="col-lg-7">
					<a href="img/gallery/g1.jpg" class="img-gal">
						<div class="single-imgs relative">		
							<div class="overlay overlay-bg"></div>
							<div class="relative">				
								<img class="img-fluid" src="img/gallery/g1.jpg" alt="">		
							</div>
						</div>
					</a>
				</div>
				<div class="col-lg-5">
					<a href="img/gallery/g2.jpg" class="img-gal">
						<div class="single-imgs relative">		
							<div class="overlay overlay-bg"></div>
							<div class="relative">				
								<img class="img-fluid" src="img/gallery/g2.jpg" alt="">				
							</div>
						</div>
					</a>
				</div>
				<div class="col-lg-4">
					<a href="img/gallery/g3.jpg" class="img-gal">
						<div class="single-imgs relative">		
							<div class="overlay overlay-bg"></div>
							<div class="relative">				
								<img class="img-fluid" src="img/gallery/g3.jpg" alt="">				
							</div>
						</div>
					</a>
				</div>
				<div class="col-lg-4">
					<a href="img/gallery/g4.jpg" class="img-gal">
						<div class="single-imgs relative">		
							<div class="overlay overlay-bg"></div>
							<div class="relative">					
								<img class="img-fluid" src="img/gallery/g4.jpg" alt="">				
							</div>
						</div>
					</a>
				</div>
				<div class="col-lg-4">
					<a href="img/gallery/g5.jpg"  class="img-gal">
						<div class="single-imgs relative">		
							<div class="overlay overlay-bg"></div>
							<div class="relative">					
								<img class="img-fluid" src="img/gallery/g5.jpg" alt="">				
							</div>
						</div>
					</a>
				</div>
				<div class="col-lg-5">
					<a href="img/gallery/g6.jpg" class="img-gal">
						<div class="single-imgs relative">		
							<div class="overlay overlay-bg"></div>
							<div class="relative">				
								<img class="img-fluid" src="img/gallery/g6.jpg" alt="">				
							</div>
						</div>
					 </a>
				</div>
				<div class="col-lg-7">
					<a href="img/gallery/g7.jpg" class="img-gal">
						<div class="single-imgs relative">		
							<div class="overlay overlay-bg"></div>
							<div  class="relative">					
								<img class="img-fluid" src="img/gallery/g7.jpg" alt="">				
							</div>
						</div>
					</a>
				</div>
			</div>
		</div>	
	</section>
	<!-- End gallery Area -->
											

	<!-- Start cta-two Area -->
	<section class="cta-two-area">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 cta-left">
					<h1>Not Yet Satisfied with our Trend?</h1>
				</div>
				<div class="col-lg-4 cta-right">
					<a class="primary-btn wh" href="#">view our blog</a>
				</div>
			</div>
		</div>	
	</section>
	<!-- End cta-two Area -->						    			

	<!-- start footer Area -->
	<%@include file="/WEB-INF/view/user/include/footer.jsp"%>
	<!-- End footer Area -->

	<!-- Link js -->
	<jsp:include page="/WEB-INF/view/user/include/js.jsp" />
</body>
</html>