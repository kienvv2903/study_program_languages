<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="zxx" class="no-js">
<head>
<!-- Mobile Specific Meta -->
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- Favicon-->
<link rel="shortcut icon" href="img/fav.png">
<!-- Author Meta -->
<meta name="author" content="colorlib">
<!-- Meta Description -->
<meta name="description" content="">
<!-- Meta Keyword -->
<meta name="keywords" content="">
<!-- meta character set -->
<meta charset="UTF-8">
<!-- Site Title -->
<title>Education</title>

<jsp:include page="/WEB-INF/view/user/include/css.jsp" />
</head>
<body>
	<!-- Start header -->
	<%@include file="/WEB-INF/view/user/include/header.jsp"%>
	<!-- End header -->

	<!-- start banner Area -->
	<section class="banner-area relative about-banner" id="home">
		<div class="overlay overlay-bg"></div>
		<div class="container">
			<div class="row d-flex align-items-center justify-content-center">
				<div class="about-content col-lg-12">
					<h1 class="text-white">Khóa học đã đăng ký</h1>
					<p class="text-white link-nav">
						<a href="home">Trang chủ </a> <span class="lnr lnr-arrow-right"></span>
						<a href="#"> Khóa học của tôi</a>
					</p>
				</div>
			</div>
		</div>
	</section>
	<!-- End banner Area -->
	<section class="popular-course-area section-gap">
		<div class="container">
			<div class="row d-flex justify-content-center">
				<div class="menu-content pb-70 col-lg-8">
					<div class="title text-center">
						<h1 class="mb-10">Danh sách khóa học đã đăng ký</h1>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="active-popular-carusel">
					<c:if test="${myCourses.size()>0}">
						<c:forEach items="${myCourses}" var="course">
							<div class="single-popular-carusel">
								<div class="thumb-wrap relative">
									<div class="thumb relative">
										<div class="overlay overlay-bg"></div>
										<img class="img-fluid" src="user/img/p1.jpg" alt="">
									</div>
								</div>
								<div class="details">
									<a
										href="course-details?courseId=<c:out value="${course.courseId}" />">
										<h4>
											<c:out value="${course.title}" />
										</h4>
									</a>
									<p>
										<c:out value="${course.description}" />
									</p>
								</div>
							</div>
						</c:forEach>
					</c:if>
				</div>
			</div>
			<c:if test="${myCourses.size() == 0}">
				<div class="row d-flex justify-content-center">
					<div class="title text-center">
						<h4>Hiện tại bạn chưa đăng ký khóa học nào</h4>
					</div>
				</div>
			</c:if>
		</div>
	</section>
	<!-- start footer Area -->
	<%@include file="/WEB-INF/view/user/include/footer.jsp"%>
	<!-- End footer Area -->

	<!-- Link js -->
	<jsp:include page="/WEB-INF/view/user/include/js.jsp" />
</body>
</html>