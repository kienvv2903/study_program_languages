<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>Education</title>
<link rel="shortcut icon" href="<c:url value="user/img/logo.png"/>">
<!-- Bootstrap Core CSS -->
<link href="<c:url value="admin/css/bootstrap.min.css"/>"
	rel="stylesheet">

<!-- MetisMenu CSS -->
<link href="<c:url value="admin/css/metisMenu.min.css"/>"
	rel="stylesheet">

<!-- Custom CSS -->
<link href="<c:url value="admin/css/startmin.css"/>" rel="stylesheet">

<!-- Custom Fonts -->
<link href="<c:url value="admin/css/font-awesome.min.css"/>"
	rel="stylesheet" type="text/css">
</head>
<body style="background-image: url('user/img/background.jpg');">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<div class="login-panel panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">
							<a href="home"><i class="fa fa-home"></i>Trang chủ</a>
						</h3>
					</div>
					<div class="panel-body">
						<div
							<c:if test="${error != null}">class="alert alert-danger"</c:if>
							id="error">
							<c:out value="${error}"></c:out>
						</div>
						<c:if test="${message != ''}">
							<div class="alert alert-success">
								<strong>${message}</strong>
							</div>
						</c:if>
						<form method="post" action="login"
							onsubmit="return validateLogin()">
							<fieldset>
								<div class="form-group">
									<input class="form-control" placeholder="Tài khoản"
										name="username" id="username"
										value='<c:out value="${username}"></c:out>' autofocus>
								</div>
								<div class="form-group">
									<input class="form-control" placeholder="Mật khẩu"
										name="password" id="password" type="password">
								</div>
								<div class="form-group">
									<div class="clearfix">
										<a class="float-left">Quên mật khẩu ?</a> <a
											style="float: right;" href="register">Đăng kí</a>
									</div>
								</div>
								<!-- Change this to a button or input when using this as a form -->
								<input class="btn btn-lg btn-success btn-block"
									value="Đăng nhập" type="submit" />
							</fieldset>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Footer -->

	<!-- jQuery -->
	<script src="<c:url value="admin/js/jquery.min.js"/>"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="<c:url value="admin/js/bootstrap.min.js"/>"></script>

	<!-- Metis Menu Plugin JavaScript -->
	<script src="<c:url value="admin/js/metisMenu.min.js"/>"></script>

	<!-- Custom Theme JavaScript -->
	<script src="<c:url value="admin/js/startmin.js"/>"></script>
	<script src="<c:url value="user/js/login.js"/>"></script>
</body>
</html>