<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>Education</title>
<link rel="shortcut icon" href="<c:url value="user/img/logo.png"/>">

<!-- Bootstrap Core CSS -->
<link href="<c:url value="admin/css/bootstrap.min.css"/>"
	rel="stylesheet">

<!-- MetisMenu CSS -->
<link href="<c:url value="admin/css/metisMenu.min.css"/>"
	rel="stylesheet">

<!-- Custom CSS -->
<link href="<c:url value="admin/css/startmin.css"/>" rel="stylesheet">

<!-- Custom Fonts -->
<link href="<c:url value="admin/css/font-awesome.min.css"/>"
	rel="stylesheet" type="text/css">
<!-- Date picker -->
<link href="<c:url value="user/css/datepicker/datepicker.css"/>"
	rel="stylesheet" />
</head>
<body style="background-image: url('user/img/background.jpg');">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3" style="padding-top: 10px">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Đăng ký tài khoản</h3>
					</div>
					<div class="panel-body">
						<div
							<c:if test="${errors != null}">class="alert alert-danger"</c:if>
							id="error">
							<c:forEach var="error" items="${errors}">
								<c:out value="${error}" />
								<br>
							</c:forEach>
						</div>
						<form method="post" action="register"
							onsubmit="return validateRegister()">
							<fieldset>
								<div class="form-group">
									<label>Tên tài khoản : </label> <input class="form-control"
										placeholder="Tên tài khoản" name="username" id="username"
										value='<c:out value="${username}"></c:out>' autofocus><span
										id="check-username" style="color: red"></span>
								</div>
								<div class="form-group">
									<label>Họ và tên : </label> <input class="form-control"
										placeholder="Họ và tên" name="name" id="name"
										value='<c:out value="${name}"></c:out>' autofocus>
								</div>
								<div class="form-group">
									<label>Giới tính : </label> <select class="form-control"
										id="sex" name="sex">
										<option value="Nam">Nam</option>
										<option value="Nữ">Nữ</option>
										<option value="Khác">Khác</option>
									</select>
								</div>
								<div class="form-group">
									<label>Số điện thoại : </label> <input class="form-control"
										placeholder="Số điện thoại" name="phone" id="phone"
										value='<c:out value="${phone}"></c:out>' autofocus>
								</div>
								<div class="form-group">
									<label>Ngày sinh : </label> <input type="text" id="birthDay"
										name="birthday" class="form-control" value="${birthday}" />
								</div>
								<div class="form-group">
									<label>Email : </label> <input class="form-control"
										placeholder="E-mail" name="email" id="email" type="email"
										value='<c:out value="${email}"></c:out>' autofocus><span
										id="check-email" style="color: red"></span>
								</div>
								<div class="form-group">
									<label>Mật khẩu : </label> <input class="form-control"
										placeholder="Mật khẩu" name="password" id="password"
										type="password" value="">
								</div>
								<div class="form-group">
									<label>Mật khẩu xác nhận : </label> <input class="form-control"
										placeholder="Xác nhận mật khẩu" name="passwordAgain"
										id="passwordAgain" type="password" value="">
								</div>
								<!-- Change this to a button or input when using this as a form -->
								<input class="btn btn-lg btn-success btn-block" value="Đăng kí"
									type="submit" />
							</fieldset>
						</form>
						<div style="text-align: center">
							<a href="login">Về màn hình đăng nhập</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Footer -->

	<!-- jQuery -->
	<script src="<c:url value="admin/js/jquery.min.js"/>"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="<c:url value="admin/js/bootstrap.min.js"/>"></script>

	<!-- Metis Menu Plugin JavaScript -->
	<script src="<c:url value="admin/js/metisMenu.min.js"/>"></script>

	<!-- Custom Theme JavaScript -->
	<script src="<c:url value="admin/js/startmin.js"/>"></script>
	<!-- Js DatePicker -->
	<script
		src="<c:url value="user/js/datepicker/bootstrap-datepicker.js"/>"></script>
	<script src="<c:url value="user/js/registe.js"/>"></script>
	<script type="text/javascript" src="<c:url value="user/js/utils.js"/>"></script>

</body>
</html>
