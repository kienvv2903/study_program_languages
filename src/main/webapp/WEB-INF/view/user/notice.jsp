<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="zxx" class="no-js">
<head>
<!-- Mobile Specific Meta -->
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- Favicon-->
<link rel="shortcut icon" href="img/fav.png">
<!-- Author Meta -->
<meta name="author" content="colorlib">
<!-- Meta Description -->
<meta name="description" content="">
<!-- Meta Keyword -->
<meta name="keywords" content="">
<!-- meta character set -->
<meta charset="UTF-8">
<!-- Site Title -->
<title>Education</title>

<jsp:include page="/WEB-INF/view/user/include/css.jsp" />
</head>
<body>
	<!-- Start header -->
	<%@include file="/WEB-INF/view/user/include/header.jsp"%>
	<!-- End header -->

	<!-- start banner Area -->
	<section class="banner-area relative about-banner" id="home">
		<div class="overlay overlay-bg"></div>
		<div class="container">
			<div class="row d-flex align-items-center justify-content-center">
				<div class="about-content col-lg-12">
					<h1 class="text-white">Thông báo</h1>
					<p class="text-white link-nav">
						<a href="home">Trang chủ </a> <span class="lnr lnr-arrow-right"></span>
						<a href="note"> Thông báo trang web</a>
					</p>
				</div>
			</div>
		</div>
	</section>
	<!-- End banner Area -->

	<!-- Start Sample Area -->
	<section class="sample-text-area">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<h3 class="mb-30">Thông báo</h3>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-lg-3">
					<div class="primary-select form-group" id="default-select">
						<label>Hiển thị số người dùng/bản ghi </label> <select
							name="limit">
							<option value="25" <c:if test="${limit == 25}">selected</c:if>>25</option>
							<option value="50" <c:if test="${limit == 50}">selected</c:if>>50</option>
							<option value="100" <c:if test="${limit == 100}">selected</c:if>>100</option>
						</select>
					</div>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-lg-12">
					<dl class="accordion">
						<c:if test="${listNotifications != null}">
							<c:forEach items="${listNotifications}" var="notice"
								varStatus="status">
								<dt>
									<a href="">${status.index + 1}. <c:out
											value="${notice.typeStr}" /> | <fmt:formatDate
											pattern="yyyy-MM-dd HH:mm" value="${notice.creatAt}" /> |
										Bởi <c:out value="${notice.username}"></c:out></a>
								</dt>
								<dd>
									<div class="row">
										<div class="col-lg-12">
											<blockquote class="generic-blockquote">
												“
												<c:out value="${notice.description}"></c:out>
												”
											</blockquote>
										</div>
									</div>
								</dd>
								<c:if test="${status.index != listNotifications.size()-1}">
									<hr>
								</c:if>
							</c:forEach>
						</c:if>
					</dl>
				</div>
			</div>
		</div>
	</section>

	<!-- start footer Area -->
	<%@include file="/WEB-INF/view/user/include/footer.jsp"%>
	<!-- End footer Area -->

	<!-- Link js -->
	<jsp:include page="/WEB-INF/view/user/include/js.jsp" />
</body>
</html>