<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="zxx" class="no-js">
<head>
<!-- Mobile Specific Meta -->
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- Favicon-->
<link rel="shortcut icon" href="img/fav.png">
<!-- Author Meta -->
<meta name="author" content="colorlib">
<!-- Meta Description -->
<meta name="description" content="">
<!-- Meta Keyword -->
<meta name="keywords" content="">
<!-- meta character set -->
<meta charset="UTF-8">
<!-- Site Title -->
<title>Education</title>

<jsp:include page="/WEB-INF/view/user/include/css.jsp" />
</head>
<body>
	<!-- Start header -->
	<%@include file="/WEB-INF/view/user/include/header.jsp"%>
	<!-- End header -->

	<!-- start banner Area -->
	<section class="banner-area relative about-banner" id="home">
		<div class="overlay overlay-bg"></div>
		<div class="container">
			<div class="row d-flex align-items-center justify-content-center">
				<div class="about-content col-lg-12">
					<h1 class="text-white">Câu hỏi thường gặp</h1>
					<p class="text-white link-nav">
						<a href="home">Trang chủ </a> <span class="lnr lnr-arrow-right"></span>
						<a href="questions"> Câu hỏi thường gặp</a>
					</p>
				</div>
			</div>
		</div>
	</section>
	<!-- End banner Area -->

	<!-- Start Sample Area -->
	<section class="sample-text-area">
		<div class="container">
			<dl class="accordion">
				<dt>
					<a href="">Cách nạp tiền vào tài khoản để đăng ký khóa học</a>
				</dt>
				<dd>
					<div class="row">
						<div class="col-lg-12">
							<blockquote class="generic-blockquote">
								“Vui lòng liên hệ với chủ trang web qua email <a
									href="mailto:vukien29031997@gmail.com"><span class="text">vukien29031997@gmail.com</span></a>
								để được hỗ trợ cách thức nạp thẻ và thanh toán khóa học”
							</blockquote>
						</div>
					</div>
				</dd>
				<hr>
				<dt>
					<a href="">Block Quotes</a>
				</dt>
				<dd>
					<div class="row">
						<div class="col-lg-12">
							<blockquote class="generic-blockquote">“Recently,
								the US Federal government banned online casinos from operating
								in America by making it illegal to transfer money to them
								through any US bank or payment system. As a result of this law,
								most of the popular online casino networks such as Party Gaming
								and PlayTech left the United States. Overnight, online casino
								players found themselves being chased by the Federal government.
								But, after a fortnight, the online casino industry came up with
								a solution and new online casinos started taking root. These
								began to operate under a different business umbrella, and by
								doing that, rendered the transfer of money to and from them
								legal. A major part of this was enlisting electronic banking
								systems that would accept this new clarification and start doing
								business with me. Listed in this article are the electronic
								banking”</blockquote>
						</div>
					</div>
				</dd>
			</dl>
		</div>
	</section>

	<!-- start footer Area -->
	<%@include file="/WEB-INF/view/user/include/footer.jsp"%>
	<!-- End footer Area -->

	<!-- Link js -->
	<jsp:include page="/WEB-INF/view/user/include/js.jsp" />
</body>
</html>