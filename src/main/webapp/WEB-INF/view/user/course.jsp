<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE>
<html>
<head>
<!-- Mobile Specific Meta -->
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- Favicon-->
<link rel="shortcut icon" href="img/fav.png">
<!-- Author Meta -->
<meta name="author" content="colorlib">
<!-- Meta Description -->
<meta name="description" content="">
<!-- Meta Keyword -->
<meta name="keywords" content="">
<!-- meta character set -->
<meta charset="UTF-8">
<!-- Site Title -->
<title>Education</title>
<jsp:include page="/WEB-INF/view/user/include/css.jsp" />
<jsp:useBean id="now" class="java.util.Date" />
</head>
<body>
	<!-- Start header -->
	<%@include file="/WEB-INF/view/user/include/header.jsp"%>
	<!-- End header -->

	<!-- start banner Area -->
	<section class="banner-area relative about-banner" id="home">
		<div class="overlay overlay-bg"></div>
		<div class="container">
			<div class="row d-flex align-items-center justify-content-center">
				<div class="about-content col-lg-12">
					<h1 class="text-white">Khóa học</h1>
					<p class="text-white link-nav">
						<a href="home">Trang chủ </a> <span class="lnr lnr-arrow-right"></span>
						<a href="course"> Khóa học</a>
					</p>
				</div>
			</div>
		</div>
	</section>
	<!-- End banner Area -->

	<!-- Start popular-courses Area -->
	<section class="popular-courses-area section-gap courses-page">
		<div class="container">
			<form action="course" method="get">
				<div class="single-element-widget">
					<h3 class="mb-30">Sắp xếp theo</h3>
					<div class="row">
						<input type="hidden" name="type" value="search">
						<div class="primary-select col-lg-3" id="default-select">
							<select class="sort_course" name="typeSort">
								<option value="most_interested"
									<c:if test="${typeSort eq 'most_interested'}">selected</c:if>>Quan
									tâm</option>
								<option value="hight_price"
									<c:if test="${typeSort eq 'hight_price'}">selected</c:if>>Giá
									từ cao đến thấp</option>
								<option value="low_price"
									<c:if test="${typeSort eq 'low_price'}">selected</c:if>>Giá
									từ thấp đến cao</option>
								<option value="sale"
									<c:if test="${typeSort eq 'sale'}">selected</c:if>>Giảm
									giá</option>
								<option value="new_course"
									<c:if test="${typeSort eq 'new_course'}">selected</c:if>>Khóa
									học mới</option>
							</select>
						</div>
						<div class="col-lg-2">
							<img style="padding-top: 10px; display: none" class="loading"
								alt="Loading"
								src='<c:out value="user/img/gif/ajax-loader.gif"></c:out>'>
						</div>
						<div class="col-lg-7">
							<input type="text" name="keySearch" id="search-course"
								placeholder="Tìm kiếm khóa học (Tên khóa học)"
								class="form-control" value="${keySearch}">
						</div>
					</div>
				</div>
			</form>
			<div class="row list_course">
				<c:if test="${listCourses != null}">
					<c:choose>
						<c:when test="${listCourses.size() > 0}">
							<c:forEach var="course" items="${listCourses}">
								<div class="single-popular-carusel col-lg-3 col-md-6">
									<div class="thumb-wrap relative">
										<div class="thumb relative">
											<div class="overlay overlay-bg"></div>
											<c:if
												test="${course.endSale != null && now < course.endSale}">
												<img
													style="width: 32px; height: 32px; position: absolute; right: 0; z-index: 2"
													src="user/img/sale.png" alt="Giảm giá">
											</c:if>
											<img class="img-fluid img-course"
												src='<c:out value="${course.urlImage}"></c:out>' alt="">
										</div>
										<div class="meta d-flex justify-content-between">
											<p>
												<span class="lnr lnr-users"></span>
												<fmt:formatNumber type="number" maxFractionDigits="2"
													value="${course.rate}" />
												<span class="lnr lnr-bubble"></span>${course.sumVote}
											</p>
											<h4>
												<c:choose>
													<c:when
														test="${course.endSale != null && now < course.endSale}">
														<fmt:formatNumber type="number"
															value="${course.price - course.price * course.sale / 100}" />
											đ
												</c:when>
													<c:otherwise>
														<fmt:formatNumber type="number" value="${course.price}" />
											đ
												</c:otherwise>
												</c:choose>
											</h4>
										</div>
									</div>
									<div class="details">
										<a
											href="course-details?courseId=<c:out value="${course.courseId}" />">
											<h4>
												<c:out value="${course.title}" />
											</h4>
										</a>
										<p>
											<c:out value="${course.teacherName}" />
										</p>
									</div>
								</div>
							</c:forEach>
						</c:when>
						<c:otherwise>
							<div>Không tìm thấy khóa học phù hợp</div>
						</c:otherwise>
					</c:choose>
				</c:if>
			</div>
			<div class=" row paging">
				<nav class="text-center">
					<ul class="pagination">
						<c:if test="${listPagings != null}">
							<c:forEach var="page" items="${listPagings}" varStatus="status">
								<c:if test="${status.index == 0 && currentPage >= page}">
									<c:if test="${listPagings.size() > 1 && currentPage > 1}">
										<li class="page-item"><a class="page-link"
											href="course?type=paging&page=1">Đầu</a></li>
									</c:if>
									<c:if test="${currentPage > 1}">
										<li class="page-item"><a class="page-link"
											href="course?type=paging&page=${currentPage-1}"
											aria-label="Previous"> <span aria-hidden="true">&laquo;</span>
												<span class="sr-only">Previous</span></a></li>
									</c:if>
								</c:if>
								<li
									class="page-item <c:if test="${currentPage == page}">active</c:if>"><a
									class="page-link" href="course?type=paging&page=${page}"><c:out
											value="${page}"></c:out></a></li>
								<c:if
									test="${status.index == listPagings.size() - 1 && page < totalPage}">
									<li class="page-item"><a class="page-link"
										href="course?type=paging&page=${currentPage+1}"
										aria-label="Next"> <span aria-hidden="true">&raquo;</span>
											<span class="sr-only">Next</span></a></li>
									<li class="page-item"><a class="page-link"
										href="course?type=paging&page=${totalPage}">Cuối</a></li>
								</c:if>
							</c:forEach>
						</c:if>
					</ul>
				</nav>
			</div>
		</div>
	</section>
	<!-- End popular-courses Area -->

	<!-- Start search-course Area -->
	<section class="search-course-area relative">
		<div class="overlay overlay-bg"></div>
		<div class="container">
			<div class="row justify-content-between align-items-center">
				<div class="col-lg-6 col-md-6 search-course-left">
					<h1 class="text-white">
						Get reduced fee <br> during this Summer!
					</h1>
					<p>inappropriate behavior is often laughed off as “boys will be
						boys,” women face higher conduct standards especially in the
						workplace. That’s why it’s crucial that, as women, our behavior on
						the job is beyond reproach.</p>
					<div class="row details-content">
						<div class="col single-detials">
							<span class="lnr lnr-graduation-hat"></span> <a href="#"><h4>Expert
									Instructors</h4></a>
							<p>Usage of the Internet is becoming more common due to rapid
								advancement of technology and power.</p>
						</div>
						<div class="col single-detials">
							<span class="lnr lnr-license"></span> <a href="#"><h4>Certification</h4></a>
							<p>Usage of the Internet is becoming more common due to rapid
								advancement of technology and power.</p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6 search-course-right section-gap">
					<form class="form-wrap" action="#">
						<h4 class="text-white pb-20 text-center mb-30">Search for
							Available Course</h4>
						<input type="text" class="form-control" name="name"
							placeholder="Your Name" onfocus="this.placeholder = ''"
							onblur="this.placeholder = 'Your Name'"> <input
							type="phone" class="form-control" name="phone"
							placeholder="Your Phone Number" onfocus="this.placeholder = ''"
							onblur="this.placeholder = 'Your Phone Number'"> <input
							type="email" class="form-control" name="email"
							placeholder="Your Email Address" onfocus="this.placeholder = ''"
							onblur="this.placeholder = 'Your Email Address'">
						<div class="form-select" id="service-select">
							<select>
								<option data-display="">Choose Course</option>
								<option value="1">Course One</option>
								<option value="2">Course Two</option>
								<option value="3">Course Three</option>
								<option value="4">Course Four</option>
							</select>
						</div>
						<button class="primary-btn text-uppercase">Submit</button>
					</form>
				</div>
			</div>
		</div>
	</section>
	<!-- End search-course Area -->

	<!-- Start upcoming-event Area -->
	<section class="upcoming-event-area section-gap">
		<div class="container">
			<div class="row d-flex justify-content-center">
				<div class="menu-content pb-70 col-lg-8">
					<div class="title text-center">
						<h1 class="mb-10">Upcoming Events of our Institute</h1>
						<p>If you are a serious astronomy fanatic like a lot of us</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="active-upcoming-event-carusel">
					<div class="single-carusel row align-items-center">
						<div class="col-12 col-md-6 thumb">
							<img class="img-fluid" src="user/img/e1.jpg" alt="">
						</div>
						<div class="detials col-12 col-md-6">
							<p>25th February, 2018</p>
							<a href="#"><h4>The Universe Through A Child S Eyes</h4></a>
							<p>For most of us, the idea of astronomy is something we
								directly connect to “stargazing”, telescopes and seeing
								magnificent displays in the heavens.</p>
						</div>
					</div>
					<div class="single-carusel row align-items-center">
						<div class="col-12 col-md-6 thumb">
							<img class="img-fluid" src="user/img/e2.jpg" alt="">
						</div>
						<div class="detials col-12 col-md-6">
							<p>25th February, 2018</p>
							<a href="#"><h4>The Universe Through A Child S Eyes</h4></a>
							<p>For most of us, the idea of astronomy is something we
								directly connect to “stargazing”, telescopes and seeing
								magnificent displays in the heavens.</p>
						</div>
					</div>
					<div class="single-carusel row align-items-center">
						<div class="col-12 col-md-6 thumb">
							<img class="img-fluid" src="user/img/e1.jpg" alt="">
						</div>
						<div class="detials col-12 col-md-6">
							<p>25th February, 2018</p>
							<a href="#"><h4>The Universe Through A Child S Eyes</h4></a>
							<p>For most of us, the idea of astronomy is something we
								directly connect to “stargazing”, telescopes and seeing
								magnificent displays in the heavens.</p>
						</div>
					</div>
					<div class="single-carusel row align-items-center">
						<div class="col-12 col-md-6 thumb">
							<img class="img-fluid" src="user/img/e1.jpg" alt="">
						</div>
						<div class="detials col-12 col-md-6">
							<p>25th February, 2018</p>
							<a href="#"><h4>The Universe Through A Child S Eyes</h4></a>
							<p>For most of us, the idea of astronomy is something we
								directly connect to “stargazing”, telescopes and seeing
								magnificent displays in the heavens.</p>
						</div>
					</div>
					<div class="single-carusel row align-items-center">
						<div class="col-12 col-md-6 thumb">
							<img class="img-fluid" src="user/img/e2.jpg" alt="">
						</div>
						<div class="detials col-12 col-md-6">
							<p>25th February, 2018</p>
							<a href="#"><h4>The Universe Through A Child S Eyes</h4></a>
							<p>For most of us, the idea of astronomy is something we
								directly connect to “stargazing”, telescopes and seeing
								magnificent displays in the heavens.</p>
						</div>
					</div>
					<div class="single-carusel row align-items-center">
						<div class="col-12 col-md-6 thumb">
							<img class="img-fluid" src="user/img/e1.jpg" alt="">
						</div>
						<div class="detials col-12 col-md-6">
							<p>25th February, 2018</p>
							<a href="#"><h4>The Universe Through A Child S Eyes</h4></a>
							<p>For most of us, the idea of astronomy is something we
								directly connect to “stargazing”, telescopes and seeing
								magnificent displays in the heavens.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End upcoming-event Area -->

	<!-- start footer Area -->
	<%@include file="/WEB-INF/view/user/include/footer.jsp"%>
	<!-- End footer Area -->

	<!-- Link js -->
	<jsp:include page="/WEB-INF/view/user/include/js.jsp" />
	<script type="text/javascript">
		$(".sort_course").change(function() {
			var typeSort = "";
			$(".sort_course option:selected").each(function() {
				typeSort += $(this).val() + " ";
			});
			var keySearch = $("#search-course").val();
			$.ajax({
				type : 'GET',
				url : 'sortCourse',
				data : {
					keySearch : keySearch,
					typeSort : typeSort,
					start : 0,
				},
				beforeSend : function() {
					$('.loading').show();
				},
				complete : function() {
					setTimeout(function() {
						$(".loading").hide();
					}, 500);
				},
				success : function(data) {
					$(".list_course").empty();
					$(".paging").empty();
					$(".list_course").append(data);
				}
			});
			return false;
		});
	</script>
</body>
</html>