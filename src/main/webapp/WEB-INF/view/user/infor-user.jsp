<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<!-- Mobile Specific Meta -->
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- Favicon-->
<link rel="shortcut icon" href="img/fav.png">
<!-- Author Meta -->
<meta name="author" content="colorlib">
<!-- Meta Description -->
<meta name="description" content="">
<!-- Meta Keyword -->
<meta name="keywords" content="">
<!-- meta character set -->
<meta charset="UTF-8">
<!-- Site Title -->
<title>Education</title>
<jsp:include page="/WEB-INF/view/user/include/css.jsp" />
<link href="<c:url value="user/css/datepicker/datepicker.css"/>"
	rel="stylesheet" />
<link href="<c:url value="user/css/loading-bar/loading-bar.css"/>"
	rel="stylesheet" />
<style type="text/css">
.boder-error {
	border: 1px solid red;
}

form span {
	color: red;
	font-size: 15px;
}
</style>
</head>
<body>
	<!-- Start header -->
	<%@include file="/WEB-INF/view/user/include/header.jsp"%>
	<!-- End header -->
	<!-- start banner Area -->
	<section class="banner-area relative" id="home">
		<div class="overlay overlay-bg"></div>
		<div class="container">
			<div class="row d-flex align-items-center justify-content-center">
				<div class="about-content col-lg-12">
					<h1 class="text-white">Thông tin cá nhân</h1>
					<p class="text-white link-nav">
						<a href="home">Trang chủ </a>
					</p>
				</div>
			</div>
		</div>
	</section>
	<!-- End banner Area -->

	<!-- Start post-content Area -->
	<section class="post-content-area single-post-area">
		<div class="container">
			<div class="row">
				<div class="col-lg-4 sidebar-widgets">
					<div class="widget-wrap"
						style="background-image: url('user/img/background.jpg');">
						<div class="single-sidebar-widget user-info-widget">
							<img width="120px" height="120px"
								src='<c:out value="${user.urlImg}"></c:out>' alt="">
							<h4>
								<c:out value="${user.name}"></c:out>
							</h4>
							<div class="user-details row">
								<p class="date col-lg-12 col-md-12 col-6">
									<span class="fa fa-balance-scale"> Số dư tài khoản : </span> <span
										style="color: red"><c:out value="${user.balance}"></c:out>
										đ</span>
								</p>
								<p class="date col-lg-12 col-md-12 col-6">
									<span class="lnr lnr-calendar-full"> Ngày tham gia : </span> <span
										style="color: red"><fmt:formatDate
											pattern="yyyy-MM-dd HH:mm:ss" value="${user.creatAt}" /></span>
								</p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-8 sidebar-widgets">
					<div class="widget-wrap">
						<div class="container">
							<div
								<c:if test="${errors != null}">class="alert alert-danger"</c:if>
								id="error">
								<c:forEach var="error" items="${errors}">
									<c:out value="${error}" />
									<br>
								</c:forEach>
							</div>
							<form class="form-horizontal" action="updateInfor.us"
								method="post">
								<input type="hidden" id="urlImg" name="urlImg" value="">
								<div class="form-group">
									<label>Tên tài khoản : </label> <input type="email"
										class="form-control" id="username"
										value="<c:out value="${user.username}"></c:out>"
										name="username" disabled="disabled">
								</div>
								<div class="form-group">
									<label>Họ và tên :</label> <input type="text"
										class="form-control" id="name" name="name"
										value="<c:out value="${user.name}"></c:out>"> <span
										class="err error-name"></span>
								</div>
								<div class="form-group">
									<label>Giới tính :</label>
									<div class="radio">
										<label class="radio-inline"> <input type="radio"
											name="sex" value="Nam"
											<c:if test = "${user.sex eq 'Nam'}"> checked
      										</c:if>>Nam
										</label> &nbsp;&nbsp;&nbsp; <label class="radio-inline"><input
											type="radio" name="sex" value="Nữ"
											<c:if test = "${user.sex eq 'Nữ'}"> checked
      										</c:if>>Nữ</label>
									</div>
								</div>
								<div class="form-group">
									<label>Số điện thoại :</label> <input type="text"
										class="form-control" id="phone" name="phone"
										value="<c:out value="${user.phone}"></c:out>"> <span
										class="err error-phone"></span>
								</div>
								<div class="form-group">
									<label>Ngày sinh : </label> <input type="text" id="birthday"
										name="birthday" class="form-control"
										value="<c:out value="${user.birthday}"></c:out>" /><span
										class="err error-birthday"></span>
								</div>
								<div class="form-group">
									<label>Email :</label> <input type="email" class="form-control"
										id="email" name="email"
										value="<c:out value="${user.email}"></c:out>"><span
										class="err error-email"></span>
								</div>
								<div class="form-group">
									<div class="input-group">
										<div class="input-group-prepend">
											<span class="input-group-text">Ảnh đại diện</span>
										</div>
										<div class="custom-file">
											<input type="file" class="custom-file-input" id="upFile"
												accept="image/x-png,image/gif,image/jpeg"> <label
												class="custom-file-label" id="fileName">Chọn file </label>
										</div>
										<span id="errorUpload" style="color: red"></span>
									</div>
									<div class="progress" id="progress" style="height: 2px">
										<div class="progress-bar" role="progressbar" aria-valuemin="0"
											aria-valuemax="100" style="width: 0%;"></div>
									</div>
								</div>
								<div class="form-group">
									<button disabled="disabled" id="update-infor" type="submit" class="btn btn-primary">Cập
										nhật thông tin</button>
									<button id="reset-data" type="reset" class="btn btn-default">Cancel</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End post-content Area -->
	<!-- start footer Area -->
	<%@include file="/WEB-INF/view/user/include/footer.jsp"%>
	<!-- End footer Area -->

	<!-- Link js -->
	<jsp:include page="/WEB-INF/view/user/include/js.jsp" />
	<script
		src="<c:url value="user/js/datepicker/bootstrap-datepicker.js"/>"></script>
	<script src="<c:url value="user/js/loading-bar/loading-bar.js"/>"></script>

	<!-- Firebase -->
	<script src="https://www.gstatic.com/firebasejs/5.9.1/firebase.js"></script>
	<script>
		// Initialize Firebase
		var config = {
			apiKey : "AIzaSyAyUs_cIATdZdx1Q9fvkPIUZmmbfrOzv38",
			authDomain : "webtt-a8565.firebaseapp.com",
			databaseURL : "https://webtt-a8565.firebaseio.com",
			projectId : "webtt-a8565",
			storageBucket : "webtt-a8565.appspot.com",
			messagingSenderId : "747191552946"
		};
		firebase.initializeApp(config);
	</script>
	<script type="text/javascript"
		src="<c:url value="user/js/upload-image.js"/>"></script>
	<script type="text/javascript" src="<c:url value="user/js/utils.js"/>"></script>
	<script type="text/javascript"
		src="<c:url value="user/js/infor-user.js"/>"></script>
</body>
</html>