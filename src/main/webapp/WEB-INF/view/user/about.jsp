<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="zxx" class="no-js">
<head>
<!-- Mobile Specific Meta -->
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- Author Meta -->
<meta name="author" content="colorlib">
<!-- Meta Description -->
<meta name="description" content="">
<!-- Meta Keyword -->
<meta name="keywords" content="">
<!-- meta character set -->
<meta charset="UTF-8">
<!-- Site Title -->
<title>Education</title>
<jsp:include page="/WEB-INF/view/user/include/css.jsp" />
</head>
<body>
	<!-- Start header -->
	<%@include file="/WEB-INF/view/user/include/header.jsp"%>
	<!-- End header -->

	<!-- start banner Area -->
	<section class="banner-area relative about-banner" id="home">
		<div class="overlay overlay-bg"></div>
		<div class="container">
			<div class="row d-flex align-items-center justify-content-center">
				<div class="about-content col-lg-12">
					<h1 class="text-white">Giới thiệu</h1>
					<p class="text-white link-nav">
						<a href="home">Trang chủ </a> <span class="lnr lnr-arrow-right"></span>
						<a href="#"> Giới thiệu</a>
					</p>
				</div>
			</div>
		</div>
	</section>
	<!-- End banner Area -->

	<!-- Start feature Area -->
	<section class="feature-area pb-120">
		<div class="container">
			<div class="row">
				<c:if test="${listNotifications != null}">
					<c:forEach items="${listNotifications}" var="notification">
						<div class="col-lg-4">
							<div class="single-feature">
								<div class="title">
									<h4>
										<c:out value="${notification.typeStr}" />
									</h4>
								</div>
								<div class="desc-wrap">
									<p>
										<c:out value="${notification.description}" />
										.
									</p>
									<c:choose>
										<c:when test="${notification.type == 3}">
											<a href="notice">Xem các thông báo khác</a>
										</c:when>
										<c:otherwise>
											<a
												href="course-details?courseId=<c:out value="${notification.courseId}" />">Xem
												chi tiết</a>
										</c:otherwise>
									</c:choose>
								</div>
							</div>
						</div>
					</c:forEach>
				</c:if>
			</div>
		</div>
	</section>
	<!-- End feature Area -->

	<!-- Start info Area -->
	<section class="info-area pb-120">
		<div class="container-fluid">
			<div class="row align-items-center">
				<div class="col-lg-6 no-padding info-area-left">
					<img class="img-fluid" src="user/img/about-img.jpg" alt="">
				</div>
				<div class="col-lg-6 info-area-right">
					<h1>Who we are to Serave the nation</h1>
					<p>inappropriate behavior is often laughed off as “boys will be
						boys,” women face higher conduct standards especially in the
						workplace. That’s why it’s crucial that, as women, our behavior on
						the job is beyond reproach.</p>
					<br>
					<p>inappropriate behavior is often laughed off as “boys will be
						boys,” women face higher conduct standards especially in the
						workplace. That’s why it’s crucial that, as women, our behavior on
						the job is beyond reproach. inappropriate behavior is often
						laughed off as “boys will be boys,” women face higher conduct
						standards especially in the workplace. That’s why it’s crucial
						that, as women, our behavior on the job is beyond reproach.</p>
				</div>
			</div>
		</div>
	</section>
	<!-- End info Area -->

	<!-- Start course-mission Area -->
	<section class="course-mission-area pb-120">
		<div class="container">
			<div class="row d-flex justify-content-center">
				<div class="menu-content pb-70 col-lg-8">
					<div class="title text-center">
						<h1 class="mb-10">Goals to Achieve for the leadership</h1>
						<p>Who are in extremely love with eco friendly system.</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 accordion-left">

					<!-- accordion 2 start-->
					<dl class="accordion">
						<dt>
							<a href="">Success</a>
						</dt>
						<dd>Nunc placerat mi id nisi interdum mollis. Praesent
							pharetra, justo ut scelerisque mattis, leo quam aliquet diam,
							congue laoreet elit metus eget diam. Proin ac metus diam.</dd>
						<dt>
							<a href="">Info</a>
						</dt>
						<dd>Donec nec justo eget felis facilisis fermentum. Aliquam
							porttitor mauris sit amet orci. Aenean dignissim pellentesque
							felis. leo quam aliquet diam, congue laoreet elit metus eget
							diam.</dd>
						<dt>
							<a href="">Danger</a>
						</dt>
						<dd>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
							Phasellus hendrerit. Pellentesque aliquet nibh nec urna. Proin ac
							metus diam.</dd>
						<dt>
							<a href="">Warning</a>
						</dt>
						<dd>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
							Phasellus hendrerit. Pellentesque aliquet nibh nec urna. Proin ac
							metus diam.</dd>
					</dl>
					<!-- accordion 2 end-->
				</div>
				<div
					class="col-md-6 video-right justify-content-center align-items-center d-flex relative">
					<div class="overlay overlay-bg"></div>
					<a class="play-btn"
						href="https://www.youtube.com/watch?v=ARA0AxrnHdM"><img
						class="img-fluid mx-auto" src="user/img/play.png" alt=""></a>
				</div>
			</div>
		</div>
	</section>
	<!-- End course-mission Area -->


	<!-- Start search-course Area -->
	<section class="search-course-area relative">
		<div class="overlay overlay-bg"></div>
		<div class="container">
			<div class="row justify-content-between align-items-center">
				<div class="col-lg-6 col-md-6 search-course-left">
					<h1 class="text-white">
						Get reduced fee <br> during this Summer!
					</h1>
					<p>inappropriate behavior is often laughed off as “boys will be
						boys,” women face higher conduct standards especially in the
						workplace. That’s why it’s crucial that, as women, our behavior on
						the job is beyond reproach.</p>
					<div class="row details-content">
						<div class="col single-detials">
							<span class="lnr lnr-graduation-hat"></span> <a href="#"><h4>Expert
									Instructors</h4></a>
							<p>Usage of the Internet is becoming more common due to rapid
								advancement of technology and power.</p>
						</div>
						<div class="col single-detials">
							<span class="lnr lnr-license"></span> <a href="#"><h4>Certification</h4></a>
							<p>Usage of the Internet is becoming more common due to rapid
								advancement of technology and power.</p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6 search-course-right section-gap">
					<form class="form-wrap" action="#">
						<h4 class="text-white pb-20 text-center mb-30">Search for
							Available Course</h4>
						<input type="text" class="form-control" name="name"
							placeholder="Your Name" onfocus="this.placeholder = ''"
							onblur="this.placeholder = 'Your Name'"> <input
							type="phone" class="form-control" name="phone"
							placeholder="Your Phone Number" onfocus="this.placeholder = ''"
							onblur="this.placeholder = 'Your Phone Number'"> <input
							type="email" class="form-control" name="email"
							placeholder="Your Email Address" onfocus="this.placeholder = ''"
							onblur="this.placeholder = 'Your Email Address'">
						<div class="form-select" id="service-select">
							<select>
								<option data-display="">Choose Course</option>
								<option value="1">Course One</option>
								<option value="2">Course Two</option>
								<option value="3">Course Three</option>
								<option value="4">Course Four</option>
							</select>
						</div>
						<button class="primary-btn text-uppercase">Submit</button>
					</form>
				</div>
			</div>
		</div>
	</section>
	<!-- End search-course Area -->

	<!-- Start review Area -->
	<section class="review-area section-gap relative">
		<div class="overlay overlay-bg"></div>
		<div class="container">
			<div class="row">
				<div class="active-review-carusel">
					<div class="single-review item">
						<div class="title justify-content-start d-flex">
							<a href="#"><h4>Fannie Rowe</h4></a>
							<div class="star">
								<span class="fa fa-star checked"></span> <span
									class="fa fa-star checked"></span> <span
									class="fa fa-star checked"></span> <span class="fa fa-star"></span>
								<span class="fa fa-star"></span>
							</div>
						</div>
						<p>Accessories Here you can find the best computer accessory
							for your laptop, monitor, printer, scanner, speaker. Here you can
							find the best computer accessory for your laptop, monitor,
							printer, scanner, speaker.</p>
					</div>
					<div class="single-review item">
						<div class="title justify-content-start d-flex">
							<a href="#"><h4>Hulda Sutton</h4></a>
							<div class="star">
								<span class="fa fa-star checked"></span> <span
									class="fa fa-star checked"></span> <span
									class="fa fa-star checked"></span> <span class="fa fa-star"></span>
								<span class="fa fa-star"></span>
							</div>
						</div>
						<p>Accessories Here you can find the best computer accessory
							for your laptop, monitor, printer, scanner, speaker. Here you can
							find the best computer accessory for your laptop, monitor,
							printer, scanner, speaker.</p>
					</div>
					<div class="single-review item">
						<div class="title justify-content-start d-flex">
							<a href="#"><h4>Fannie Rowe</h4></a>
							<div class="star">
								<span class="fa fa-star checked"></span> <span
									class="fa fa-star checked"></span> <span
									class="fa fa-star checked"></span> <span class="fa fa-star"></span>
								<span class="fa fa-star"></span>
							</div>
						</div>
						<p>Accessories Here you can find the best computer accessory
							for your laptop, monitor, printer, scanner, speaker. Here you can
							find the best computer accessory for your laptop, monitor,
							printer, scanner, speaker.</p>
					</div>
					<div class="single-review item">
						<div class="title justify-content-start d-flex">
							<a href="#"><h4>Hulda Sutton</h4></a>
							<div class="star">
								<span class="fa fa-star checked"></span> <span
									class="fa fa-star checked"></span> <span
									class="fa fa-star checked"></span> <span class="fa fa-star"></span>
								<span class="fa fa-star"></span>
							</div>
						</div>
						<p>Accessories Here you can find the best computer accessory
							for your laptop, monitor, printer, scanner, speaker. Here you can
							find the best computer accessory for your laptop, monitor,
							printer, scanner, speaker.</p>
					</div>
					<div class="single-review item">
						<div class="title justify-content-start d-flex">
							<a href="#"><h4>Fannie Rowe</h4></a>
							<div class="star">
								<span class="fa fa-star checked"></span> <span
									class="fa fa-star checked"></span> <span
									class="fa fa-star checked"></span> <span class="fa fa-star"></span>
								<span class="fa fa-star"></span>
							</div>
						</div>
						<p>Accessories Here you can find the best computer accessory
							for your laptop, monitor, printer, scanner, speaker. Here you can
							find the best computer accessory for your laptop, monitor,
							printer, scanner, speaker.</p>
					</div>
					<div class="single-review item">
						<div class="title justify-content-start d-flex">
							<a href="#"><h4>Hulda Sutton</h4></a>
							<div class="star">
								<span class="fa fa-star checked"></span> <span
									class="fa fa-star checked"></span> <span
									class="fa fa-star checked"></span> <span class="fa fa-star"></span>
								<span class="fa fa-star"></span>
							</div>
						</div>
						<p>Accessories Here you can find the best computer accessory
							for your laptop, monitor, printer, scanner, speaker. Here you can
							find the best computer accessory for your laptop, monitor,
							printer, scanner, speaker.</p>
					</div>
					<div class="single-review item">
						<img src="user/img/r1.png" alt="">
						<div class="title justify-content-start d-flex">
							<a href="#"><h4>Fannie Rowe</h4></a>
							<div class="star">
								<span class="fa fa-star checked"></span> <span
									class="fa fa-star checked"></span> <span
									class="fa fa-star checked"></span> <span class="fa fa-star"></span>
								<span class="fa fa-star"></span>
							</div>
						</div>
						<p>Accessories Here you can find the best computer accessory
							for your laptop, monitor, printer, scanner, speaker. Here you can
							find the best computer accessory for your laptop, monitor,
							printer, scanner, speaker.</p>
					</div>
					<div class="single-review item">
						<div class="title justify-content-start d-flex">
							<a href="#"><h4>Hulda Sutton</h4></a>
							<div class="star">
								<span class="fa fa-star checked"></span> <span
									class="fa fa-star checked"></span> <span
									class="fa fa-star checked"></span> <span class="fa fa-star"></span>
								<span class="fa fa-star"></span>
							</div>
						</div>
						<p>Accessories Here you can find the best computer accessory
							for your laptop, monitor, printer, scanner, speaker. Here you can
							find the best computer accessory for your laptop, monitor,
							printer, scanner, speaker.</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End review Area -->

	<!-- start footer Area -->
	<%@include file="/WEB-INF/view/user/include/footer.jsp"%>
	<!-- End footer Area -->

	<!-- Link js -->
	<jsp:include page="/WEB-INF/view/user/include/js.jsp" />
</body>
</html>