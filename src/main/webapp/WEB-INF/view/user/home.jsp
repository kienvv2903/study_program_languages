<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="zxx">
<head>
<!-- Mobile Specific Meta -->
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- Favicon-->
<!-- Author Meta -->
<meta name="author" content="colorlib">
<!-- Meta Description -->
<meta name="description" content="Trang web học lập trình">
<!-- Meta Keyword -->
<meta name="keywords" content="Lập trình, program, languages">
<!-- Site Title -->
<title>Education</title>
<jsp:include page="/WEB-INF/view/user/include/css.jsp" />
<jsp:useBean id="now" class="java.util.Date" />
</head>
<body>
	<!-- Start header -->
	<%@include file="/WEB-INF/view/user/include/header.jsp"%>
	<!-- End header -->

	<!-- start banner Area -->
	<section class="banner-area relative" id="home">
		<div class="overlay overlay-bg"></div>
		<div class="container">
			<div
				class="row fullscreen d-flex align-items-center justify-content-between">
				<div class="banner-content col-lg-9 col-md-12">
					<h1 class="text-uppercase">Mang đến cho bạn những khóa học
						tuyệt vời nhất</h1>
					<p class="pt-10 pb-10">Luôn tự hào là trang web uy tín, được
						phần lớn cộng đồng mạng đánh giá. Luôn mang lại những điều tuyệt
						vời nhất!</p>
					<a href="#list-course" class="primary-btn text-uppercase">Danh
						sách khóa học</a>
				</div>
			</div>
		</div>
	</section>
	<!-- End banner Area -->

	<!-- Start feature Area -->
	<section class="feature-area">
		<div class="container">
			<div class="row">
				<c:if test="${listNotifications != null}">
					<c:forEach items="${listNotifications}" var="notification">
						<div class="col-lg-4">
							<div class="single-feature">
								<div class="title">
									<h4>
										<c:out value="${notification.typeStr}" />
									</h4>
								</div>
								<div class="desc-wrap">
									<p>
										<c:out value="${notification.description}" />
										.
									</p>
									<c:choose>
										<c:when test="${notification.type == 3}">
											<a href="notice">Xem các thông báo khác</a>
										</c:when>
										<c:otherwise>
											<a
												href="course-details?courseId=<c:out value="${notification.courseId}" />">Xem
												chi tiết</a>
										</c:otherwise>
									</c:choose>
								</div>
							</div>
						</div>
					</c:forEach>
				</c:if>
			</div>
		</div>
	</section>
	<!-- End feature Area -->

	<!-- Start popular-course Area -->
	<section class="popular-course-area section-gap">
		<div class="container">
			<div class="row d-flex justify-content-center">
				<div class="menu-content pb-70 col-lg-8" id="list-course">
					<div class="title text-center">
						<h1 class="mb-10">Các khóa học được quan tâm</h1>
						<p>Đây là danh sách các khóa học được nhiều người quan tâm và
							đăng ký học.</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="active-popular-carusel">
					<c:if test="${listCourses != null}">
						<c:forEach items="${listCourses}" var="course">
							<div class="single-popular-carusel">
								<div class="thumb-wrap relative">
									<div class="thumb relative">
										<div class="overlay overlay-bg"></div>
										<c:if test="${course.endSale != null && now < course.endSale}">
											<img
												style="width: 32px; height: 32px; position: absolute; right: 0; z-index: 2"
												src="user/img/sale.png" alt="Giảm giá">
										</c:if>
										<img class="img-fluid img-course"
											src='<c:out value="${course.urlImage}"></c:out>' alt="">
									</div>
									<div class="meta d-flex justify-content-between">
										<p>
											<span class="fa fa-star"></span>
											<fmt:formatNumber type="number" maxFractionDigits="2"
												value="${course.rate}" />
											<span class="lnr lnr-bubble"></span>${course.sumVote}
										</p>
										<h4>
											<c:choose>
												<c:when
													test="${course.endSale != null && now < course.endSale}">
													<fmt:formatNumber type="number"
														value="${course.price - course.price * course.sale / 100}" />
											đ
												</c:when>
												<c:otherwise>
													<fmt:formatNumber type="number" value="${course.price}" />
											đ
												</c:otherwise>
											</c:choose>
										</h4>
									</div>
								</div>
								<div class="details">
									<a
										href="course-details?courseId=<c:out value="${course.courseId}" />">
										<h4>
											<c:out value="${course.title}" />
										</h4>
									</a>
									<p>
										<c:out value="${course.teacherName}" />
									</p>
								</div>
							</div>
						</c:forEach>
					</c:if>
				</div>
			</div>
		</div>
	</section>
	<!-- End popular-course Area -->


	<!-- Start search-course Area -->
	<section class="search-course-area relative">
		<div class="overlay overlay-bg"></div>
		<div class="container">
			<div class="row justify-content-between align-items-center">
				<div class="col-lg-6 col-md-6 search-course-left">
					<h1 class="text-white">
						Get reduced fee <br> during this Summer!
					</h1>
					<p>inappropriate behavior is often laughed off as “boys will be
						boys,” women face higher conduct standards especially in the
						workplace. That’s why it’s crucial that, as women, our behavior on
						the job is beyond reproach.</p>
					<div class="row details-content">
						<div class="col single-detials">
							<span class="lnr lnr-graduation-hat"></span> <a href="#"><h4>Expert
									Instructors</h4></a>
							<p>Usage of the Internet is becoming more common due to rapid
								advancement of technology and power.</p>
						</div>
						<div class="col single-detials">
							<span class="lnr lnr-license"></span> <a href="#"><h4>Certification</h4></a>
							<p>Usage of the Internet is becoming more common due to rapid
								advancement of technology and power.</p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6 search-course-right section-gap">
					<form class="form-wrap" action="#">
						<h4 class="text-white pb-20 text-center mb-30">Search for
							Available Course</h4>
						<input type="text" class="form-control" name="name"
							placeholder="Your Name" onfocus="this.placeholder = ''"
							onblur="this.placeholder = 'Your Name'"> <input
							type="phone" class="form-control" name="phone"
							placeholder="Your Phone Number" onfocus="this.placeholder = ''"
							onblur="this.placeholder = 'Your Phone Number'"> <input
							type="email" class="form-control" name="email"
							placeholder="Your Email Address" onfocus="this.placeholder = ''"
							onblur="this.placeholder = 'Your Email Address'">
						<div class="form-select" id="service-select">
							<select>
								<option datd-display="">Choose Course</option>
								<option value="1">Course One</option>
								<option value="2">Course Two</option>
								<option value="3">Course Three</option>
								<option value="4">Course Four</option>
							</select>
						</div>
						<button class="primary-btn text-uppercase">Submit</button>
					</form>
				</div>
			</div>
		</div>
	</section>
	<!-- End search-course Area -->

	<!-- Start blog Area -->
	<section class="blog-area section-gap" id="blog">
		<div class="container">
			<div class="row d-flex justify-content-center">
				<div class="menu-content pb-70 col-lg-8">
					<div class="title text-center">
						<h1 class="mb-10">Latest posts from our Blog</h1>
						<p>In the history of modern astronomy there is.</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-3 col-md-6 single-blog">
					<div class="thumb">
						<img class="img-fluid" src="user/img/b1.jpg" alt="">
					</div>
					<p class="meta">
						25 April, 2018 | By <a href="#">Mark Wiens</a>
					</p>
					<a href="blog-single.html">
						<h5>Addiction When Gambling Becomes A Problem</h5>
					</a>
					<p>Computers have become ubiquitous in almost every facet of
						our lives. At work, desk jockeys spend hours in front of their.</p>
					<a href="#"
						class="details-btn d-flex justify-content-center align-items-center"><span
						class="details">Details</span><span class="lnr lnr-arrow-right"></span></a>
				</div>
				<div class="col-lg-3 col-md-6 single-blog">
					<div class="thumb">
						<img class="img-fluid" src="user/img/b2.jpg" alt="">
					</div>
					<p class="meta">
						25 April, 2018 | By <a href="#">Mark Wiens</a>
					</p>
					<a href="blog-single.html">
						<h5>Computer Hardware Desktops And Notebooks</h5>
					</a>
					<p>Ah, the technical interview. Nothing like it. Not only does
						it cause anxiety, but it causes anxiety for several different
						reasons.</p>
					<a href="#"
						class="details-btn d-flex justify-content-center align-items-center"><span
						class="details">Details</span><span class="lnr lnr-arrow-right"></span></a>
				</div>
				<div class="col-lg-3 col-md-6 single-blog">
					<div class="thumb">
						<img class="img-fluid" src="user/img/b3.jpg" alt="">
					</div>
					<p class="meta">
						25 April, 2018 | By <a href="#">Mark Wiens</a>
					</p>
					<a href="blog-single.html">
						<h5>Make Myspace Your Best Designed Space</h5>
					</a>
					<p>Plantronics with its GN Netcom wireless headset creates the
						next generation of wireless headset and other products such as
						wireless.</p>
					<a href="#"
						class="details-btn d-flex justify-content-center align-items-center"><span
						class="details">Details</span><span class="lnr lnr-arrow-right"></span></a>
				</div>
				<div class="col-lg-3 col-md-6 single-blog">
					<div class="thumb">
						<img class="img-fluid" src="user/img/b4.jpg" alt="">
					</div>
					<p class="meta">
						25 April, 2018 | By <a href="#">Mark Wiens</a>
					</p>
					<a href="blog-single.html">
						<h5>Video Games Playing With Imagination</h5>
					</a>
					<p>About 64% of all on-line teens say that do things online
						that they wouldn’t want their parents to know about. 11% of all
						adult internet</p>
					<a href="#"
						class="details-btn d-flex justify-content-center align-items-center"><span
						class="details">Details</span><span class="lnr lnr-arrow-right"></span></a>
				</div>
			</div>
		</div>
	</section>
	<!-- End blog Area -->

	<!-- start footer Area -->
	<%@include file="/WEB-INF/view/user/include/footer.jsp"%>
	<!-- End footer Area -->

	<!-- Link js -->
	<jsp:include page="/WEB-INF/view/user/include/js.jsp" />
</body>
</html>