<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<script src="user/js/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="user/js/vendor/bootstrap.min.js"></script>
<script
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
<script src="user/js/easing.min.js"></script>
<script src="user/js/hoverIntent.js"></script>
<script src="user/js/superfish.min.js"></script>
<script src="user/js/jquery.ajaxchimp.min.js"></script>
<script src="user/js/jquery.magnific-popup.min.js"></script>
<script src="user/js/jquery.tabs.min.js"></script>
<script src="user/js/jquery.nice-select.min.js"></script>
<script src="user/js/owl.carousel.min.js"></script>
<script src="user/js/mail-script.js"></script>
<script src="user/js/main.js"></script>