<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<header id="header" id="home">
	<div class="header-top">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-sm-6 col-8 header-top-left no-padding">
					<ul>
						<li><a href="#"><i class="fa fa-facebook"></i></a></li>
						<li><a href="#"><i class="fa fa-twitter"></i></a></li>
						<li><a href="tel:+84336428930"><span
								class="lnr lnr-phone-handset"></span> <span class="text">+84336428930</span></a></li>
						<li><a href="mailto:vukien29031997@gmail.com"><span
								class="lnr lnr-envelope"></span> <span class="text">vukien29031997@gmail.com</span></a></li>
					</ul>
				</div>
				<div class="col-lg-6 col-sm-6 col-4 header-top-right no-padding">
					<c:choose>
						<c:when test="${userLogin != null}">
						</c:when>
						<c:otherwise>
							<a href="login">Bạn chưa đăng nhập? Đăng nhập để tham gia
								nhóm học tập</a>
						</c:otherwise>
					</c:choose>
				</div>
			</div>
		</div>
	</div>
	<div class="container main-menu">
		<div class="row align-items-center justify-content-between d-flex">
			<div id="logo">
				<a href="home"><img src="user/img/logo.png" alt="" title="" /></a>
			</div>
			<nav id="nav-menu-container">
				<ul class="nav-menu">
					<li><a href="home">Trang chủ</a></li>
					<li><a href="about">Giới thiệu</a></li>
					<li><a href="course">Khóa học</a></li>
					<li><a href="notice">Thông báo</a></li>
					<li><a href="questions">Câu hỏi thường gặp</a></li>
					<li><a href="contact">Liên hệ</a></li>
					<c:if test="${userLogin != null}">
						<li class="menu-has-children"><a href="#" id="login_name">Tài
								khoản (${userLogin}) <c:if
									test="${countNoticeUnread != null && countNoticeUnread > 0}"> (${countNoticeUnread})</c:if>
						</a>
							<ul>
								<li><a href="infor-user.us">Thông tin tài khoản</a></li>
								<li><a href="myCourse.us">Khóa học của tôi</a></li>
								<li><a href="myNotice.us">Thông báo <c:if
											test="${countNoticeUnread != null && countNoticeUnread > 0}">
											<i style="font-size: 20px" class="fa fa-bell">${countNoticeUnread}</i>
										</c:if></a></li>
								<li><a href="logout">Đăng xuất</a></li>
							</ul></li>
					</c:if>
				</ul>
			</nav>
			<!-- #nav-menu-container -->
		</div>
	</div>
</header>