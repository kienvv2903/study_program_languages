<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<link rel="shortcut icon" href="<c:url value="user/img/logo.png"/>">
<link
	href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700"
	rel="stylesheet">
<!-- CSS============================================= -->
<link rel="stylesheet" href="<c:url value="user/css/linearicons.css"/>">
<link rel="stylesheet"
	href="<c:url value="user/css/font-awesome.min.css"/>">
<link rel="stylesheet" href="<c:url value="user/css/bootstrap.css"/>">
<link rel="stylesheet"
	href="<c:url value="user/css/magnific-popup.css"/>">
<link rel="stylesheet" href="<c:url value="user/css/nice-select.css"/>">
<link rel="stylesheet" href="<c:url value="user/css/animate.min.css"/>">
<link rel="stylesheet" href="<c:url value="user/css/owl.carousel.css"/>">
<link rel="stylesheet" href="<c:url value="user/css/jquery-ui.css"/>">
<link rel="stylesheet" href="<c:url value="user/css/main.css"/>">
<link rel="stylesheet" href="<c:url value="user/css/course.css"/>">
