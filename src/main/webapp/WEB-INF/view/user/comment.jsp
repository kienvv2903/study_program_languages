<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<div class="comment-list" id="">
	<div class="single-comment justify-content-between d-flex">
		<div class="user justify-content-between d-flex">
			<div class="thumb ">
				<img src='<c:out value="kien"></c:out>' alt="">
			</div>
			<div class="desc">
				<h5>
					<a href="#"><c:out value="kien"></c:out></a>
				</h5>
				<p class="date">
				</p>
				<p class="comment">
					<c:out value="Dinh"></c:out>
				</p>
			</div>
		</div>
		<div class="reply-btn">
			<a href="" class="btn-reply text-uppercase">reply</a>
		</div>
	</div>
</div>