$("#recharge").change(function() {
	var money = $("#recharge").val();
	var regex = new RegExp('^\\d+$');
	var regexZero = new RegExp('^0+$');
	if (!regex.test(money) || regexZero.test(money)) {
		$("#recharge").addClass('boder-error');
		$("span.error-money").show();
		$("#recharge-btn").attr("disabled", true);
	} else {
		$("#recharge").removeClass('boder-error');
		$("span.error-money").hide();
		$("#recharge-btn").attr("disabled", false);
	}
});

function recharge(username, balance) {
	var recharge = confirm("Bạn có muốn nạp " + Number(balance)
			+ " đ vào tài khoản " + username + " không ?");
	if (recharge) {
		$.ajax({
			type : 'POST',
			url : 'updateBalance.ad',
			data : {
				username : $("#us").val(),
				balance : $("#recharge").val()
			},
			beforeSend : function() {
				$('.loading').show();
			},
			complete : function() {
				setTimeout(function() {
					$(".loading").hide();
				}, 500);
			},
			success : function(data) {
				switch (data) {
				case "updateBalanceSuccess":
					setTimeout(function() {
						$.modal.close();
					}, 500);
					setTimeout(function() {
						swal({
							title : "Nạp tiền !",
							text : "Đã nạp thành công " + Number(balance)
									+ " đ vào tài khoản " + username,
							icon : "success",
							button : false,
						});
					}, 600);
					updateBalance(username, balance);
					break;
				default:
					window.location.href = "errorPage";
					break;
				}
			}
		});
	}
	return false;
}

$(".add-blance").click(function() {
	$("#addBalance").modal({
		escapeClose : false,
		clickClose : false,
		showClose : false,
		fadeDuration : 300,
		fadeDelay : 0.50
	});
	$("#us").val($(this).attr("username"));
	return false;
});

$(".edit-user").click(function() {
	$("#loadingGif").modal({
		escapeClose : false,
		clickClose : false,
		showClose : false,
		fadeDuration : 300,
		fadeDelay : 0.50
	});
	$.ajax({
		type : 'POST',
		url : 'inforUser.ad',
		data : {
			username : $(this).attr("username")
		},
		beforeSend : function() {
			$(".loadingGif").show();
		},
		complete : function() {
			setTimeout(function() {
				$(".loadingGif").hide();
			}, 500);
		},
		success : function(data) {
			$("#inforUser").empty();
			setTimeout(function() {
				$("#inforUser").append(data);
				$("#inforUser").modal({
					escapeClose : false,
					clickClose : false,
					showClose : false,
					fadeDuration : 300,
					fadeDelay : 0.50
				});
			}, 500);
		}
	});
	return false;
});

$(".add-blance").tooltip({
	placement : "bottom"
});
$(".edit-user").tooltip({
	placement : "bottom"
});

function updateBalance(username, balance) {
	var rows = $('tbody').find('tr').each(function() {
		var columns = $(this).children('td');
		if (columns.eq(1).children('span').text() == username) {
			var balanceCurrent = columns.eq(3).text();
			columns.eq(3).text(Number(balanceCurrent) + Number(balance));
			return;
		}
	});
}