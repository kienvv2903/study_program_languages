$(document).on('click', '.fa-trash', function() {
	var numberRow = $("tbody > tr").length;
	$(this).parent().parent().remove();
});

$(document)
		.on(
				'click',
				'.add-lession',
				function() {
					var numberRow = $("tbody > tr").length + 1;
					var lession = $("<tr><td><input type=\"hidden\" name=\"lessionId\" value=\"\"><input type=\"text\" class=\"form-control "
							+ numberRow
							+ " title-lession \" name=\"title-lession\"></td><td><input type=\"file\" name=\"fileUpload\" accept=\"video/mp4,video/x-m4v,video/*\" class=\"form-control fileUpload\" indexRow = \""
							+ numberRow
							+ "\"></td><td><i class=\"fa fa-trash\" aria-hidden=\"true\"></i></td></tr>");
					$("#tb-lession").append(lession);
				});

var options = {
	ajax : {
		url : 'searchTeacher.ad',
		type : 'POST',
		dataType : 'json',
		data : {
			keySearch : '{{{q}}}'
		}
	},
	locale : {
		emptyTitle : 'Tìm kiếm và chọn giáo viên',
		currentlySelected : 'Lựa chọn gần đây',
		statusNoResults : 'Không có giáo viên phù hợp',
		searchPlaceholder : 'Tìm kiếm',
		statusInitialized : false
	},
	preprocessData : function(data) {
		var i, l = data.length, array = [];
		if (l) {
			for (i = 0; i < l; i++) {
				array.push($.extend(true, data[i], {
					text : data[i].username,
					value : data[i].username,
					data : {
						subtext : data[i].name
					}
				}));
			}
		}
		return array;
	}
};

$('.selectpicker').selectpicker().filter('.with-ajax')
		.ajaxSelectPicker(options);
$('select').trigger('change');

$("input[type='checkbox']").change(
		function() {
			if ($(this).is(":checked")) {
				$("#sale").show();
				$("input[type='date']").prop("required", true);
				$("#sale-course").attr("data-validetta",
						"required,regExp[numberInteger]");
				var now = new Date();
				var endDate = new Date(now.getTime() + 30 * 24 * 3600 * 1000);
				$("#start-date-sale").attr("value", formatDate(now));
				$("#end-date-sale").attr("value", formatDate(endDate));
			} else {
				$("#sale").hide();
				$("input[type='date']").removeAttr("required");
				$("#sale-course").removeAttr("data-validetta");
			}
		});

$("#update-course-myform").validetta({
	bubblePosition : "bottom",
	bubbleGapTop : 10,
	bubbleGapLeft : 80,
	validators : {
		regExp : {
			numberInteger : {
				pattern : /^[\+][0-9]*[1-9][0-9]*?$|^[0-9]*[1-9][0-9]*?$/,
				errorMessage : 'Giá trị phải là số nguyên dương !'
			},

			fileVideo : {
				pattern : /mp4$|mpeg$/,
				errorMessage : 'Lỗi định dạng file'
			}
		}
	}
}, {
	required : 'Trường thông tin rỗng',
	maxLength : 'Độ dài không vượt quá 255'
});

var selectFile;
$(document).on('change', '.fileUpload', function() {
	selectFile = event.target.files[0];
	var indexRow = $(this).attr("indexRow");
	if (typeof (selectFile) === "undefined") {
		$("." + indexRow).removeAttr("data-validetta");
	} else {
		var fileName = selectFile.name;
		if (!fileName.endsWith(".mp4") && !fileName.endsWith(".mpeg")) {
			$(this).attr("data-validetta", "regExp[fileVideo]");
		} else {
			$("." + indexRow).attr("data-validetta", "required");
		}
	}
});

function formatDate(date) {
	var month = (date.getMonth() + 1);
	var day = date.getDate();
	if (month < 10)
		month = "0" + month;
	if (day < 10)
		day = "0" + day;
	var today = date.getFullYear() + '-' + month + '-' + day;
	return today;
}

$("input[type='reset']").on("click", function(event) {
	$(".title-lession").each(function() {
		$(this).prop('disabled', true);
	});
});

var selectFile;
$(".fileUpload").on("change", function(event) {
	selectFile = event.target.files[0];
	var indexRow = $(this).attr("indexRow");
	console.log($("." + indexRow).val());
	if (typeof (selectFile) === "undefined") {
		$("." + indexRow).prop('disabled', true);
		$("." + indexRow).removeAttr("data-validetta");
	} else {
		var fileName = selectFile.name;
		if (!fileName.endsWith(".mp4") && !fileName.endsWith(".mpeg")) {
			$(this).attr("data-validetta", "regExp[fileVideo]");
		} else {
			$("." + indexRow).attr("data-validetta", "required");
			$("." + indexRow).prop('disabled', false);
		}
	}
});

$(function() {
	$('select').selectpicker({
		noneSelectedText : 'Tìm kiếm giáo viên',
		noneResultsText : 'Không có giáo viên phù hợp với {0}'
	});
});