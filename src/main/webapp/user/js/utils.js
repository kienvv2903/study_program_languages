function validatePhone(phone) {
	var regex = /((09|03|07|08|05)+([0-9]{8})\b)/g;
	return regex.test(phone);
}

$(function() {
	var nowTemp = new Date();
	var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(),
			nowTemp.getDate(), 0, 0, 0, 0);
	var checkin = $('#birthDay').datepicker({
		format : 'yyyy-mm-dd',
		onRender : function(date) {
			return date.valueOf() > now.valueOf() ? 'disabled' : '';
		}
	});
});