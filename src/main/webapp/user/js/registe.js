function validateRegister() {
	var error = document.getElementById("error");
	error.innerHTML = '';
	var result = new Array();
	var username = document.getElementById("username").value;
	var name = document.getElementById("name").value;
	var phone = document.getElementById("phone").value;
	var birthday = document.getElementById("birthDay").value;
	var email = document.getElementById("email").value;
	var password = document.getElementById("password").value;
	var passwordAgain = document.getElementById("passwordAgain").value;
	if (username == '') {
		result.push("Tài khoản trống");
	}
	if (name == '') {
		result.push("Họ và tên trống");
	} else if (name.length > 30) {
		result.push("Họ và tên không quá 30 kí tự");
	}
	if (phone == '') {
		result.push("Số điện thoại trống");
	} else if (!validatePhone(phone)) {
		result.push("Số điện thoại không đúng định dạng");
	}
	if (birthday == '') {
		result.push("Ngày sinh trống");
	}
	if (email == '') {
		result.push("Email trống");
	}
	if (password == '') {
		result.push("Mật khẩu trống");
	} else if (password.length < 6) {
		result.push("Độ dài mật khẩu phải lớn hơn 6");
	} else if (password != passwordAgain) {
		result.push("Mật khẩu xác nhận sai");
	}
	if (result.length != 0) {
		$('#error').attr("class", "alert alert-danger");
		for (i = 0; i < result.length; i++) {
			error.innerHTML += result[i] + "<br>";
		}
		window.location.href = "http://localhost:8080/study_program_languages/register#";
		return false;
	}
	var checkUsername = $("#check-username").text();
	var checkEmail = $("#check-email").text();
	if (checkUsername != '' || checkEmail != '') {
		return false;
	}
	return true;
}

$(document).ready(function() {
	var error = 0;
	var x_timer;
	$("#username").keyup(function(e) {
		clearTimeout(x_timer);
		var user_name = $(this).val();
		x_timer = setTimeout(function() {
			check_username_ajax(user_name);
		}, 1000);
	});

	$("#email").keyup(function(e) {
		clearTimeout(x_timer);
		var email = $(this).val();
		x_timer = setTimeout(function() {
			check_email_ajax(email);
		}, 1000);
	});

	function check_email_ajax(email) {
		$.post('checkUser', {
			'email' : email
		}, function(data) {
			$("#check-email").html(data);
		});
	}

	function check_username_ajax(username) {
		$.post('checkUser', {
			'username' : username
		}, function(data) {
			$("#check-username").html(data);
		});
	}
});