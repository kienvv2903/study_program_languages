var selectFile;
$("#upFile").on(
		"change",
		function(event) {
			selectFile = event.target.files[0];
			var fileName = selectFile.name;
			if (fileName.endsWith(".png") || fileName.endsWith(".jpg")) {
				uploadFile();
				$("#fileName").text(selectFile.name);
			} else {
				$("#fileName").html(
						"<span style=\"color :red;\">Định dạng file không hợp lệ."
								+ "Vui lòng chọn ảnh png, jpg</span>");
			}
		});

function uploadFile() {
	var fileName = selectFile.name;
	var username = document.getElementById("username").value;
	var storageRef = firebase.storage().ref(
			'/avatar/' + username + '/' + fileName);
	var uploadTask = storageRef.put(selectFile);
	uploadTask.on('state_changed', function(snapshot) {
		var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
		$(".progress-bar").css("width", progress + "%");
		$('#update-infor').attr("disabled", true);
		switch (snapshot.state) {
		case firebase.storage.TaskState.PAUSED: // or 'paused'
			console.log('Upload is paused');
			break;
		case firebase.storage.TaskState.RUNNING: // or 'running'
			console.log('Upload is running');
			break;
		}
	}, function(error) {
		$("#errorUpload").text('Hệ thống đang lỗi.Vui lòng thử lại');
	}, function() {
		uploadTask.snapshot.ref.getDownloadURL().then(function(downloadURL) {
			$("#urlImg").attr("value",downloadURL);
			$(".progress-bar").hide();
			$('#update-infor').attr("disabled", false);
			$(".progress-bar").css("width", "0%");
			setTimeout(function() {
				$(".progress-bar").show();
			}, 1000);
		});
	});
}
