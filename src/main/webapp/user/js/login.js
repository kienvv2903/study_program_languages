function validateLogin() {
	var error = document.getElementById("error");
	error.innerHTML = '';
	var result = [];
	var loginName = document.getElementById("username").value;
	var password = document.getElementById("password").value;
	if (loginName == '') {
		result.push("Tài khoản trống");
	}
	if (password == '') {
		result.push("Mật khẩu trống");
	}
	if (result.length != 0) {
		$('#error').attr("class", "alert alert-danger");
		for (i = 0; i < result.length; i++) {
			error.innerHTML += result[i] + "<br>";
		}
		return false;
	}
	return true;
}