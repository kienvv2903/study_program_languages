$(document).ready(function() {
	$("#name").keyup(function() {
		var user_name = $(this).val();
		if (user_name == '') {
			$("#name").addClass('boder-error');
			$(".error-name").text("* Họ và tên trống");
			disableButtonUpdate();
		} else if (user_name.length > 30) {
			$("#name").addClass('boder-error');
			$(".error-name").text("* Họ và tên có độ dài lớn 30");
			disableButtonUpdate();
		} else {
			$("#name").removeClass('boder-error');
			$(".error-name").text("");
			visableButtonUpdate();
		}
	});

	$("#phone").keyup(function() {
		var phone = $(this).val();
		if (phone == '') {
			$("#phone").addClass('boder-error');
			$(".error-phone").text("* Số điện thoại trống");
			disableButtonUpdate();
		} else if (!validatePhone(phone)) {
			$("#phone").addClass('boder-error');
			$(".error-phone").text("* Số điện thoại sai định dạng");
			disableButtonUpdate();
		} else {
			$("#phone").removeClass('boder-error');
			$(".error-phone").text("");
			visableButtonUpdate();
		}
	});

	$("#birthday").keyup(function() {
		var birthday = $(this).val();
		if (birthday == '') {
			$("#birthday").addClass('boder-error');
			$(".error-birthday").text("* Ngày sinh trống");
			disableButtonUpdate();
		} else {
			$("#phone").removeClass('boder-error');
			$(".error-birthday").text("");
			visableButtonUpdate();
		}
	});

	$("#email").keyup(function() {
		var email = $(this).val();
		if (email == '') {
			$("#email").addClass('boder-error');
			$(".error-email").text("* Email trống");
			disableButtonUpdate();
		} else {
			$("#email").removeClass('boder-error');
			check_email_ajax(email);

		}
	});

	function check_email_ajax(email) {
		$.post('checkExistEmail', {
			'email' : email
		}, function(data) {
			$(".error-email").html(data);
			if (data == '' || data == null) {
				visableButtonUpdate();
			} else {
				disableButtonUpdate();
			}
		});
	}
});
function disableButtonUpdate() {
	$("#update-infor").attr("disabled", true);
}

function visableButtonUpdate() {
	$("#update-infor").attr("disabled", false);
}

$("#reset-data").click(function() {
	$("form").find("span.err").each(function() {
		$(this).text('');
	});

	$("form").find("input").each(function() {
		$(this).removeClass('boder-error');
	});
	visableButtonUpdate();
});
