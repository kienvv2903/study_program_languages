package logic;

import java.util.List;

import modal.Course;
import modal.CourseDetail;

public interface CourseLogic {
	public List<Course> getAllCourses(String type, int start, int limit);

	public List<Course> searchCourse(String keySearch, String type, int start, int limit);
	
	public long getCountCourse(String keySearch);

	public boolean checkExistCourse(int courseId);

	public Course getDetailCourse(int courseId);

	public boolean checkRegistedCourse(int courseId, String username);

	public int getTimeStudyOfCourseByCourseId(int courseId);

	public String registeCourse(CourseDetail courseDetail);

	public int getPriceCourse(int courseId);

	public List<Course> getAllCourseForUser(String username);

	public List<Course> getListCourseConcern(Course course);

	public boolean createCourse(Course course);
}
