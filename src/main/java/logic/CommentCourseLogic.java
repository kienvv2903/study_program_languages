package logic;

import java.util.List;

import modal.CommentCourse;

public interface CommentCourseLogic {

	public List<CommentCourse> getListCommentWithLimitByCourseId(int courseId);

	public boolean createCommentCourse(CommentCourse commentCourse);

	public CommentCourse getCommentNewest(int courseId, String username);
}
