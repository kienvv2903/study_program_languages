package logic;

import java.util.List;

import modal.Notification;

public interface NotificationLogic {
	public List<Notification> getListNotificationsWithLimit(int start, int limit);
	
	public boolean creatNotification(Notification notification);
	
	public long getCountNotification();
	
	public boolean addNoticeRegisteCourseSuccess(Notification notification);
	
	public long getCountNoticeUnreadForUser(String username);
	
	public List<Notification> getAllNoticeForUser(String username);
	
	public boolean updateReadNoticeByUser(String username);
	
}
