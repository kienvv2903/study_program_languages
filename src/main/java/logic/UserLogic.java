package logic;

import java.security.NoSuchAlgorithmException;
import java.util.List;

import modal.User;

public interface UserLogic {

	/**
	 * Phương thức thêm đối tượng User vào CSDL
	 * 
	 * @param user
	 * @return
	 */
	public boolean insertUser(User user);

	/**
	 * Phương thức cập nhật thông tin người dùng
	 * 
	 * @param user
	 * @return
	 */
	public boolean updateInforUser(User user);

	/**
	 * 
	 * @param username
	 * @param password
	 * @return
	 * @throws NoSuchAlgorithmException
	 */
	public int checkAccount(String username, String password) throws NoSuchAlgorithmException;

	/**
	 * Phương thức kiểm tra tên tài khoản đã tồn tại trong CSDL hay không?
	 * 
	 * @param username
	 *            tên tài khoản kiểm tra
	 * @return true nếu đã tồn tại và ngược lại
	 */
	public boolean checkExistUsername(String username);

	/**
	 * Phương thức kiểm tra email đã tồn tại trong CSDL hay không?
	 * 
	 * @param email
	 *            tên tài khoản kiểm tra
	 * @return true nếu đã tồn tại và ngược lại
	 */
	public boolean checkExistEmail(String email);

	public boolean checkExistEmailNotOfUser(String username, String email);

	public User getInforUserByUserName(String username);

	public int getBalanceAccount(String username);

	public List<User> getListUser(String keySearch, int start, int limit);

	public long getCountUser(String keySearch);

	public String rechargeAccount(String username, int balance);

	public List<String> getListUsernameTeacher();

	public String searchTeacher(String keySearch);
	
	public String getNameTeacherByUsername(String username);
}
