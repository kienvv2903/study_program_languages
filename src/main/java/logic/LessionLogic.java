package logic;

import java.util.List;

import modal.Lession;

public interface LessionLogic {
	public List<Lession> getAllLessionByCourseId(int courseId);
}
