package logic.impl;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import dao.CourseDao;
import dao.UserDao;
import dao.impl.CourseDaoImpl;
import dao.impl.UserDaoImpl;
import logic.CourseLogic;
import logic.UserLogic;
import modal.Course;
import modal.CourseDetail;
import modal.Notification;
import modal.User;
import util.Constant;

public class CourseLogicImpl implements CourseLogic {

	private static CourseDao courseDaoImpl = null;

	/**
	 * Phương thức khởi tạo CourseLogicImpl
	 */
	public CourseLogicImpl() {
		if (courseDaoImpl == null) {
			courseDaoImpl = new CourseDaoImpl();
		}
	}

	@Override
	public List<Course> getAllCourses(String type, int start, int limit) {
		return courseDaoImpl.getAllCourses(type, start, limit);
	}

	@Override
	public boolean checkExistCourse(int courseId) {
		return courseDaoImpl.checkExistCourse(courseId);
	}

	@Override
	public Course getDetailCourse(int courseId) {
		return courseDaoImpl.getDetailCourse(courseId);
	}

	@Override
	public boolean checkRegistedCourse(int courseId, String username) {
		return courseDaoImpl.checkRegistedCourse(courseId, username);
	}

	@Override
	public int getTimeStudyOfCourseByCourseId(int courseId) {
		return courseDaoImpl.getTimeStudyOfCourseByCourseId(courseId);
	}

	@Override
	public String registeCourse(CourseDetail courseDetail) {
		String username = courseDetail.getUserName();
		UserLogic userLogicImpl = new UserLogicImpl();
		// Nếu người dùng tồn tại
		if (userLogicImpl.checkExistUsername(username)) {
			int courseId = courseDetail.getCourseId();
			// Nếu khóa học hợp lệ
			if (courseId > Constant.ERROR && checkExistCourse(courseId)) {
				// Nếu đã đăng ký khóa học
				if (checkRegistedCourse(courseId, username)) {
					return Constant.ERR_REGISTED_COURSE;
				} else {
					int priceCourse = courseDaoImpl.getPriceCourse(courseId);
					int balance = userLogicImpl.getBalanceAccount(username);
					// Nếu tài khoản đủ phí
					if (priceCourse <= balance) {
						CourseDao courseDaoImpl = new CourseDaoImpl();
						Course course = new Course();
						Set<CourseDetail> courseDetails = new HashSet<>();
						UserDao userDaoImpl = new UserDaoImpl();
						User user = userDaoImpl.getInforUserByUserName(username);
						user.setBalance(balance - priceCourse);
						courseDetail.setUser(user);
						courseDetails.add(courseDetail);
						course.setCourseDetails(courseDetails);
						Notification notification = new Notification();
						notification.setUsername(username);
						notification.setCourseId(courseId);
						notification.setDescription("Đăng ký khóa học thành công! Tài khoản của bạn bị trừ "
								+ priceCourse + " đ. Số dư hiện tại là : " + (balance - priceCourse) + " đ.");
						notification.setType(Constant.REGISTE_SUCCESS);
						notification.setStatus(Constant.UNREAD);
						notification.setCreatAt(new Date());
						course.setNotification(notification);
						if (courseDaoImpl.registeCourse1(course)) {
							return Constant.REGISTE_COURSE_SUCCESS;
						}
						return Constant.ERR_SYSTEM;
						// Ngược lại, tài khoản không đủ phí
					} else {
						return Constant.NOT_MONEY_PAY_COURSE;
					}
				}
				// Ngược lại, nếu khóa học không tồn tại
			} else {
				return Constant.ERR_NOT_EXIST_COURSE;
			}
			// Ngược lại, người dùng không tồn tại
		} else {
			return Constant.ERR_USERNAME;
		}
	}

	@Override
	public int getPriceCourse(int courseId) {
		return courseDaoImpl.getPriceCourse(courseId);
	}

	@Override
	public List<Course> getAllCourseForUser(String username) {
		return courseDaoImpl.getAllCourseForUser(username);
	}

	@Override
	public List<Course> getListCourseConcern(Course course) {
		return courseDaoImpl.getListCourseConcern(course);
	}

	@Override
	public boolean createCourse(Course course) {
		return courseDaoImpl.createCourse(course);
	}

	@Override
	public List<Course> searchCourse(String keySearh, String type, int start, int limit) {
		return courseDaoImpl.searchCourse(keySearh, type, start, limit);
	}

	@Override
	public long getCountCourse(String keySearch) {
		return courseDaoImpl.getCountCourse(keySearch);
	}
}
