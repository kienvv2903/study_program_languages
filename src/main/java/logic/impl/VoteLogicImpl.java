package logic.impl;

import java.util.Collections;
import java.util.List;

import dao.VoteDao;
import dao.impl.VoteDaoImpl;
import logic.VoteLogic;
import modal.Rate;
import modal.Vote;
import util.Common;

public class VoteLogicImpl implements VoteLogic {
	private static VoteDao voteDaoImpl = null;

	/**
	 * Phương thức khởi tạo VoteLogicImpl
	 */
	public VoteLogicImpl() {
		if (voteDaoImpl == null) {
			voteDaoImpl = new VoteDaoImpl();
		}
	}

	@Override
	public List<Vote> getListVoteWithLimit(int courseId,int start, int limit) {
		return voteDaoImpl.getListVoteWithLimit(courseId, start, limit);
	}

	@Override
	public List<Rate> getListRateByCourseId(int courseId) {
		List<Rate> listRates = voteDaoImpl.getListRateByCourseId(courseId);
		List<Integer> listStars = Common.getListStars(listRates);
		for (int i = 1; i <= 5; i++) {
			if (!listStars.contains(i)) {
				listRates.add(new Rate(i, 0));
			}
		}
		Collections.sort(listRates, Rate.rateCompator);
		return listRates;
	}

	@Override
	public int checkExistVote(int courseId, String username) {
		return voteDaoImpl.checkExistVote(courseId, username);
	}

	@Override
	public boolean saveVote(Vote vote) {
		return voteDaoImpl.saveVote(vote);
	}

	@Override
	public boolean updateVote(Vote vote) {
		return voteDaoImpl.updateVote(vote);
	}
}
