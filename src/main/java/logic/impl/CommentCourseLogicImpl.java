package logic.impl;

import java.util.List;

import dao.CommentCourseDao;
import dao.impl.CommentCourseDaoImpl;
import logic.CommentCourseLogic;
import modal.CommentCourse;

public class CommentCourseLogicImpl implements CommentCourseLogic {

	private static CommentCourseDao commentCourseDaoImpl = null;

	/**
	 * Phương thức khởi tạo CourseLogicImpl
	 */
	public CommentCourseLogicImpl() {
		if (commentCourseDaoImpl == null) {
			commentCourseDaoImpl = new CommentCourseDaoImpl();
		}
	}

	@Override
	public List<CommentCourse> getListCommentWithLimitByCourseId(int courseId) {
		return commentCourseDaoImpl.getListCommentWithLimitByCourseId(courseId);
	}

	@Override
	public boolean createCommentCourse(CommentCourse commentCourse) {
		return commentCourseDaoImpl.createCommentCourse(commentCourse);
	}

	@Override
	public CommentCourse getCommentNewest(int courseId, String username) {
		return commentCourseDaoImpl.getCommentNewest(courseId, username);
	}

}
