package logic.impl;

import java.util.List;

import dao.NotificationDao;
import dao.impl.NotificationDaoImpl;
import logic.NotificationLogic;
import modal.Notification;
import util.ConfigProperties;
import util.Constant;

public class NotificationLogicImpl implements NotificationLogic {

	private static NotificationDao notificationDao = null;

	/**
	 * Phương thức khởi tạo LessionLogicImpl
	 */
	public NotificationLogicImpl() {
		if (notificationDao == null) {
			notificationDao = new NotificationDaoImpl();
		}
	}

	@Override
	public List<Notification> getListNotificationsWithLimit(int start, int limit) {
		List<Notification> listNotifications = notificationDao.getListNotificationsWithLimit(start, limit);
		for (Notification notification : listNotifications) {
			switch (notification.getType()) {
			case Constant.COURSE_NEW:
				notification.setTypeStr(ConfigProperties.getData(ConfigProperties.NOTIFICATION_COURSE_NEW));
				break;
			case Constant.COURSE_SALE:
				notification.setTypeStr(ConfigProperties.getData(ConfigProperties.NOTIFICATION_COURSE_SALE));
				break;
			default:
				notification.setTypeStr(ConfigProperties.getData(ConfigProperties.NOTIFICATION));
				break;
			}
		}
		return listNotifications;
	}

	@Override
	public boolean creatNotification(Notification notification) {
		return notificationDao.createNotification(notification);
	}

	@Override
	public long getCountNotification() {
		return notificationDao.getCountNotification();
	}

	@Override
	public boolean addNoticeRegisteCourseSuccess(Notification notification) {
		return notificationDao.addNoticeRegisteCourseSuccess(notification);
	}

	@Override
	public long getCountNoticeUnreadForUser(String username) {
		return notificationDao.getCountNoticeUnreadForUser(username);
	}

	@Override
	public List<Notification> getAllNoticeForUser(String username) {
		return notificationDao.getAllNoticeForUser(username);
	}

	@Override
	public boolean updateReadNoticeByUser(String username) {
		return notificationDao.updateReadNoticeByUser(username);
	}

}
