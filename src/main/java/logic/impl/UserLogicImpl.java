package logic.impl;

import java.security.NoSuchAlgorithmException;
import java.util.List;

import com.google.gson.Gson;

import dao.UserDao;
import dao.impl.UserDaoImpl;
import logic.UserLogic;
import modal.User;
import util.Constant;

public class UserLogicImpl implements UserLogic {

	private static UserDao userDaoImpl = null;

	/**
	 * Phương thức khởi tạo UserLogicImpl
	 */
	public UserLogicImpl() {
		if (userDaoImpl == null) {
			userDaoImpl = new UserDaoImpl();
		}
	}

	@Override
	public boolean insertUser(User user) {
		return userDaoImpl.insertUser(user);
	}

	@Override
	public boolean updateInforUser(User user) {
		return userDaoImpl.updateInforUser(user);
	}

	@Override
	public int checkAccount(String username, String password) throws NoSuchAlgorithmException {
		return userDaoImpl.checkAccount(username, password);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see logic.UserLogic#checkExistUsername(java.lang.String)
	 */
	@Override
	public boolean checkExistUsername(String username) {
		return userDaoImpl.checkExistUsername(username);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see logic.UserLogic#checkExistEmail(java.lang.String)
	 */
	@Override
	public boolean checkExistEmail(String email) {
		return userDaoImpl.checkExistEmail(email);
	}

	@Override
	public boolean checkExistEmailNotOfUser(String username, String email) {
		return userDaoImpl.checkExistEmailNotOfUser(username, email);
	}

	@Override
	public User getInforUserByUserName(String username) {
		return userDaoImpl.getInforUserByUserName(username);
	}

	@Override
	public int getBalanceAccount(String username) {
		return userDaoImpl.getBalanceAccount(username);
	}

	@Override
	public List<User> getListUser(String keySearch, int start, int limit) {
		return userDaoImpl.getListUser(keySearch, start, limit);
	}

	@Override
	public long getCountUser(String keySearch) {
		return userDaoImpl.getCountUser(keySearch);
	}

	@Override
	public String rechargeAccount(String username, int balance) {
		if (checkExistUsername(username)) {
			int balanceCurrent = getBalanceAccount(username);
			if (userDaoImpl.updateBalanceAccountForAdmin(username, balanceCurrent + balance)) {
				return Constant.UPDATE_BALANCE_SUCCESS;
			} else {
				return Constant.ERR_SYSTEM;
			}
		} else {
			return Constant.ERR_USERNAME;
		}
	}

	@Override
	public List<String> getListUsernameTeacher() {
		return userDaoImpl.getListUsernameTeacher();
	}

	@Override
	public String searchTeacher(String keySearch) {
		List<User> users = userDaoImpl.searchTeacher(keySearch);
		String jsonUsers = new Gson().toJson(users);
		return jsonUsers;
	}

	@Override
	public String getNameTeacherByUsername(String username) {
		return userDaoImpl.getNameTeacherByUsername(username);
	}
}
