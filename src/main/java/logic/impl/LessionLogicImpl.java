package logic.impl;

import java.util.List;

import dao.LessionDao;
import dao.impl.LessionDaoImpl;
import logic.LessionLogic;
import modal.Lession;

public class LessionLogicImpl implements LessionLogic {

	private static LessionDao lessionDaoImpl = null;

	/**
	 * Phương thức khởi tạo LessionLogicImpl
	 */
	public LessionLogicImpl() {
		if (lessionDaoImpl == null) {
			lessionDaoImpl = new LessionDaoImpl();
		}
	}

	@Override
	public List<Lession> getAllLessionByCourseId(int courseId) {
		return lessionDaoImpl.getAllLessionByCourseId(courseId);
	}

}
