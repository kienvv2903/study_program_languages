package logic;

import java.util.List;

import modal.Rate;
import modal.Vote;

public interface VoteLogic {
	
	public List<Vote> getListVoteWithLimit(int courseId,int start, int limit);
	
	public List<Rate> getListRateByCourseId(int courseId);
	
	public int checkExistVote(int courseId, String username);
	
	public boolean saveVote(Vote vote);
	
	public boolean updateVote(Vote vote);
	
}
