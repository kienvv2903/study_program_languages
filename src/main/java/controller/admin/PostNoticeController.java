package controller.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import logic.NotificationLogic;
import logic.impl.NotificationLogicImpl;
import modal.Notification;
import util.Constant;
import util.StringUtils;

public class PostNoticeController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -798107825812646161L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		try {
			request.getRequestDispatcher(Constant.POST_NOTICE_VIEW).forward(request, response);
		} catch (Exception e) {
			response.sendRedirect(Constant.ERROR_URL);
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		HttpSession session = request.getSession();
		try {
			String admin = (String) session.getAttribute("admin");
			if (!StringUtils.isNull(admin)) {
				String content = request.getParameter("content");
				Notification notification = new Notification(admin, content, Constant.NORMAL);
				NotificationLogic notificationLogicImpl = new NotificationLogicImpl();
				if (notificationLogicImpl.creatNotification(notification)) {
					request.getRequestDispatcher(Constant.POST_NOTICE_VIEW).forward(request, response);
				} else {
					response.sendRedirect(Constant.ERROR_URL);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.sendRedirect(Constant.ERROR_URL);
		}
	}
}
