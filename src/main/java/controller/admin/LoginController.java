package controller.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import logic.UserLogic;
import logic.impl.UserLogicImpl;
import util.Constant;
import util.MessageErrorProperties;


public class LoginController extends HttpServlet{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4274225182247939198L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		try {
			request.getRequestDispatcher(Constant.LOGIN_ADMIN_VIEW).forward(request, response);
		} catch (Exception e) {
			response.sendRedirect(Constant.ERROR_URL);
		}
	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		try {
			String username = request.getParameter("username");
			String password = request.getParameter("password");
			UserLogic userLogicImpl = new UserLogicImpl();
			int rule = userLogicImpl.checkAccount(username, password);
			if (Constant.RULE_ADMIN == rule) {
				HttpSession session = request.getSession();
				session.setAttribute("admin", username);
				session.setAttribute("rule", rule);
				response.sendRedirect(Constant.HOME_ADMIN_URL);
			} else {
				request.setAttribute("error", MessageErrorProperties.getData(Constant.ERROR_ACCOUNT));
				request.setAttribute("username", username);
				request.getRequestDispatcher(Constant.LOGIN_ADMIN_VIEW).forward(request, response);
			}
		} catch (Exception e) {
			response.sendRedirect(Constant.ERROR_URL);
		}
	}
}
