package controller.admin;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import logic.UserLogic;
import logic.impl.UserLogicImpl;
import modal.User;
import util.Common;
import util.Constant;
import util.StringUtils;

public class ManagerUserController extends HttpServlet {

	// private static final int START_RECORD = 0;
	private static final int LIMIT_RECORD = 10;

	/**
	 * 
	 */
	private static final long serialVersionUID = 6541868219803037992L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		boolean error = false;
		try {
			HttpSession session = request.getSession();
			int currentPage = 1;
			int limit = LIMIT_RECORD;
			String keySearch = "";
			String type = request.getParameter("type");
			String pageStr = request.getParameter("page");
			if (StringUtils.isNull(type)) {
				currentPage = Common.convertStringToInteger(pageStr, 1);
				keySearch = "";
				removeSession(session);
			} else if ("search".equals(type)) {
				keySearch = Common.convertString(request.getParameter("keySearch"), "");
				currentPage = 1;
				limit = Common.convertStringToInteger(request.getParameter("limit"), LIMIT_RECORD);
				session.setAttribute("keySearch", keySearch);
				session.setAttribute("limit", limit);
			} else if ("paging".equals(type)) {
				int page = Common.isInteger(pageStr);
				if (page > Constant.ERROR) {
					currentPage = page;
					keySearch = Common.convertString((String) session.getAttribute("keySearch"), "");
					if (session.getAttribute("limit") == null) {
						limit = LIMIT_RECORD;
					} else {
						limit = Common.formatLimitRecord((int) session.getAttribute("limit"), LIMIT_RECORD);
					}
				} else {
					error = true;
					removeSession(session);
				}
			} else {
				error = true;
				removeSession(session);
			}
			if (!error) {
				int startRecord = (currentPage - 1) * limit;
				UserLogic userLogicImpl = new UserLogicImpl();
				long countUser = userLogicImpl.getCountUser(keySearch);
				List<User> listUsers = userLogicImpl.getListUser(keySearch, startRecord, limit);
				List<Integer> listPagings = Common.getListPaging((int) countUser, currentPage, limit);
				int totalPage;
				if (countUser % limit == 0) {
					totalPage = (int) countUser / limit;
				} else {
					totalPage = (int) countUser / limit + 1;
				}
				request.setAttribute("totalPage", totalPage);
				request.setAttribute("limit", limit);
				request.setAttribute("keySearch", keySearch);
				request.setAttribute("countUser", countUser);
				request.setAttribute("listUsers", listUsers);
				request.setAttribute("listPagings", listPagings);
				request.setAttribute("currentPage", currentPage);
			}
			request.setAttribute("error", error);
			request.getRequestDispatcher(Constant.MANAGER_USERS_VIEW).forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
			response.sendRedirect(Constant.ERROR_URL);
		}
	}

	private void removeSession(HttpSession session) {
		session.removeAttribute("keySearch");
		session.removeAttribute("limit");
	}
}
