package controller.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import logic.UserLogic;
import logic.impl.UserLogicImpl;

public class SearchTeacherController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9023227356777287591L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String keySearch = request.getParameter("keySearch");
		UserLogic userLogicImpl = new UserLogicImpl();
		String jsonData = userLogicImpl.searchTeacher(keySearch);
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(jsonData);
	}
}
