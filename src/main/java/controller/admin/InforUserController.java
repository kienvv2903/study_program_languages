package controller.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import logic.UserLogic;
import logic.impl.UserLogicImpl;
import modal.User;
import util.Constant;

public class InforUserController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5309341660550667770L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		String username = request.getParameter("username");
		UserLogic userLogicImpl = new UserLogicImpl();
		User user = userLogicImpl.getInforUserByUserName(username);
		StringBuilder result = new StringBuilder();
		result.append(
				"<div class=\"modal-dialog\" id=\"modal-infor-user\"><div class=\"modal-content\"><div class=\"modal-header\">");
		result.append("<a href=\"#close\" rel=\"modal:close\"><button type=\"button\" "
				+ "class=\"close\" data-dismiss=\"modal\">&times;</button></a>"
				+ "<h4 class=\"modal-title\">Thông tin chi tiết tài khoản</h4></div>");
		result.append("<div class=\"modal-body\">");
		result.append("<div class=\"form-group\"><label>Tên tài khoản : </label> <input type=\"text\" id=\"username\" "
				+ "class=\"form-control\" disabled=\"disabled\" value=\"" + username + "\"></div>");
		result.append("<div class=\"form-group\"><label>Họ và tên : </label> <input type=\"text\" "
				+ "class=\"form-control\" disabled=\"disabled\" value=\"" + user.getName() + "\"></div>");
		result.append("<div class=\"form-group\"><label>Giới tính : </label> <input type=\"text\""
				+ " class=\"form-control\" disabled=\"disabled\" value=\"" + user.getSex() + "\"></div>");
		result.append("<div class=\"form-group\"><label>Số điện thoại : </label> <input type=\"text\""
				+ " class=\"form-control\" disabled=\"disabled\" value=\"" + user.getBirthday() + "\"></div>");
		result.append("<div class=\"form-group\"><label>Email : </label> <input type=\"text\""
				+ " class=\"form-control\" disabled=\"disabled\" value=\"" + user.getEmail() + "\"> </div>");
		result.append("<div class=\"form-group\"><label>Số dư : </label> <input type=\"text\" "
				+ "class=\"form-control\" disabled=\"disabled\" value=\"" + user.getBalance() + "\"></div>");
		String typeUser = "Người dùng thường";
		switch (user.getRule()) {
		case Constant.RULE_TEACHER:
			typeUser = "Giáo viên";
			break;
		default:
			typeUser = "Người dùng thường";
			break;
		}
		result.append("<div class=\"form-group\"><label>Đối tượng : </label> <input type=\"text\" "
				+ "class=\"form-control\" disabled=\"disabled\" value=\"" + typeUser + "\"></div></div>");
		result.append("<div class=\"modal-footer\"><a href=\"#close\" rel=\"modal:close\"><button type=\"button\" "
				+ "class=\"btn btn-default\">Close</button></a></div></div></div>");
		response.getWriter().write(result.toString());
	}
}
