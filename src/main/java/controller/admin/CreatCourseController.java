package controller.admin;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import logic.CourseLogic;
import logic.UserLogic;
import logic.impl.CourseLogicImpl;
import logic.impl.UserLogicImpl;
import modal.Course;
import modal.Lession;
import modal.Notification;
import util.Common;
import util.Constant;
import util.MessageProperties;
import util.StringUtils;

@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 10, maxFileSize = 1024 * 1024 * 30, maxRequestSize = 1024 * 1024
		* 50)
public class CreatCourseController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8870792780659352432L;
	private static final SimpleDateFormat yyyyMMdd = new SimpleDateFormat("yyyy-MM-dd");
	private static final SimpleDateFormat ddMMyyyy = new SimpleDateFormat("dd/MM/yyyy");

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		try {
			HttpSession session = request.getSession();
			request.getRequestDispatcher(Constant.CREAT_COURSE_VIEW).forward(request, response);
			session.removeAttribute("creatCourseSuccess");
		} catch (Exception e) {
			response.sendRedirect(Constant.ERROR_URL);
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			if (uploadVideo(request)) {
				HttpSession session = request.getSession();
				response.sendRedirect(Constant.CREAT_COURSE_URL);
				session.setAttribute("creatCourseSuccess",
						MessageProperties.getData(MessageProperties.CREAT_COURSE_SUCCESS));
			} else {
				response.sendRedirect(Constant.ERROR_URL);
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.sendRedirect(Constant.ERROR_URL);
		}
	}

	private boolean uploadVideo(HttpServletRequest request) throws Exception {
		if (ServletFileUpload.isMultipartContent(request)) {
			String uploadPath = request.getServletContext().getRealPath("") + File.separator
					+ Constant.UPLOAD_DIRECTORY;
			// Tạo thư mục lưu file nếu chưa tồn tại
			String pathVideo = uploadPath + File.separator + Constant.UPLOAD_VIDEO_DIRECTORY;
			String pathImage = uploadPath + File.separator + Constant.UPLOAD_IMAGE_DIRECTORY;
			String urlImage = "";
			File uploadVideo = new File(pathVideo);
			if (!uploadVideo.exists()) {
				uploadVideo.mkdir();
			}

			File uploadImage = new File(pathImage);
			if (!uploadImage.exists()) {
				uploadImage.mkdir();
			}

			Map<String, String> map = new HashMap<>();
			List<String> titleLessions = new ArrayList<>();
			List<String> urlLessions = new ArrayList<>();
			List<Integer> typeLessions = new ArrayList<>();
			FileItemFactory factory = new DiskFileItemFactory();
			ServletFileUpload upload = new ServletFileUpload(factory);
			List<FileItem> formItems = upload.parseRequest(request);

			if (formItems != null && formItems.size() > 0) {
				for (FileItem item : formItems) {
					if (!item.isFormField()) {
						String nameItem = item.getName();
						if (!StringUtils.isNullOrEmpty(nameItem)) {
							String fileName = new File(nameItem).getName();
							String fieldName = item.getFieldName();
							String path = "";
							if ("image-course".equals(fieldName)) {
								path = pathImage + File.separator + fileName;
								urlImage = Constant.UPLOAD_DIRECTORY + File.separator + Constant.UPLOAD_IMAGE_DIRECTORY
										+ File.separator + fileName;
							} else {
								if (!StringUtils.isNullOrEmpty(fileName)) {
									path = pathVideo + File.separator + fileName;
									urlLessions.add(Constant.UPLOAD_DIRECTORY + File.separator
											+ Constant.UPLOAD_VIDEO_DIRECTORY + File.separator + fileName);
								}
							}
							File storeFile = new File(path);
							item.write(storeFile);
						}
						continue;
					} else {
						String fieldname = item.getFieldName();
						String value = item.getString("UTF-8");
						if ("title-lession".equals(fieldname)) {
							if (!StringUtils.isNullOrEmpty(value)) {
								titleLessions.add(value);
							}
						} else if ("type-lession".equals(fieldname)) {
							if (!StringUtils.isNullOrEmpty(value)) {
								typeLessions.add(Common.convertStringToInteger(value, Constant.PAID_LESSION));
							}
						} else {
							map.put(fieldname, value);
						}
					}
				}
			}
			Course course = setDataCourse(map, urlImage);
			Set<Lession> lessions = mergeLession(titleLessions, urlLessions, typeLessions);
			course.setLessions(lessions);
			course.setNotification(creatNotificationNewCourse(course, request));
			CourseLogic courseLogicImpl = new CourseLogicImpl();
			if (courseLogicImpl.createCourse(course)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 
	 * @param titleLessions
	 * @param urlLessions
	 * @return
	 */
	private Set<Lession> mergeLession(List<String> titleLessions, List<String> urlLessions,
			List<Integer> typeLessions) {
		Set<Lession> lessions = new HashSet<>();
		for (int i = 0; i < urlLessions.size(); i++) {
			Lession lession = new Lession();
			lession.setTitle(titleLessions.get(i));
			lession.setUrl(urlLessions.get(i));
			lession.setTypeLession(typeLessions.get(i));
			lessions.add(lession);
		}
		return lessions;
	}

	/**
	 * 
	 * @param map
	 * @param key
	 * @return
	 */
	private String getData(Map<String, String> map, String key) {
		String value = "";
		if (map.containsKey(key)) {
			value = map.get(key);
		}
		return value;
	}

	/**
	 * 
	 * @param map
	 * @return
	 * @throws ParseException
	 */
	private Course setDataCourse(Map<String, String> map, String urlImage) throws ParseException {
		Course course = new Course();
		course.setTitle(getData(map, "title"));
		course.setDescription(getData(map, "description"));
		course.setUrlImage(urlImage);
		course.setTimeStudy(Common.convertStringToInteger(getData(map, "time-study"), 10));
		course.setUserTeacher(getData(map, "teacher"));
		Date creatAt = new Date();
		course.setCreatAt(creatAt);
		course.setPrice(Common.convertStringToInteger(getData(map, "price"), Constant.ZERO));
		String checkSale = getData(map, "check-sale");
		if ("sale".equals(checkSale)) {
			course.setSale(Common.convertStringToDouble(getData(map, "sale-course"), Constant.ZERO));
			course.setStartSale(yyyyMMdd.parse(getData(map, "start-date-sale")));
			course.setEndSale(yyyyMMdd.parse(getData(map, "end-date-sale")));
		} else {
			course.setSale(Constant.ZERO);
			course.setStartSale(null);
			course.setEndSale(null);
		}
		return course;
	}

	/**
	 * 
	 * @param course
	 * @param request
	 * @return
	 */
	private Notification creatNotificationNewCourse(Course course, HttpServletRequest request) {
		Notification notification = new Notification();
		HttpSession session = request.getSession();
		UserLogic userLogicImpl = new UserLogicImpl();
		String nameTeacher = userLogicImpl.getNameTeacherByUsername(course.getUserTeacher());
		String admin = (String) session.getAttribute("admin");
		if (!StringUtils.isNullOrEmpty(admin)) {
			notification.setUsername(admin);
			StringBuilder description = new StringBuilder(
					"Giáo viên " + nameTeacher + " mở khóa học " + course.getTitle() + " với "
							+ course.getLessions().size() + " bài học trong " + course.getTimeStudy() + " ngày.");
			if (course.getSale() > Constant.ZERO) {
				String startSale = ddMMyyyy.format(course.getStartSale());
				String endSale = ddMMyyyy.format(course.getEndSale());
				description.append("Đặc biệt khóa học giảm giá " + course.getSale() + "% từ ngày " + startSale
						+ " đến ngày " + endSale);
			}
			notification.setDescription(description.toString());
			notification.setType(Constant.COURSE_NEW);
			notification.setCreatAt(new Date());
			notification.setStatus(Constant.ZERO);
		}
		return notification;
	}
}
