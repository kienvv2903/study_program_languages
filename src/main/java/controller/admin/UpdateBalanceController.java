package controller.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import logic.UserLogic;
import logic.impl.UserLogicImpl;
import util.Common;
import util.Constant;

public class UpdateBalanceController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8544627257359800041L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		String username = request.getParameter("username");
		int balance = Common.convertStringToInteger(request.getParameter("balance"), Constant.ZERO);
		UserLogic userLogicImpl = new UserLogicImpl();
		String result = userLogicImpl.rechargeAccount(username, balance);
		response.getWriter().write(result);
	}
}
