package controller.admin;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import logic.CourseLogic;
import logic.impl.CourseLogicImpl;
import modal.Course;
import modal.Lession;
import util.Common;
import util.Constant;
import util.StringUtils;

@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 10, maxFileSize = 1024 * 1024 * 30, maxRequestSize = 1024 * 1024
		* 50)
public class UpdateCourseController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1909764027228247441L;
	private static final SimpleDateFormat yyyyMMdd = new SimpleDateFormat("yyyy-MM-dd");

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		try {
			HttpSession session = request.getSession();
			int courseId = Common.isInteger(request.getParameter(Constant.COURSE_ID));
			CourseLogic courseLogicImpl = new CourseLogicImpl();
			if (courseId > Constant.ERROR && courseLogicImpl.checkExistCourse(courseId)) {
				Course course = courseLogicImpl.getDetailCourse(courseId);
				request.setAttribute("course", course);
				request.getRequestDispatcher(Constant.UPDATE_COURSE_VIEW).forward(request, response);
			} else {
				session.setAttribute(Constant.ERR, Constant.ERR_NOT_EXIST_COURSE);
				// Chuyển sang màn hình lỗi với kiểu lỗi khóa học không tồn tại
				response.sendRedirect(Constant.ERROR_URL);
			}
		} catch (Exception e) {
			response.sendRedirect(Constant.ERROR_URL);
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		try {
			if (ServletFileUpload.isMultipartContent(request)) {
				String uploadPath = request.getServletContext().getRealPath("") + File.separator
						+ Constant.UPLOAD_DIRECTORY;
				// Tạo thư mục lưu file nếu chưa tồn tại
				File uploadDir = new File(uploadPath);
				if (!uploadDir.exists()) {
					uploadDir.mkdir();
				}
				Map<String, String> map = new HashMap<>();
				List<String> titleLessions = new ArrayList<>();
				List<String> lessionIds = new ArrayList<>();
				List<String> urlLessions = new ArrayList<>();
				FileItemFactory factory = new DiskFileItemFactory();
				ServletFileUpload upload = new ServletFileUpload(factory);
				List<FileItem> formItems = upload.parseRequest(request);

				// Lọc dữ liệu
				if (formItems != null && formItems.size() > 0) {
					for (FileItem item : formItems) {
						if (!item.isFormField()) {
							String nameItem = item.getName();
							if (!StringUtils.isNullOrEmpty(nameItem)) {
								String fileName = new File(nameItem).getName();
								String filePath = uploadPath + File.separator + fileName;
								urlLessions.add(Constant.UPLOAD_DIRECTORY + File.separator + fileName);
								File storeFile = new File(filePath);
								item.write(storeFile);
							}
							continue;
						} else {
							String fieldname = item.getFieldName();
							String value = item.getString("UTF-8");
							System.out.println("Name : " + fieldname + "---> Value : " + value);
							if ("title-lession".equals(fieldname)) {
								if (!StringUtils.isNullOrEmpty(value)) {
									titleLessions.add(value);
								}
							} else if ("lessionId".equals(fieldname)) {
								lessionIds.add(value);
							} else {
								map.put(fieldname, value);
							}
						}
					}
				}
				// Tách dữ liệu Lession
				Set<Lession> lessionsDeleteOfUpdate = new HashSet<>();
				Set<Lession> lessionsSave = new HashSet<>();
				
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.sendRedirect(Constant.ERROR_URL);
		}
	}

	/**
	 * 
	 * @param map
	 * @return
	 * @throws ParseException
	 */
	private Course setDataCourse(Map<String, String> map) throws ParseException {
		Course course = new Course();
		course.setTitle(getData(map, "title"));
		course.setDescription(getData(map, "description"));
		course.setTimeStudy(Common.convertStringToInteger(getData(map, "time-study"), 10));
		course.setUserTeacher(getData(map, "teacher"));
		Date creatAt = new Date();
		course.setCreatAt(creatAt);
		course.setPrice(Common.convertStringToInteger(getData(map, "price"), Constant.ZERO));
		String checkSale = getData(map, "check-sale");
		if ("sale".equals(checkSale)) {
			course.setSale(Common.convertStringToDouble(getData(map, "sale-course"), Constant.ZERO));
			course.setStartSale(yyyyMMdd.parse(getData(map, "start-date-sale")));
			course.setEndSale(yyyyMMdd.parse(getData(map, "end-date-sale")));
		} else {
			course.setSale(Constant.ZERO);
			course.setStartSale(null);
			course.setEndSale(null);
		}
		return course;
	}

	private String getData(Map<String, String> map, String key) {
		String value = "";
		if (map.containsKey(key)) {
			value = map.get(key);
		}
		return value;
	}
}
