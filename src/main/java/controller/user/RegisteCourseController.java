package controller.user;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import logic.CourseLogic;
import logic.impl.CourseLogicImpl;
import modal.CourseDetail;
import util.Common;
import util.Constant;

public class RegisteCourseController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4888984679313833598L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		try {
			HttpSession session = request.getSession();
			String userLogin = (String) session.getAttribute(Constant.USER_LOGIN);
			CourseLogic courseLogicImpl = new CourseLogicImpl();
			int courseId = Common.isInteger(request.getParameter(Constant.COURSE_ID));
			int timeStudy = courseLogicImpl.getTimeStudyOfCourseByCourseId(courseId);
			Calendar calendar = Calendar.getInstance();
			Date startDate = calendar.getTime();
			calendar.add(Calendar.DATE, timeStudy);
			Date endDate = calendar.getTime();
			CourseDetail courseDetail = new CourseDetail(courseId, userLogin, startDate, endDate);
			String result = courseLogicImpl.registeCourse(courseDetail);
			if (Constant.NOT_MONEY_PAY_COURSE.equals(result)) {
				session.setAttribute(Constant.NOT_MONEY_PAY_COURSE, Constant.NOT_MONEY_PAY_COURSE);
				response.sendRedirect(Constant.COURSE_DETAIL_URL + "?courseId=" + courseId);
			} else if (Constant.REGISTE_COURSE_SUCCESS.equals(result)) {
				session.setAttribute(Constant.REGISTE_COURSE_SUCCESS, Constant.REGISTE_COURSE_SUCCESS);
				response.sendRedirect(Constant.COURSE_DETAIL_URL + "?courseId=" + courseId);
			} else {
				session.setAttribute(Constant.ERR, result);
				response.sendRedirect(Constant.ERROR_URL);
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.sendRedirect(Constant.ERROR_URL);
		}
	}

}
