package controller.user;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import logic.CommentCourseLogic;
import logic.impl.CommentCourseLogicImpl;
import modal.CommentCourse;
import util.Common;
import util.Constant;

public class CommentController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6030344916582572053L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		try {
			String courseIdStr = request.getParameter(Constant.COURSE_ID);
			int courseId = Common.isInteger(courseIdStr);
			String content = request.getParameter(Constant.CONTENT_COMMENT);
			String username = request.getParameter(Constant.USER_LOGIN);
			Date now = Calendar.getInstance().getTime();
			CommentCourse commentCourse = new CommentCourse();
			commentCourse.setCourseId(courseId);
			commentCourse.setContent(content);
			commentCourse.setUsername(username);
			commentCourse.setCreatAt(now);
			CommentCourseLogic commentCourseLogicImpl = new CommentCourseLogicImpl();
			commentCourseLogicImpl.createCommentCourse(commentCourse);
			CommentCourse commentNewest = commentCourseLogicImpl.getCommentNewest(courseId, username);
			StringBuilder comment = new StringBuilder();
			comment.append("<div class=\"comment-list\" id=\"" + courseId + "_" + username + "\">");
			comment.append("<div class=\"single-comment justify-content-between d-flex\">");
			comment.append("<div class=\"user justify-content-between d-flex\">");
			comment.append("<div class=\"thumb\"><img src=\"" + commentNewest.getUser().getUrlImg() + "\"></div>");
			comment.append("<div class=\"desc\"><h5><a href=\"#\">" + username
					+ "</a></h5><p class=\"date\"></p><p class=\"comment\">" + commentNewest.getContent()
					+ "</p></div></div>");
			comment.append("<div class=\"reply-btn\"><a href=\"\" class=\"btn-reply text-uppercase\">reply</a></div>");
			comment.append("</div></div><br>");
			response.getWriter().write(comment.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
