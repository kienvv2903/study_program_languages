package controller.user;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import logic.UserLogic;
import logic.impl.UserLogicImpl;
import modal.User;
import util.Common;
import util.Constant;
import util.MessageErrorProperties;

public class RegisterController extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3150249277834012101L;
	private static final SimpleDateFormat FORMAT = new SimpleDateFormat("yyyy-MM-dd");

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		try {
			request.getRequestDispatcher(Constant.REGISTER_VIEW).forward(request, response);
		} catch (Exception e) {
			response.sendRedirect(Constant.ERROR_URL);
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		try {
			String username = request.getParameter("username");
			String name = request.getParameter("name");
			String sex = request.getParameter("sex");
			String phone = request.getParameter("phone");
			String birthdayStr = request.getParameter("birthday");
			String email = request.getParameter("email");
			UserLogic userLogicImpl = new UserLogicImpl();
			boolean checkExistUsername = userLogicImpl.checkExistUsername(username);
			boolean checkExistEmail = userLogicImpl.checkExistEmail(email);
			if (checkExistUsername || checkExistEmail) {
				List<String> errors = new ArrayList<String>();
				if (checkExistUsername) {
					errors.add(MessageErrorProperties.getData(Constant.ERROR_USERNAME));
				}
				if (checkExistEmail) {
					errors.add(MessageErrorProperties.getData(Constant.ERROR_EMAIL));
				}
				request.setAttribute("errors", errors);
				request.setAttribute("username", username);
				request.setAttribute("name", name);
				request.setAttribute("sex", sex);
				request.setAttribute("phone", phone);
				request.setAttribute("birthday", birthdayStr);
				request.setAttribute("email", email);
				request.getRequestDispatcher(Constant.REGISTER_VIEW).forward(request, response);
			} else {
				String salt = Common.creatSalt(Constant.SALT_LENGTH);
				String password = request.getParameter("password");
				String passEncrypt = Common.encrypt(password, salt);
				Date birthday = new Date(FORMAT.parse(birthdayStr).getTime());
				String urlImg = Constant.URL_DEFAULT_AVATAR_BOY;
				if (Constant.SEX_GIRL.equals(sex)) {
					urlImg = Constant.URL_DEFAULT_AVATAR_GIRL;
				}
				User user = new User(username, name, sex, phone, birthday, email, urlImg, passEncrypt, salt,
						Constant.RULE_USER);
				if (userLogicImpl.insertUser(user)) {
					HttpSession session = request.getSession();
					String from = (String) session.getAttribute(Constant.FROM);
					if (from == null) {
						from = Constant.REGISTER_FROM;
					}
					session.setAttribute(Constant.FROM, from);
					session.setAttribute(Constant.USERNAME, username);
					response.sendRedirect(Constant.LOGIN_URL);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.sendRedirect(Constant.ERROR_URL);
		}
	}
}
