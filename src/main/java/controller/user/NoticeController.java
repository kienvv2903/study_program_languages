package controller.user;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import logic.NotificationLogic;
import logic.impl.NotificationLogicImpl;
import modal.Notification;
import util.Common;
import util.Constant;

public class NoticeController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2399704836487346893L;
	private static final int LIMIT_RECORD = 25;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		// boolean error = false;
		try {
			NotificationLogic notificationLogicImpl = new NotificationLogicImpl();
			int limit = Common.convertStringToInteger(request.getParameter("limit"), LIMIT_RECORD);
			// String pageStr = request.getParameter("page");
			// String type = request.getParameter("type");
			// int currentPage;
			// // Trường hợp mặc định vào ban đầu
			// if (StringUtils.isNullOrEmpty(type)) {
			// currentPage = 1;
			// } else if ("paging".equals(type)) {
			// currentPage = Common.isInteger(pageStr);
			// if (currentPage > Constant.ERROR) {
			//
			// } else {
			// error = true;
			// }
			// } else {
			// error = true;
			// }
			List<Notification> listNotifications = notificationLogicImpl.getListNotificationsWithLimit(0, limit);
			// long countNotice = notificationLogicImpl.getCountNotification();
			// List<Integer> listPagings = Common.getListPaging((int)
			// countNotice, currentPage, limit);
			// request.setAttribute("error", error);
			request.setAttribute("listNotifications", listNotifications);
			request.getRequestDispatcher(Constant.NOTICE_VIEW).forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
			response.sendRedirect(Constant.ERROR_URL);
		}
	}
}
