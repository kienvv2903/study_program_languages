package controller.user;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import util.Common;
import util.Constant;
import util.MessageErrorProperties;

public class ErrorController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1119790496456807202L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		try {
			HttpSession session = request.getSession();
			String error = (String) session.getAttribute(Constant.ERR);
			if (!Common.isNull(error)) {
				switch (error) {
				case Constant.ERR_NOT_EXIST_COURSE:
					error = MessageErrorProperties.getData(Constant.ERR_NOT_EXIST_COURSE);
					break;
				case Constant.ERR_USERNAME:
					error = MessageErrorProperties.getData(Constant.ERR_USERNAME);
					break;
				default:
					break;
				}
				request.setAttribute(Constant.ERR, error);
				session.removeAttribute(Constant.ERR);
			}
			request.getRequestDispatcher(Constant.ERROR_VIEW).forward(request, response);
		} catch (Exception e) {
			response.sendRedirect(Constant.ERROR_URL);
		}
	}

}
