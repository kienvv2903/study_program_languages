package controller.user;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import logic.CourseLogic;
import logic.impl.CourseLogicImpl;
import modal.Course;
import util.Common;
import util.Constant;
import util.StringUtils;

public class CourseController extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3104543062100688636L;
	private static final int LIMIT_RECORD = 8;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		try {
			// CourseLogic courseLogicImpl = new CourseLogicImpl();
			// List<Course> listCourses =
			// courseLogicImpl.getAllCourses(Constant.MOST_INTERESTED, 0, 8);
			// request.setAttribute("listCourses", listCourses);
			boolean error = false;
			HttpSession session = request.getSession();
			int currentPage = 1;
			String typeSort = Constant.MOST_INTERESTED;
			String keySearch = "";
			String type = request.getParameter("type");
			String pageStr = request.getParameter("page");
			if (StringUtils.isNull(type)) {
				currentPage = Common.convertStringToInteger(pageStr, 1);
				keySearch = "";
				removeSession(session);
			} else if ("search".equals(type)) {
				keySearch = Common.convertString(request.getParameter("keySearch"), "");
				currentPage = 1;
				typeSort = Common.convertString(request.getParameter("typeSort"), Constant.MOST_INTERESTED);
				session.setAttribute("keySearch", keySearch);
				session.setAttribute("typeSort", typeSort);
			} else if ("paging".equals(type)) {
				int page = Common.isInteger(pageStr);
				if (page > Constant.ERROR) {
					currentPage = page;
					keySearch = Common.convertString((String) session.getAttribute("keySearch"), "");
					if (session.getAttribute("typeSort") == null) {
						typeSort = Constant.MOST_INTERESTED;
					} else {
						typeSort = Common.convertString((String) session.getAttribute("keySearch"), "");
					}
				} else {
					error = true;
					removeSession(session);
				}
			} else {
				error = true;
				removeSession(session);
			}
			if (!error) {
				int startRecord = (currentPage - 1) * LIMIT_RECORD;
				CourseLogic courseLogicImpl = new CourseLogicImpl();
				List<Course> listCourses = courseLogicImpl.searchCourse(keySearch, typeSort, startRecord, LIMIT_RECORD);
				long countCourse = courseLogicImpl.getCountCourse(keySearch);
				List<Integer> listPagings = Common.getListPaging((int) countCourse, currentPage, LIMIT_RECORD);
				int totalPage;
				if (countCourse % LIMIT_RECORD == 0) {
					totalPage = (int) countCourse / LIMIT_RECORD;
				} else {
					totalPage = (int) countCourse / LIMIT_RECORD + 1;
				}
				request.setAttribute("totalPage", totalPage);
				request.setAttribute("typeSort", typeSort);
				request.setAttribute("limit", LIMIT_RECORD);
				request.setAttribute("keySearch", keySearch);
				request.setAttribute("countCourse", countCourse);
				request.setAttribute("listPagings", listPagings);
				request.setAttribute("currentPage", currentPage);
				request.setAttribute("listCourses", listCourses);
			}
			request.setAttribute("error", error);
			request.getRequestDispatcher(Constant.COURSE_VIEW).forward(request, response);
		} catch (Exception e) {
			response.sendRedirect(Constant.ERROR_URL);
		}
	}

	private void removeSession(HttpSession session) {
		session.removeAttribute("keySearch");
		session.removeAttribute("typeSort");
	}
}
