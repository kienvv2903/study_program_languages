package controller.user;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import logic.UserLogic;
import logic.impl.UserLogicImpl;

public class CheckUserController extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4234919375829635845L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		try {
			UserLogic userLogicImpl = new UserLogicImpl();
			String username = request.getParameter("username");
			if (username != null) {
				if (userLogicImpl.checkExistUsername(username)) {
					response.getWriter().write("<img src=\"user/img/not-available.png\" />" + "Tài khoản đã tồn tại");
				}
			}
			String email = request.getParameter("email");
			if (email != null) {
				if (userLogicImpl.checkExistEmail(email)) {
					response.getWriter().write("<img src=\"user/img/not-available.png\" />" + "Email đã tồn tại");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
