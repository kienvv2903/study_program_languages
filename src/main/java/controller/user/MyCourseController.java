package controller.user;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import logic.CourseLogic;
import logic.impl.CourseLogicImpl;
import modal.Course;
import util.Constant;
import util.StringUtils;

public class MyCourseController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -778061677550701740L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		try {
			CourseLogic courseLogicImpl = new CourseLogicImpl();
			HttpSession session = request.getSession();
			String username = (String) session.getAttribute(Constant.USER_LOGIN);
			if (!StringUtils.isNullOrEmpty(username)) {
				List<Course> myCourses = courseLogicImpl.getAllCourseForUser(username);
				request.setAttribute("myCourses", myCourses);
				request.getRequestDispatcher(Constant.MY_COURSE_VIEW).forward(request, response);
			}else{
				response.sendRedirect(Constant.LOGIN_URL);
			}
		} catch (Exception e) {
			response.sendRedirect(Constant.ERROR_URL);
		}
	}

}
