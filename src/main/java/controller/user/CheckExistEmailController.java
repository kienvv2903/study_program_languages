package controller.user;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import logic.UserLogic;
import logic.impl.UserLogicImpl;
import util.Constant;

public class CheckExistEmailController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 666055286704183134L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		try {
			UserLogic userLogicImpl = new UserLogicImpl();
			String username = (String) request.getSession().getAttribute(Constant.USER_LOGIN);
			String email = request.getParameter("email");
			if (email != null && username != null) {
				String result = "";
				if (userLogicImpl.checkExistEmailNotOfUser(username, email)) {
					result = "<img src=\"user/img/not-available.png\" />" + "Email đã tồn tại";
				}
				response.getWriter().write(result);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
