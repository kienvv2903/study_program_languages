package controller.user;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import util.Constant;

public class LogoutController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3763429589637737702L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		try {
			HttpSession session = request.getSession();
			session.invalidate();
			response.sendRedirect(Constant.HOME_URL);
		} catch (Exception e) {
			response.sendRedirect(Constant.ERROR_URL);
		}
	}
}
