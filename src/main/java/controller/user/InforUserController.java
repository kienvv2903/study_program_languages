package controller.user;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import logic.UserLogic;
import logic.impl.UserLogicImpl;
import modal.User;
import util.Constant;

public class InforUserController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4279195728952513948L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		try {
			HttpSession session = request.getSession();
			String userLogin = (String) session.getAttribute(Constant.USER_LOGIN);
			UserLogic userLogicImpl = new UserLogicImpl();
			User user = userLogicImpl.getInforUserByUserName(userLogin);
			if (user != null) {
				request.setAttribute("user", user);
				request.getRequestDispatcher(Constant.INFOR_USER_VIEW).forward(request, response);
			} else {
				response.sendRedirect(Constant.ERROR_URL);
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.sendRedirect(Constant.ERROR_URL);
		}
	}
}
