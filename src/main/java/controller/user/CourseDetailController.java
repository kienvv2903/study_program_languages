package controller.user;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import logic.CommentCourseLogic;
import logic.CourseLogic;
import logic.VoteLogic;
import logic.impl.CommentCourseLogicImpl;
import logic.impl.CourseLogicImpl;
import logic.impl.VoteLogicImpl;
import modal.CommentCourse;
import modal.Course;
import modal.Rate;
import modal.Vote;
import util.Common;
import util.Constant;

public class CourseDetailController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4081052657491795928L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		try {
			HttpSession session = request.getSession();
			int courseId = Common.isInteger(request.getParameter(Constant.COURSE_ID));
			CourseLogic courseLogicImpl = new CourseLogicImpl();
			if (courseId > Constant.ERROR && courseLogicImpl.checkExistCourse(courseId)) {
				Course course = courseLogicImpl.getDetailCourse(courseId);
				VoteLogic voteLogicImpl = new VoteLogicImpl();
				List<Vote> listVotes = voteLogicImpl.getListVoteWithLimit(courseId, 0, 4);
				List<Rate> listRates = voteLogicImpl.getListRateByCourseId(courseId);
				CommentCourseLogic commentCourseLogicImpl = new CommentCourseLogicImpl();
				List<CommentCourse> listComments = commentCourseLogicImpl.getListCommentWithLimitByCourseId(courseId);
				boolean checkRegistedCourse = courseLogicImpl.checkRegistedCourse(courseId,
						(String) session.getAttribute(Constant.USER_LOGIN));
				List<Course> listCoursesConcern = courseLogicImpl.getListCourseConcern(course);
				request.setAttribute("listCoursesConcern", listCoursesConcern);
				request.setAttribute("sumRating", Common.calSumRatings(listRates));
				request.setAttribute("countRating", Common.getRatings(listRates));
				request.setAttribute("listRates", listRates);
				request.setAttribute("listVotes", listVotes);
				request.setAttribute("course", course);
				request.setAttribute("listComments", listComments);
				request.setAttribute("checkRegistedCourse", checkRegistedCourse);
				request.getRequestDispatcher(Constant.COURSE_DETAIL_VIEW).forward(request, response);
				session.removeAttribute("ratingShow");
				session.removeAttribute("notifi");
				session.removeAttribute(Constant.REGISTE_COURSE_SUCCESS);
				session.removeAttribute(Constant.NOT_MONEY_PAY_COURSE);
			} else {
				session.setAttribute(Constant.ERR, Constant.ERR_NOT_EXIST_COURSE);
				// Chuyển sang màn hình lỗi với kiểu lỗi khóa học không tồn tại
				response.sendRedirect(Constant.ERROR_URL);
			}
		} catch (Exception e) {
			e.printStackTrace();
			// Hệ thống đang lỗi
			response.sendRedirect(Constant.ERROR_URL);
		}
	}
}
