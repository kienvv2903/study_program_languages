package controller.user;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import logic.UserLogic;
import logic.impl.UserLogicImpl;
import modal.User;
import util.Constant;
import util.MessageErrorProperties;
import util.StringUtils;

public class UpdateInforController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8841455872829425195L;
	private static final SimpleDateFormat FORMAT = new SimpleDateFormat("yyyy-MM-dd");

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		try {
			HttpSession session = request.getSession();
			String username = (String) session.getAttribute(Constant.USER_LOGIN);
			if (!StringUtils.isNull(username)) {
				String name = request.getParameter("name");
				String sex = request.getParameter("sex");
				String phone = request.getParameter("phone");
				String birthdayStr = request.getParameter("birthday");
				Date birthday = new Date(FORMAT.parse(birthdayStr).getTime());
				String email = request.getParameter("email");
				String urlImg = request.getParameter("urlImg").toString();
				UserLogic userLogicImpl = new UserLogicImpl();
				boolean checkExistEmail = userLogicImpl.checkExistEmailNotOfUser(username, email);
				User user = new User(username, name, sex, phone, birthday, email, urlImg);
				if (checkExistEmail) {
					List<String> errors = new ArrayList<String>();
					if (checkExistEmail) {
						errors.add(MessageErrorProperties.getData(Constant.ERROR_EMAIL));
					}
					request.setAttribute("user", user);
					request.getRequestDispatcher(Constant.INFOR_USER_VIEW).forward(request, response);
				} else {
					if (userLogicImpl.updateInforUser(user)) {
						response.sendRedirect(Constant.INFOR_USER_URL);
					}
				}
			}
		} catch (Exception e) {
			response.sendRedirect(Constant.ERROR_URL);
		}
	}
}
