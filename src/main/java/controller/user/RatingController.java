package controller.user;

import java.io.IOException;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import logic.VoteLogic;
import logic.impl.VoteLogicImpl;
import modal.Vote;
import util.Common;
import util.Constant;

public class RatingController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 507556556420489302L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		try {
			HttpSession session = request.getSession();
			String username = (String) session.getAttribute(Constant.USER_LOGIN);
			int courseId = Common.isInteger(request.getParameter(Constant.COURSE_ID));
			int score = Common.isScoreRate(request.getParameter("score"));
			String feedBack = Common.formatObjectToString(request.getParameter("feedback"));
			// Kiểm tra định dạng khóa học
			// Khóa học là số nguyên
			// Điểm rating từ 1-5
			if (courseId > Constant.ERROR && score > Constant.ERROR) {
				VoteLogic voteLogicImpl = new VoteLogicImpl();
				int voteId = voteLogicImpl.checkExistVote(courseId, username);
				Vote vote = new Vote(courseId, username, score, feedBack, Calendar.getInstance().getTime());
				// Nếu người dùng đã đánh giá điểm khóa học
				if (voteId > Constant.ZERO) {
					vote.setVoteId(voteId);
					// Tiến hành cập nhật lại điểm rating
					if (voteLogicImpl.updateVote(vote)) {
						session.setAttribute("ratingShow", "ratingShow");
						session.setAttribute("notifi", "notifi");
						response.sendRedirect(Constant.COURSE_DETAIL_URL + "?courseId=" + courseId + "#review");
					}
					// Thêm mới điểm ra ting
				} else {
					if (voteLogicImpl.saveVote(vote)) {
						session.setAttribute("ratingShow", "ratingShow");
						session.setAttribute("notifi", "notifi");
						response.sendRedirect(Constant.COURSE_DETAIL_URL + "?courseId=" + courseId + "#review");
					}
				}
			} else {
				response.sendRedirect(Constant.ERROR_URL);
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.sendRedirect(Constant.ERROR_URL);
		}
	}

}
