package controller.user;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import logic.UserLogic;
import logic.impl.UserLogicImpl;
import util.Common;
import util.Constant;
import util.MessageErrorProperties;
import util.MessageProperties;

public class LoginController extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6551313491394093846L;
	private String message = "";

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		try {
			HttpSession session = request.getSession();
			String from = (String) session.getAttribute(Constant.FROM);
			if (from != null) {
				switch (from) {
				case Constant.REGISTER_FROM:
					message = MessageProperties.getData(MessageProperties.REGISTER_SUCCESS);
					break;
				case Constant.RATING_FROM:
					message = MessageProperties.getData(MessageProperties.LOGIN_FOR_RATING);
					break;
				case Constant.REGISTE_COURSE_FROM:
					message = MessageProperties.getData(MessageProperties.LOGIN_FOR_REGISTE_COURSE);
					break;
				default:
					break;
				}
			}
			request.setAttribute(Constant.MESSAGE, message);
			request.getRequestDispatcher(Constant.LOGIN_USER_VIEW).forward(request, response);
		} catch (Exception e) {
			response.sendRedirect(Constant.ERROR_URL);
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		try {
			String username = request.getParameter("username");
			String password = request.getParameter("password");
			UserLogic userLogicImpl = new UserLogicImpl();
			int rule = userLogicImpl.checkAccount(username, password);
			if (Common.checkRuleUser(rule)) {
				HttpSession session = request.getSession();
				session.setAttribute(Constant.USER_LOGIN, username);
				session.setAttribute(Constant.RULE, rule);
				String from = (String) session.getAttribute(Constant.FROM);
				if (from != null) {
					String courseId = "";
					switch (from) {
					case Constant.REGISTER_FROM:
						response.sendRedirect(Constant.HOME_URL);
						break;
					case Constant.RATING_FROM:
						courseId = (String) session.getAttribute(Constant.COURSE_ID_RATING);
						response.sendRedirect(Constant.COURSE_DETAIL_URL + "?courseId=" + courseId + "#review");
						session.setAttribute(Constant.RATING_SHOW, Constant.RATING_SHOW);
						session.removeAttribute(Constant.COURSE_ID_RATING);
						break;
					case Constant.REGISTE_COURSE_FROM:
						courseId = (String) session.getAttribute(Constant.COURSE_ID_REGISTE);
						response.sendRedirect(Constant.COURSE_DETAIL_URL + "?courseId=" + courseId);
						session.removeAttribute(Constant.COURSE_ID_REGISTE);
						break;
					default:
						response.sendRedirect(Constant.HOME_URL);
						break;
					}
					return;
				}
				response.sendRedirect(Constant.HOME_URL);
				session.removeAttribute(Constant.FROM);
			} else {
				request.setAttribute(Constant.ERROR_NOTIFI, MessageErrorProperties.getData(Constant.ERROR_ACCOUNT));
				request.setAttribute(Constant.USERNAME, username);
				request.setAttribute(Constant.MESSAGE, message);
				request.getRequestDispatcher(Constant.LOGIN_USER_VIEW).forward(request, response);
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.sendRedirect(Constant.ERROR_URL);
		}
	}
}
