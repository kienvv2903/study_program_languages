package controller.user;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import logic.CourseLogic;
import logic.impl.CourseLogicImpl;
import modal.Course;
import util.Common;
import util.Constant;

public class SortCourseController extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1053571959057406647L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		try {
			String typeSort = Common.convertString(request.getParameter("typeSort"), Constant.MOST_INTERESTED);
			String keySearch = Common.convertString(request.getParameter("keySearch"), "");
			int start = Common.convertStringToInteger(request.getParameter("start"), Constant.ZERO);
			CourseLogic courseLogicImpl = new CourseLogicImpl();
			List<Course> listCourses = courseLogicImpl.searchCourse(keySearch, typeSort, start, 8);
			request.setAttribute("listCourses", listCourses);
			StringBuilder sortCourse = new StringBuilder();
			Date now = Calendar.getInstance().getTime();
			for (Course course : listCourses) {
				sortCourse.append("<div class=\"single-popular-carusel col-lg-3 col-md-6\">");
				sortCourse.append("<div class=\"thumb-wrap relative\">");
				sortCourse.append("<div class=\"thumb relative\">");
				sortCourse.append("<div class=\"overlay overlay-bg\"></div>");
				Date endSale = course.getEndSale();
				if (endSale != null && endSale.after(now)) {
					sortCourse.append("<img style=\"width: 32px; height: 32px; position: absolute;"
							+ " right: 0; z-index: 2\" " + "src=\"user/img/sale.png\" alt=\"Giảm giá\">");
				}
				sortCourse.append("<img class=\"img-fluid\" src=\"user/img/p1.jpg\"></div>");
				sortCourse.append("<div class=\"meta d-flex justify-content-between\">");
				sortCourse.append("<p><span class=\"lnr lnr-users\"></span>" + course.getSumVote()
						+ "<span class=\"lnr lnr-bubble\"></span>" + course.getSumVote() + "</p><h4>");
				if (endSale != null && endSale.after(now)) {
					sortCourse.append(
							Common.fomatPriceCourse(course.getPrice() - course.getPrice() * course.getSale() / 100)
									+ " đ");
				} else {
					sortCourse.append(Common.fomatPriceCourse(course.getPrice()) + " đ");
				}
				sortCourse.append("</h4></div></div>");
				sortCourse.append("<div class=\"details\">");
				sortCourse.append("<a href=\"course-details?courseId=" + course.getCourseId() + "\"><h4>"
						+ course.getTitle() + "</h4></a>");
				sortCourse.append("<p>" + course.getTeacherName() + "</p></div></div>");
			}
			response.getWriter().write(sortCourse.toString());
		} catch (Exception e) {
			response.sendRedirect(Constant.ERROR_URL);
		}
	}
}
