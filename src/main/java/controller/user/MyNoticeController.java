package controller.user;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import logic.NotificationLogic;
import logic.impl.NotificationLogicImpl;
import modal.Notification;
import util.Constant;
import util.StringUtils;

public class MyNoticeController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2976742272702738118L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		try {
			NotificationLogic notificationLogicImpl = new NotificationLogicImpl();
			HttpSession session = request.getSession();
			String username = (String) session.getAttribute(Constant.USER_LOGIN);
			if (!StringUtils.isNullOrEmpty(username)) {
				if (notificationLogicImpl.updateReadNoticeByUser(username)) {
					List<Notification> listNotifications = notificationLogicImpl.getAllNoticeForUser(username);
					request.setAttribute("listNotifications", listNotifications);
					session.removeAttribute("countNoticeUnread");
					request.getRequestDispatcher(Constant.MY_NOTICE_VIEW).forward(request, response);
				} else {
					response.sendRedirect(Constant.ERROR_URL);
				}
			} else {
				response.sendRedirect(Constant.LOGIN_URL);
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.sendRedirect(Constant.ERROR_URL);
		}
	}
}
