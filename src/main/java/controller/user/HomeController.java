package controller.user;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import logic.CourseLogic;
import logic.NotificationLogic;
import logic.impl.CourseLogicImpl;
import logic.impl.NotificationLogicImpl;
import modal.Course;
import modal.Notification;
import util.Constant;

public class HomeController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8023457440462812041L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		try {
			CourseLogic courseLogicImpl = new CourseLogicImpl();
			NotificationLogic notificationLogicImpl = new NotificationLogicImpl();
			List<Course> listCourses = courseLogicImpl.getAllCourses(Constant.MOST_INTERESTED, 0, 8);
			List<Notification> listNotifications = notificationLogicImpl.getListNotificationsWithLimit(0, 3);
			request.setAttribute("listNotifications", listNotifications);
			request.setAttribute("listCourses", listCourses);
			request.getRequestDispatcher(Constant.HOME_VIEW).forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
			response.sendRedirect(Constant.ERROR_URL);
		}
	}
}
