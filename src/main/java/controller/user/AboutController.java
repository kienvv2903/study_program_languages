package controller.user;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import logic.NotificationLogic;
import logic.impl.NotificationLogicImpl;
import modal.Notification;
import util.Constant;

public class AboutController extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		try {
			NotificationLogic notificationLogicImpl = new NotificationLogicImpl();
			List<Notification> listNotifications = notificationLogicImpl.getListNotificationsWithLimit(0, 3);
			request.setAttribute("listNotifications", listNotifications);
			request.getRequestDispatcher(Constant.ABOUT_VIEW).forward(request, response);
		} catch (Exception e) {
			response.sendRedirect(Constant.ERROR_URL);
		}
	}
}
