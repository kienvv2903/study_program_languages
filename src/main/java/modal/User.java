package modal;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "user")
public class User implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8319580704087131897L;

	@Id
	@Column(name = "username", nullable = false, unique = true, columnDefinition = "VARCHAR(50)")
	private String username;

	@Column(name = "name", length = 30, nullable = false)
	private String name;

	@Column(name = "sex", length = 5, nullable = false)
	private String sex;

	@Column(name = "phone", length = 15, nullable = false)
	private String phone;

	@Temporal(TemporalType.DATE)
	@Column(name = "birthday", nullable = false)
	private Date birthday;

	@Column(name = "email", length = 50, nullable = false)
	private String email;

	@Column(name = "url", length = 255, nullable = false)
	private String urlImg;

	@Column(name = "balance", nullable = false, columnDefinition = "int default '0'")
	private int balance;

	@Column(name = "password", length = 255, nullable = false)
	private String password;

	@Column(name = "salt", length = 255, nullable = false)
	private String salt;

	@Column(name = "rule", length = 11, nullable = false)
	private int rule;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "creat_at")
	private Date creatAt;

	@OneToOne(mappedBy = "user")
	private Course course;

	@OneToOne(mappedBy = "user")
	private Vote vote;

	@OneToOne(mappedBy = "user")
	private CourseDetail courseDetail;

	@OneToOne(mappedBy = "user")
	private CommentCourse commentCourse;
	
	@OneToOne(mappedBy = "user")
	private Notification notification;

	public User() {

	}

	public User(String password, String salt) {
		this.password = password;
		this.salt = salt;
	}
	
	

	/**
	 * @param username
	 * @param name
	 * @param sex
	 * @param phone
	 * @param birthday
	 * @param email
	 * @param urlImg
	 */
	public User(String username, String name, String sex, String phone, Date birthday, String email, String urlImg) {
		super();
		this.username = username;
		this.name = name;
		this.sex = sex;
		this.phone = phone;
		this.birthday = birthday;
		this.email = email;
		this.urlImg = urlImg;
	}

	/**
	 * @param username
	 * @param name
	 * @param sex
	 * @param phone
	 * @param birthday
	 * @param email
	 * @param password
	 * @param salt
	 * @param rule
	 */
	public User(String username, String name, String sex, String phone, Date birthday, String email, String urlImg,
			String password, String salt, int rule) {
		super();
		this.username = username;
		this.name = name;
		this.sex = sex;
		this.phone = phone;
		this.birthday = birthday;
		this.email = email;
		this.urlImg = urlImg;
		this.password = password;
		this.salt = salt;
		this.rule = rule;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username
	 *            the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the sex
	 */
	public String getSex() {
		return sex;
	}

	/**
	 * @param sex
	 *            the sex to set
	 */
	public void setSex(String sex) {
		this.sex = sex;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone
	 *            the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the birthday
	 */
	public Date getBirthday() {
		return birthday;
	}

	/**
	 * @param birthday
	 *            the birthday to set
	 */
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the urlImg
	 */
	public String getUrlImg() {
		return urlImg;
	}

	/**
	 * @param urlImg
	 *            the urlImg to set
	 */
	public void setUrlImg(String urlImg) {
		this.urlImg = urlImg;
	}

	/**
	 * @return the balance
	 */
	public int getBalance() {
		return balance;
	}

	/**
	 * @param balance
	 *            the balance to set
	 */
	public void setBalance(int balance) {
		this.balance = balance;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the salt
	 */
	public String getSalt() {
		return salt;
	}

	/**
	 * @param salt
	 *            the salt to set
	 */
	public void setSalt(String salt) {
		this.salt = salt;
	}

	/**
	 * @return the creatAt
	 */
	public Date getCreatAt() {
		return creatAt;
	}

	/**
	 * @param creatAt
	 *            the creatAt to set
	 */
	public void setCreatAt(Date creatAt) {
		this.creatAt = creatAt;
	}

	/**
	 * @return the rule
	 */
	public int getRule() {
		return rule;
	}

	/**
	 * @param rule
	 *            the rule to set
	 */
	public void setRule(int rule) {
		this.rule = rule;
	}

	/**
	 * @return the course
	 */
	public Course getCourse() {
		return course;
	}

	/**
	 * @param course
	 *            the course to set
	 */
	public void setCourse(Course course) {
		this.course = course;
	}

	/**
	 * @return the vote
	 */
	public Vote getVote() {
		return vote;
	}

	/**
	 * @param vote
	 *            the vote to set
	 */
	public void setVote(Vote vote) {
		this.vote = vote;
	}

	/**
	 * @return the commentCourse
	 */
	public CommentCourse getCommentCourse() {
		return commentCourse;
	}

	/**
	 * @param commentCourse
	 *            the commentCourse to set
	 */
	public void setCommentCourse(CommentCourse commentCourse) {
		this.commentCourse = commentCourse;
	}

	/**
	 * @return the courseDetail
	 */
	public CourseDetail getCourseDetail() {
		return courseDetail;
	}

	/**
	 * @param courseDetail
	 *            the courseDetail to set
	 */
	public void setCourseDetail(CourseDetail courseDetail) {
		this.courseDetail = courseDetail;
	}

	/**
	 * @return the notification
	 */
	public Notification getNotification() {
		return notification;
	}

	/**
	 * @param notification the notification to set
	 */
	public void setNotification(Notification notification) {
		this.notification = notification;
	}

}
