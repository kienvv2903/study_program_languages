package modal;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "vote")
public class Vote {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "vote_id")
	private int voteId;

	@Column(name = "course_id", length = 11, nullable = false)
	private int courseId;

	@Column(name = "username", nullable = false, length = 50)
	private String userName;

	@Column(name = "score", nullable = false, length = 11)
	private int score;

	@Column(name = "feed_back", columnDefinition = "text default ''", nullable = false)
	private String feedBack;

	@Column(name = "time_vote", columnDefinition = "TIMESTAMP default CURRENT_TIMESTAMP", nullable = false)
	private Date timeVote;

	@ManyToOne
	@JoinColumn(name = "course_id", nullable = false, insertable = false, updatable = false)
	private Course course;

	@OneToOne
	@JoinColumn(name = "username", nullable = false, insertable = false, updatable = false)
	private User user;

	@Transient
	private Set<Rate> listRates;

	public Vote() {

	}

	/**
	 * @param courseId
	 * @param userName
	 * @param score
	 * @param feedBack
	 * @param timeVote
	 */
	public Vote(int courseId, String userName, int score, String feedBack, Date timeVote) {
		super();
		this.courseId = courseId;
		this.userName = userName;
		this.score = score;
		this.feedBack = feedBack;
		this.timeVote = timeVote;
	}

	/**
	 * @return the courseId
	 */
	public int getCourseId() {
		return courseId;
	}

	/**
	 * @param courseId
	 *            the courseId to set
	 */
	public void setCourseId(int courseId) {
		this.courseId = courseId;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName
	 *            the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the score
	 */
	public int getScore() {
		return score;
	}

	/**
	 * @param score
	 *            the score to set
	 */
	public void setScore(int score) {
		this.score = score;
	}

	/**
	 * @return the timeVote
	 */
	public Date getTimeVote() {
		return timeVote;
	}

	/**
	 * @param timeVote
	 *            the timeVote to set
	 */
	public void setTimeVote(Date timeVote) {
		this.timeVote = timeVote;
	}

	/**
	 * @return the course
	 */
	public Course getCourse() {
		return course;
	}

	/**
	 * @param course
	 *            the course to set
	 */
	public void setCourse(Course course) {
		this.course = course;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user
	 *            the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * @return the voteId
	 */
	public int getVoteId() {
		return voteId;
	}

	/**
	 * @param voteId
	 *            the voteId to set
	 */
	public void setVoteId(int voteId) {
		this.voteId = voteId;
	}

	/**
	 * @return the feedBack
	 */
	public String getFeedBack() {
		return feedBack;
	}

	/**
	 * @param feedBack
	 *            the feedBack to set
	 */
	public void setFeedBack(String feedBack) {
		this.feedBack = feedBack;
	}

	/**
	 * @return the listRates
	 */
	public Set<Rate> getListRates() {
		return listRates;
	}

	/**
	 * @param listRates
	 *            the listRates to set
	 */
	public void setListRates(Set<Rate> listRates) {
		this.listRates = listRates;
	}
}
