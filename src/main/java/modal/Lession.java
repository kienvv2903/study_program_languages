package modal;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "lession")
public class Lession implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3926980672648635480L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "lession_id", nullable = false)
	private int lessionId;

	@Column(name = "course_id", nullable = false)
	private int courseId;

	@Column(name = "title", length = 255, nullable = false)
	private String title;

	@Column(name = "type", length = 11, nullable = false, columnDefinition = "int default 1")
	private int typeLession;

	@Column(name = "url", columnDefinition = "text", nullable = false)
	private String url;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "creat_at")
	private Date creatAt;

	@ManyToOne
	@JoinColumn(name = "course_id", nullable = false, insertable = false, updatable = false)
	private Course course;

	public Lession() {

	}

	/**
	 * @return the lessionId
	 */
	public int getLessionId() {
		return lessionId;
	}

	/**
	 * @param lessionId
	 *            the lessionId to set
	 */
	public void setLessionId(int lessionId) {
		this.lessionId = lessionId;
	}

	/**
	 * @return the courseId
	 */
	public int getCourseId() {
		return courseId;
	}

	/**
	 * @param courseId
	 *            the courseId to set
	 */
	public void setCourseId(int courseId) {
		this.courseId = courseId;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the typeLession
	 */
	public int getTypeLession() {
		return typeLession;
	}

	/**
	 * @param typeLession
	 *            the typeLession to set
	 */
	public void setTypeLession(int typeLession) {
		this.typeLession = typeLession;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url
	 *            the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the creatAt
	 */
	public Date getCreatAt() {
		return creatAt;
	}

	/**
	 * @param creatAt
	 *            the creatAt to set
	 */
	public void setCreatAt(Date creatAt) {
		this.creatAt = creatAt;
	}

	/**
	 * @return the course
	 */
	public Course getCourse() {
		return course;
	}

	/**
	 * @param course
	 *            the course to set
	 */
	public void setCourse(Course course) {
		this.course = course;
	}

}
