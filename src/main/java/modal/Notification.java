package modal;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.sun.istack.Nullable;

@Entity
@Table(name = "notification")
public class Notification {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "notifi_id")
	private int notificationId;

	@Column(name = "username", length = 50, nullable = false)
	private String username;

	@Nullable
	@Column(name = "course_id", nullable = true)
	private Integer courseId;

	@Column(name = "description", columnDefinition = "text", nullable = false)
	private String description;

	@Column(name = "type", length = 11, nullable = false)
	private int type;

	@Column(name = "status", columnDefinition = "int default 0", length = 11, nullable = false)
	private int status;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "creat_at", nullable = false)
	private Date creatAt;

	@Transient
	private String typeStr;

	@OneToOne
	@JoinColumn(name = "username", nullable = false, insertable = false, updatable = false)
	private User user;

	@OneToOne
	@JoinColumn(name = "course_id", insertable = false, updatable = false, nullable = true)
	private Course course;

	public Notification() {

	}

	/**
	 * @param username
	 * @param courseId
	 * @param description
	 * @param type
	 * @param creatAt
	 */
	public Notification(String username, String description, int type) {
		super();
		this.username = username;
		this.description = description;
		this.type = type;
	}

	/**
	 * @return the notificationId
	 */
	public int getNotificationId() {
		return notificationId;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @return the type
	 */
	public int getType() {
		return type;
	}

	/**
	 * @return the creatAt
	 */
	public Date getCreatAt() {
		return creatAt;
	}

	/**
	 * @param notificationId
	 *            the notificationId to set
	 */
	public void setNotificationId(int notificationId) {
		this.notificationId = notificationId;
	}

	/**
	 * @param username
	 *            the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(int type) {
		this.type = type;
	}

	/**
	 * @param creatAt
	 *            the creatAt to set
	 */
	public void setCreatAt(Date creatAt) {
		this.creatAt = creatAt;
	}

	/**
	 * @return the typeStr
	 */
	public String getTypeStr() {
		return typeStr;
	}

	/**
	 * @param typeStr
	 *            the typeStr to set
	 */
	public void setTypeStr(String typeStr) {
		this.typeStr = typeStr;
	}

	/**
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(int status) {
		this.status = status;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user
	 *            the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * @return the courseId
	 */
	public Integer getCourseId() {
		return courseId;
	}

	/**
	 * @return the course
	 */
	public Course getCourse() {
		return course;
	}

	/**
	 * @param courseId
	 *            the courseId to set
	 */
	public void setCourseId(Integer courseId) {
		this.courseId = courseId;
	}

	/**
	 * @param course
	 *            the course to set
	 */
	public void setCourse(Course course) {
		this.course = course;
	}

}
