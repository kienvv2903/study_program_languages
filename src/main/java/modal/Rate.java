package modal;

import java.util.Comparator;

public class Rate {

	private int numberStar;
	private long countRate;

	/**
	 * @param numberStar
	 * @param countRate
	 */
	public Rate(int numberStar, long countRate) {
		super();
		this.numberStar = numberStar;
		this.countRate = countRate;
	}

	public static Comparator<Rate> rateCompator = new Comparator<Rate>() {

		public int compare(Rate rate1, Rate rate2) {
			int numberStart1 = rate1.getNumberStar();
			int numberStart2 = rate2.getNumberStar();
			return numberStart2 - numberStart1;
		}
	};

	/**
	 * @return the numberStar
	 */
	public int getNumberStar() {
		return numberStar;
	}

	/**
	 * @param numberStar
	 *            the numberStar to set
	 */
	public void setNumberStar(int numberStar) {
		this.numberStar = numberStar;
	}

	/**
	 * @return the countRate
	 */
	public long getCountRate() {
		return countRate;
	}

	/**
	 * @param countRate
	 *            the countRate to set
	 */
	public void setCountRate(long countRate) {
		this.countRate = countRate;
	}
}
