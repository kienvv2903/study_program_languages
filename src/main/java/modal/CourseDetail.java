package modal;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "course_detail")
public class CourseDetail implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1365318579167419465L;

	@Id
	@Column(name = "course_id")
	private int courseId;

	@Id
	@Column(name = "username", length = 50, nullable = false)
	private String userName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "start_study", nullable = false)
	private Date startDate;

	@Temporal(TemporalType.DATE)
	@Column(name = "end_study", nullable = false)
	private Date endDate;

	@Transient
	private String title;

	@ManyToOne
	@JoinColumn(name = "course_id", insertable = false, updatable = false)
	private Course course;

	@OneToOne
	@JoinColumn(name = "username", nullable = false, insertable = false, updatable = false)
	private User user;

	public CourseDetail() {

	}

	/**
	 * @param courseId
	 * @param userName
	 * @param startDate
	 * @param endDate
	 */
	public CourseDetail(int courseId, String userName, Date startDate, Date endDate) {
		super();
		this.courseId = courseId;
		this.userName = userName;
		this.startDate = startDate;
		this.endDate = endDate;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName
	 *            the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate
	 *            the startDate to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the endDate
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate
	 *            the endDate to set
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return the courseId
	 */
	public int getCourseId() {
		return courseId;
	}

	/**
	 * @param courseId
	 *            the courseId to set
	 */
	public void setCourseId(int courseId) {
		this.courseId = courseId;
	}

	/**
	 * @return the course
	 */
	public Course getCourse() {
		return course;
	}

	/**
	 * @param course_cd
	 *            the course_cd to set
	 */
	public void setCourse(Course course) {
		this.course = course;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

}
