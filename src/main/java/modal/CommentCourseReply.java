package modal;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "comment_reply_course")
public class CommentCourseReply implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 959205181922313823L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "comment_reply_id")
	private int commentReplayId;

	@Column(name = "comment_id", length = 11, nullable = false)
	private int commentId;

	@Column(name = "content", columnDefinition = "text", nullable = false)
	private String content;

	@Column(name = "username", length = 50, nullable = false)
	private String username;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "creat_at")
	private Date creatAt;
	
	@ManyToOne
	@JoinColumn(name = "comment_id", nullable = false, insertable = false, updatable = false)
	private CommentCourse commentCourse;
	
	@OneToOne
	@JoinColumn(name = "username", nullable = false, insertable = false, updatable = false)
	private User user;

	public CommentCourseReply() {

	}

	/**
	 * @return the commentReplayId
	 */
	public int getCommentReplayId() {
		return commentReplayId;
	}

	/**
	 * @return the commentId
	 */
	public int getCommentId() {
		return commentId;
	}

	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @return the creatAt
	 */
	public Date getCreatAt() {
		return creatAt;
	}

//	/**
//	 * @return the commentCourse
//	 */
//	public CommentCourse getCommentCourse() {
//		return commentCourse;
//	}

	/**
	 * @param commentReplayId
	 *            the commentReplayId to set
	 */
	public void setCommentReplayId(int commentReplayId) {
		this.commentReplayId = commentReplayId;
	}

	/**
	 * @param commentId
	 *            the commentId to set
	 */
	public void setCommentId(int commentId) {
		this.commentId = commentId;
	}

	/**
	 * @param content
	 *            the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * @param username
	 *            the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @param creatAt
	 *            the creatAt to set
	 */
	public void setCreatAt(Date creatAt) {
		this.creatAt = creatAt;
	}

	/**
	 * @return the commentCourse
	 */
	public CommentCourse getCommentCourse() {
		return commentCourse;
	}

	/**
	 * @param commentCourse the commentCourse to set
	 */
	public void setCommentCourse(CommentCourse commentCourse) {
		this.commentCourse = commentCourse;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

//	/**
//	 * @param commentCourse
//	 *            the commentCourse to set
//	 */
//	public void setCommentCourse(CommentCourse commentCourse) {
//		this.commentCourse = commentCourse;
//	}
}
