package modal;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "course")
public class Course implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4810478031416603461L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "course_id")
	private int courseId;

	@Column(name = "title", length = 255, nullable = false)
	private String title;

	@Column(name = "description", columnDefinition = "text", nullable = false)
	private String description;

	@Column(name = "url_image", columnDefinition = "text", nullable = false)
	private String urlImage;

	@Column(name = "time_study", nullable = false, length = 11)
	private int timeStudy;

	@Column(name = "username", length = 50, nullable = false)
	private String userTeacher;

	@Column(name = "creat_at", nullable = false)
	private Date creatAt;

	@Column(name = "price", nullable = false)
	private int price;

	@Column(name = "sale", columnDefinition = "int default 0")
	private double sale;

	@Column(name = "start_sale", nullable = true)
	private Date startSale;

	@Column(name = "end_sale", nullable = true)
	private Date endSale;

	@OneToOne
	@JoinColumn(name = "username", nullable = false, insertable = false, updatable = false)
	private User user;

	@OneToMany(mappedBy = "course")
	private Set<Vote> votes;

	@OneToMany(mappedBy = "course", fetch = FetchType.EAGER)
	private Set<CourseDetail> courseDetails;

	@OneToMany(mappedBy = "course", fetch = FetchType.EAGER)
	@OrderBy("lessionId DESC, typeLession ASC")
	private Set<Lession> lessions;

	@OneToMany(mappedBy = "courseComment", fetch = FetchType.EAGER)
	private Set<CommentCourse> commentCourses;

	@OneToOne(mappedBy = "course", fetch = FetchType.EAGER)
	private Notification notification;

	@Transient
	private String teacherName;

	@Transient
	private long sumVote;

	@Transient
	private double rate;

	@Transient
	private long countRegiste;

	@Transient
	private long countComment;

	public Course() {

	}

	/**
	 * @param courseId
	 * @param title
	 * @param teacherName
	 * @param price
	 * @param countVote
	 */
	public Course(int courseId, String title, String teacherName, String description, String urlImage, int price,
			long sumVote, double sale, Date startSale, Date endSale, double rate) {
		this.courseId = courseId;
		this.title = title;
		this.teacherName = teacherName;
		this.description = description;
		this.urlImage = urlImage;
		this.price = price;
		this.sumVote = sumVote;
		this.sale = sale;
		this.startSale = startSale;
		this.endSale = endSale;
		this.rate = rate;
	}

	/**
	 * @return the rate
	 */
	public double getRate() {
		return rate;
	}

	/**
	 * @param rate
	 *            the rate to set
	 */
	public void setRate(double rate) {
		this.rate = rate;
	}

	/**
	 * @return the courseId
	 */
	public int getCourseId() {
		return courseId;
	}

	/**
	 * @param courseId
	 *            the courseId to set
	 */
	public void setCourseId(int courseId) {
		this.courseId = courseId;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the urlImage
	 */
	public String getUrlImage() {
		return urlImage;
	}

	/**
	 * @param urlImage
	 *            the urlImage to set
	 */
	public void setUrlImage(String urlImage) {
		this.urlImage = urlImage;
	}

	/**
	 * @return the timeStudy
	 */
	public int getTimeStudy() {
		return timeStudy;
	}

	/**
	 * @param timeStudy
	 *            the timeStudy to set
	 */
	public void setTimeStudy(int timeStudy) {
		this.timeStudy = timeStudy;
	}

	/**
	 * @return the userTeacher
	 */
	public String getUserTeacher() {
		return userTeacher;
	}

	/**
	 * @param userTeacher
	 *            the userTeacher to set
	 */
	public void setUserTeacher(String userTeacher) {
		this.userTeacher = userTeacher;
	}

	/**
	 * @return the creatAt
	 */
	public Date getCreatAt() {
		return creatAt;
	}

	/**
	 * @param creatAt
	 *            the creatAt to set
	 */
	public void setCreatAt(Date creatAt) {
		this.creatAt = creatAt;
	}

	/**
	 * @return the price
	 */
	public int getPrice() {
		return price;
	}

	/**
	 * @param price
	 *            the price to set
	 */
	public void setPrice(int price) {
		this.price = price;
	}

	/**
	 * @return the sale
	 */
	public double getSale() {
		return sale;
	}

	/**
	 * @param sale
	 *            the sale to set
	 */
	public void setSale(double sale) {
		this.sale = sale;
	}

	/**
	 * @return the startSale
	 */
	public Date getStartSale() {
		return startSale;
	}

	/**
	 * @param startSale
	 *            the startSale to set
	 */
	public void setStartSale(Date startSale) {
		this.startSale = startSale;
	}

	/**
	 * @return the endSale
	 */
	public Date getEndSale() {
		return endSale;
	}

	/**
	 * @param endSale
	 *            the endSale to set
	 */
	public void setEndSale(Date endSale) {
		this.endSale = endSale;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user
	 *            the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * @return the votes
	 */
	public Set<Vote> getVotes() {
		return votes;
	}

	/**
	 * @param votes
	 *            the votes to set
	 */
	public void setVotes(Set<Vote> votes) {
		this.votes = votes;
	}

	/**
	 * @return the lessions
	 */
	public Set<Lession> getLessions() {
		return lessions;
	}

	/**
	 * @param lessions
	 *            the lessions to set
	 */
	public void setLessions(Set<Lession> lessions) {
		this.lessions = lessions;
	}

	/**
	 * @return the teacherName
	 */
	public String getTeacherName() {
		return teacherName;
	}

	/**
	 * @param teacherName
	 *            the teacherName to set
	 */
	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}

	/**
	 * @return the sumVote
	 */
	public long getSumVote() {
		return sumVote;
	}

	/**
	 * @param sumVote
	 *            the sumVote to set
	 */
	public void setSumVote(long sumVote) {
		this.sumVote = sumVote;
	}

	// /**
	// * @return the countRegiste
	// */
	// public long getCountRegiste() {
	// return countRegiste;
	// }
	//
	// /**
	// * @param countRegiste
	// * the countRegiste to set
	// */
	// public void setCountRegiste(long countRegiste) {
	// this.countRegiste = countRegiste;
	// }
	//
	// /**
	// * @return the teacherName
	// */
	// public String getTeacherName() {
	// return teacherName;
	// }

	/**
	 * @return the courseDetails
	 */
	public Set<CourseDetail> getCourseDetails() {
		return courseDetails;
	}

	/**
	 * @param courseDetails
	 *            the courseDetails to set
	 */
	public void setCourseDetails(Set<CourseDetail> courseDetails) {
		this.courseDetails = courseDetails;
	}

	/**
	 * @return the commentCourses
	 */
	public Set<CommentCourse> getCommentCourses() {
		return commentCourses;
	}

	/**
	 * @return the notification
	 */
	public Notification getNotification() {
		return notification;
	}

	/**
	 * @return the countRegiste
	 */
	public long getCountRegiste() {
		return countRegiste;
	}

	/**
	 * @return the countComment
	 */
	public long getCountComment() {
		return countComment;
	}

	/**
	 * @param commentCourses
	 *            the commentCourses to set
	 */
	public void setCommentCourses(Set<CommentCourse> commentCourses) {
		this.commentCourses = commentCourses;
	}

	/**
	 * @param notification
	 *            the notification to set
	 */
	public void setNotification(Notification notification) {
		this.notification = notification;
	}

	/**
	 * @param countRegiste
	 *            the countRegiste to set
	 */
	public void setCountRegiste(long countRegiste) {
		this.countRegiste = countRegiste;
	}

	/**
	 * @param countComment
	 *            the countComment to set
	 */
	public void setCountComment(long countComment) {
		this.countComment = countComment;
	}

}
