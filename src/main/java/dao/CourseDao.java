package dao;

import java.util.List;
import java.util.Set;

import modal.Course;
import modal.CourseDetail;
import modal.Lession;

public interface CourseDao extends BaseDao {

	/**
	 * Phương thức lấy ra danh sách khóa học
	 * 
	 * @param type
	 *            kiểu sắp xếp khóa học
	 * @param start
	 *            vị trí bắt đầu lấy bản ghi
	 * @param limit
	 *            số bản ghi tối đa lấy ra
	 * @return List<Course>
	 */
	public List<Course> getAllCourses(String type, int start, int limit);

	/**
	 * Phương thức kiểm tra khóa học có tồn tại không?
	 * 
	 * @param courseId
	 *            mã khóa học
	 * @return boolean true nếu tồn tại và ngược lại
	 */
	public boolean checkExistCourse(int courseId);

	/**
	 * Phương thức lấy ra chi tiết khóa học
	 * 
	 * @param courseId
	 *            mã khóa học
	 * @return Course
	 */
	public Course getDetailCourse(int courseId);

	/**
	 * Phương thức lâý ra danh sách các video khóa học
	 * 
	 * @param courseId
	 *            mã khóa học
	 * @return Set<Lession>
	 */
	public Set<Lession> getLessionByCourseId(int courseId);

	/**
	 * 
	 * @param courseId
	 * @param username
	 * @return
	 */
	public boolean checkRegistedCourse(int courseId, String username);

	/**
	 * 
	 * @param courseId
	 * @return
	 */
	public int getTimeStudyOfCourseByCourseId(int courseId);

	/**
	 * 
	 * @param courseDetail
	 * @return
	 */
	public boolean registeCourse(CourseDetail courseDetail);

	/**
	 * 
	 * @param course
	 * @return
	 */
	public boolean registeCourse1(Course course);

	/**
	 * 
	 * @param courseId
	 * @return
	 */
	public int getPriceCourse(int courseId);

	/**
	 * 
	 * @param course
	 * @return
	 */
	public boolean createCourse(Course course);

	/**
	 * 
	 * @param course
	 * @return
	 */
	public boolean updateCourse(Course course);

	/**
	 * Phương thức lấy ra tất cả khóa học mà người dùng đã đăng ký học
	 * 
	 * @param username
	 *            tên tài khoản
	 * @return List<Course>
	 */
	public List<Course> getAllCourseForUser(String username);

	/**
	 * Phương thức lấy ra danh sách các khóa học liên quan với khóa học này
	 * 
	 * @param couse
	 *            khóa học
	 * @return List<Course>
	 */
	public List<Course> getListCourseConcern(Course course);

	public List<Course> searchCourse(String keySearh, String type, int start, int limit);

	public long getCountCourse(String keySearch);

}
