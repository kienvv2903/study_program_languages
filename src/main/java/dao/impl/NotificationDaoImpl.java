package dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import dao.NotificationDao;
import modal.Notification;
import util.Constant;

public class NotificationDaoImpl extends BaseDaoImpl implements NotificationDao {
	Transaction transaction = null;

	@SuppressWarnings("unchecked")
	@Override
	public List<Notification> getListNotificationsWithLimit(int start, int limit) {
		List<Notification> listNotifications = new ArrayList<>();
		try {
			getInstance().creatSession();
			transaction = session.beginTransaction();
			String sql = "SELECT N FROM Notification N WHERE N.type != :type ORDER BY N.creatAt DESC";
			Query<Notification> query = session.createQuery(sql);
			query.setParameter("type", Constant.REGISTE_SUCCESS);
			query.setFirstResult(start);
			query.setMaxResults(limit);
			listNotifications = query.list();
		} catch (HibernateException e) {
			transaction.rollback();
		} finally {
			closeSession();
		}
		return listNotifications;
	}

	@Override
	public boolean createNotification(Notification notification) {
		try {
			getInstance().creatSession();
			transaction = session.beginTransaction();
			notification.setCreatAt(new Date());
			Integer courseId = null;
			notification.setCourseId(courseId);
			session.save(notification);
			session.getTransaction().commit();
			return true;
		} catch (HibernateException e) {
			transaction.rollback();
		} finally {
			closeSession();
		}
		return false;
	}

	@Override
	public boolean updateNotification(Notification notification) {
		try {
			getInstance().creatSession();
			transaction = session.beginTransaction();
			session.saveOrUpdate(notification);
			session.getTransaction().commit();
			return true;
		} catch (HibernateException e) {
			transaction.rollback();
		} finally {
			closeSession();
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public long getCountNotification() {
		try {
			getInstance().creatSession();
			transaction = session.beginTransaction();
			String sql = "SELECT COUNT(N.notificationId) FROM Notification N WHERE N.type != :type";
			Query<Long> query = session.createQuery(sql);
			query.setParameter("type", Constant.REGISTE_SUCCESS);
			return query.list().get(0);
		} catch (HibernateException e) {
			transaction.rollback();
		} finally {
			closeSession();
		}
		return Constant.ZERO;
	}

	@Override
	public boolean addNoticeRegisteCourseSuccess(Notification notification) {
		try {
			getInstance().creatSession();
			transaction = session.getTransaction();
			session.save(notification);
			transaction.commit();
			return true;
		} catch (HibernateException e) {
			e.printStackTrace();
			transaction.rollback();
		} finally {
			closeSession();
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public long getCountNoticeUnreadForUser(String username) {
		try {
			getInstance().creatSession();
			transaction = session.beginTransaction();
			String sql = "SELECT COUNT(N.notificationId) FROM Notification N WHERE N.type = :type AND N.status = :status AND N.username = :username";
			Query<Long> query = session.createQuery(sql);
			query.setParameter("type", Constant.REGISTE_SUCCESS);
			query.setParameter("status", Constant.UNREAD);
			query.setParameter("username", username);
			return query.list().get(0);
		} catch (HibernateException e) {
			transaction.rollback();
		} finally {
			closeSession();
		}
		return Constant.ZERO;
	}

	public static void main(String[] args) {
		NotificationDaoImpl notificationDaoImpl = new NotificationDaoImpl();
		System.out.println(notificationDaoImpl.getCountNoticeUnreadForUser("hello"));
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Notification> getAllNoticeForUser(String username) {
		List<Notification> listNotifications = new ArrayList<>();
		try {
			getInstance().creatSession();
			transaction = session.beginTransaction();
			String sql = "SELECT N FROM Notification N WHERE N.username = :username ORDER BY N.status ASC, N.creatAt DESC";
			Query<Notification> query = session.createQuery(sql);
			query.setParameter("username", username);
			listNotifications = query.list();
		} catch (HibernateException e) {
			transaction.rollback();
		} finally {
			closeSession();
		}
		return listNotifications;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean updateReadNoticeByUser(String username) {
		try {
			getInstance().creatSession();
			transaction = session.beginTransaction();
			String sql = "UPDATE Notification N SET N.status = :read WHERE N.username = :username AND N.status = :unread";
			Query<Notification> query = session.createQuery(sql);
			query.setParameter("read", Constant.READ);
			query.setParameter("username", username);
			query.setParameter("unread", Constant.UNREAD);
			query.executeUpdate();
			return true;
		} catch (HibernateException e) {
			transaction.rollback();
		} finally {
			closeSession();
		}
		return false;
	}
}
