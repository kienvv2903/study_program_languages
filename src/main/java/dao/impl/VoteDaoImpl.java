package dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import dao.VoteDao;
import modal.Rate;
import modal.Vote;
import util.Constant;

public class VoteDaoImpl extends BaseDaoImpl implements VoteDao {

	Transaction transaction = null;

	@SuppressWarnings("unchecked")
	@Override
	public List<Vote> getListVoteWithLimit(int courseId, int start, int limit) {
		List<Vote> listVotes = new ArrayList<>();
		try {
			getInstance().creatSession();
			transaction = session.beginTransaction();
			String sql = "SELECT V FROM Vote V WHERE V.courseId = :courseId";
			Query<Vote> query = session.createQuery(sql);
			query.setParameter("courseId", courseId);
			query.setFirstResult(start);
			query.setMaxResults(start + limit);
			listVotes = query.list();
		} catch (HibernateException e) {
			transaction.rollback();
		} finally {
			closeSession();
		}
		return listVotes;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Rate> getListRateByCourseId(int courseId) {
		List<Rate> listRates = new ArrayList<Rate>();
		try {
			getInstance().creatSession();
			transaction = session.beginTransaction();
			String sql = "SELECT V.score, COUNT(V.voteId) FROM Vote V WHERE V.courseId = :courseId GROUP BY V.score";
			Query<Object[]> query = session.createQuery(sql);
			query.setParameter("courseId", courseId);
			List<Object[]> result = query.list();
			if (!result.isEmpty()) {
				for (Object[] object : result) {
					Rate rate = new Rate((Integer) object[0], (Long) object[1]);
					listRates.add(rate);
				}
				return listRates;
			}
		} catch (HibernateException e) {
			transaction.rollback();
		} finally {
			closeSession();
		}
		return listRates;
	}

	@SuppressWarnings("unchecked")
	@Override
	public int checkExistVote(int courseId, String username) {
		try {
			getInstance().creatSession();
			transaction = session.beginTransaction();
			String sql = "SELECT V FROM Vote V WHERE V.courseId = :courseId AND V.userName = :username";
			Query<Vote> query = session.createQuery(sql);
			query.setParameter("courseId", courseId);
			query.setParameter("username", username);
			List<Vote> result = query.list();
			if (!result.isEmpty()) {
				Vote vote = result.get(0);
				return vote.getVoteId();
			}
		} catch (HibernateException e) {
			transaction.rollback();
		} finally {
			closeSession();
		}
		return Constant.ZERO;
	}

	@Override
	public boolean saveVote(Vote vote) {
		try {
			getInstance().creatSession();
			transaction = session.beginTransaction();
			session.save(vote);
			transaction.commit();
			return true;
		} catch (HibernateException e) {
			e.printStackTrace();
			transaction.rollback();
		} finally {
			closeSession();
		}
		return false;
	}

	@Override
	public boolean updateVote(Vote vote) {
		try {
			getInstance().creatSession();
			transaction = session.beginTransaction();
			session.saveOrUpdate(vote);
			transaction.commit();
			return true;
		} catch (HibernateException e) {
			e.printStackTrace();
			transaction.rollback();
		} finally {
			closeSession();
		}
		return false;
	}

}
