package dao.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import dao.BaseDao;
import util.HibernateUtil;

public class BaseDaoImpl implements BaseDao {

	private static BaseDao baseDaoImpl;
	protected static SessionFactory sessionFactory = null;
	protected static Session session = null;

	@Override
	public Session creatSession() {
		if (session == null || !session.isConnected()) {
			sessionFactory = HibernateUtil.getSessionFactory();
			session = sessionFactory.getCurrentSession();
		}
		return session;
	}

	@Override
	public Session getSession() {
		return session;
	}

	@Override
	public void setSession(Session session) {
		BaseDaoImpl.session = session;
	}

	@Override
	public void closeSession() {
		if (session != null || session.isConnected()) {
			session.close();
		}
	}

	@Override
	public BaseDao getInstance() {
		if (baseDaoImpl == null) {
			baseDaoImpl = new BaseDaoImpl();
		}
		return baseDaoImpl;
	}

}
