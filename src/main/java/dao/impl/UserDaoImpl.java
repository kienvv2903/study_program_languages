package dao.impl;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import dao.UserDao;
import modal.User;
import util.Common;
import util.Constant;
import util.StringUtils;

public class UserDaoImpl extends BaseDaoImpl implements UserDao {
	Transaction transaction = null;

	@Override
	public boolean insertUser(User user) {
		try {
			getInstance().creatSession();
			transaction = session.beginTransaction();
			session.save(user);
			transaction.commit();
			return true;
		} catch (HibernateException e) {
			transaction.rollback();
		} finally {
			closeSession();
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean updateInforUser(User user) {
		try {
			getInstance().creatSession();
			transaction = session.beginTransaction();
			String sql = "UPDATE User U SET U.name = :name ,U.sex = :sex, U.phone = :phone, U.birthday = :birthday, "
					+ "U.email = :email ";
			StringBuilder queryStatement = new StringBuilder(sql);
			String urlImg = user.getUrlImg();
			if (!StringUtils.isEmpty(urlImg)) {
				queryStatement.append(", U.urlImg = :urlImg ");
			}
			queryStatement.append(" WHERE U.username = :username");
			Query<Object> query = session.createQuery(queryStatement.toString());
			query.setParameter("name", user.getName());
			query.setParameter("sex", user.getSex());
			query.setParameter("phone", user.getPhone());
			query.setParameter("birthday", user.getBirthday());
			query.setParameter("email", user.getEmail());
			if (!StringUtils.isEmpty(urlImg)) {
				query.setParameter("urlImg", urlImg);
			}
			query.setParameter("username", user.getUsername());
			query.executeUpdate();
			transaction.commit();
			return true;
		} catch (HibernateException e) {
			transaction.rollback();
		} finally {
			closeSession();
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public int checkAccount(String username, String password) throws NoSuchAlgorithmException {
		try {
			getInstance().creatSession();
			transaction = session.beginTransaction();
			String sql = "SELECT U.password, U.salt, U.rule FROM " + User.class.getName()
					+ " U WHERE U.username = :username";
			Query<Object[]> query = session.createQuery(sql);
			query.setParameter("username", username);
			List<Object[]> result = query.list();
			if (!result.isEmpty()) {
				Object[] objects = result.get(0);
				String passEncrypt = (String) objects[0];
				String salt = (String) objects[1];
				int rule = (Integer) objects[2];
				if (passEncrypt.equals(Common.encrypt(password, salt))) {
					return rule;
				}
			}
		} catch (HibernateException e) {
			transaction.rollback();
		} finally {
			closeSession();
		}
		return Constant.ERROR;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see dao.UserDao#checkExistUsername(java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean checkExistUsername(String username) {
		try {
			getInstance().creatSession();
			transaction = session.beginTransaction();
			String sql = "FROM " + User.class.getName() + " U WHERE U.username = :username";
			Query<User> query = session.createQuery(sql);
			query.setParameter("username", username);
			List<User> result = query.list();
			if (result.isEmpty()) {
				return false;
			}
		} catch (HibernateException e) {
			transaction.rollback();
		} finally {
			closeSession();
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean checkExistEmail(String email) {
		try {
			getInstance().creatSession();
			transaction = session.beginTransaction();
			String sql = "SELECT U.username FROM " + User.class.getName() + " U WHERE U.email = :email";
			Query<User> query = session.createQuery(sql);
			query.setParameter("email", email);
			List<User> resut = query.list();
			if (resut.isEmpty()) {
				return false;
			}
		} catch (HibernateException e) {
			transaction.rollback();
		} finally {
			closeSession();
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean checkExistEmailNotOfUser(String username, String email) {
		try {
			getInstance().creatSession();
			transaction = session.beginTransaction();
			String sql = "SELECT U.username FROM " + User.class.getName()
					+ " U WHERE U.email = :email AND U.username != :username";
			Query<User> query = session.createQuery(sql);
			query.setParameter("email", email);
			query.setParameter("username", username);
			List<User> resut = query.list();
			if (resut.isEmpty()) {
				return false;
			}
		} catch (HibernateException e) {
			transaction.rollback();
		} finally {
			closeSession();
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public User getInforUserByUserName(String username) {
		User user = new User();
		try {
			getInstance().creatSession();
			transaction = session.beginTransaction();
			String sql = "FROM " + User.class.getName() + " U WHERE U.username = :username";
			Query<User> query = session.createQuery(sql);
			query.setParameter("username", username);
			List<User> resut = query.list();
			if (!resut.isEmpty()) {
				user = resut.get(0);
			}
		} catch (HibernateException e) {
			transaction.rollback();
		} finally {
			closeSession();
		}
		return user;
	}

	@SuppressWarnings("unchecked")
	@Override
	public int getBalanceAccount(String username) {
		try {
			getInstance().creatSession();
			transaction = session.beginTransaction();
			String sql = "SELECT U.balance FROM User U WHERE U.username = :username";
			Query<Object> query = session.createQuery(sql);
			query.setParameter("username", username);
			List<Object> listResult = query.list();
			if (!listResult.isEmpty()) {
				return (int) listResult.get(0);
			}
		} catch (HibernateException e) {
			transaction.rollback();
		} finally {
			closeSession();
		}
		return Constant.ZERO;
	}

	@Override
	public boolean updateBalanceAccount(String username, int balance) {
		try {
			transaction = session.getTransaction();
			String sql = "UPDATE User U SET U.balance = :balance WHERE U.username = :username";
			session.createQuery(sql).setParameter("balance", balance).setParameter("username", username)
					.executeUpdate();
			transaction.commit();
			return true;
		} catch (HibernateException e) {
			e.printStackTrace();
			transaction.rollback();
		} finally {
			closeSession();
		}
		return false;
	}

	@Override
	public boolean updateBalanceAccountForAdmin(String username, int balance) {
		try {
			getInstance().creatSession();
			transaction = session.beginTransaction();
			String sql = "UPDATE User U SET U.balance = :balance WHERE U.username = :username";
			session.createQuery(sql).setParameter("balance", balance).setParameter("username", username)
					.executeUpdate();
			transaction.commit();
			return true;
		} catch (HibernateException e) {
			e.printStackTrace();
			transaction.rollback();
		} finally {
			closeSession();
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> getListUser(String keySearch, int start, int limit) {
		List<User> listUser = new ArrayList<User>();
		try {
			getInstance().creatSession();
			transaction = session.beginTransaction();
			String queryStatement = "SELECT U.username,U.urlImg, U.email, U.balance FROM User U ";
			StringBuilder sql = new StringBuilder(queryStatement);
			if (!StringUtils.isEmpty(keySearch)) {
				sql.append("WHERE U.username LIKE :username OR U.email LIKE :email AND rule != :rule");
			} else {
				sql.append("WHERE U.rule != :rule");
			}
			Query<Object[]> query = session.createQuery(sql.toString());
			if (!StringUtils.isEmpty(keySearch)) {
				query.setParameter("username", "%" + keySearch + "%");
				query.setParameter("email", "%" + keySearch + "%");
			}
			query.setParameter("rule", Constant.RULE_ADMIN);
			query.setFirstResult(start);
			query.setMaxResults(limit);
			List<Object[]> resultList = query.list();
			int index = 0;
			for (Object[] result : resultList) {
				index = 0;
				String username = result[index++].toString();
				String urlImg = result[index++].toString();
				String email = result[index++].toString();
				int balance = (Integer) result[index++];
				User user = new User();
				user.setUsername(username);
				user.setUrlImg(urlImg);
				user.setEmail(email);
				user.setBalance(balance);
				listUser.add(user);
			}
		} catch (HibernateException e) {
			transaction.rollback();
		} finally {
			closeSession();
		}
		return listUser;
	}

	@SuppressWarnings("unchecked")
	@Override
	public long getCountUser(String keySearch) {
		try {
			getInstance().creatSession();
			transaction = session.beginTransaction();
			String queryStatement = "SELECT COUNT(U.username) FROM User U ";
			StringBuilder sql = new StringBuilder(queryStatement);
			if (!StringUtils.isEmpty(keySearch)) {
				sql.append("WHERE U.username LIKE :username OR U.email LIKE :email AND rule != :rule");
			} else {
				sql.append("WHERE U.rule != :rule");
			}
			Query<Object> query = session.createQuery(sql.toString());
			if (!StringUtils.isEmpty(keySearch)) {
				query.setParameter("username", "%" + keySearch + "%");
				query.setParameter("email", "%" + keySearch + "%");
			}
			query.setParameter("rule", Constant.RULE_ADMIN);
			List<Object> result = query.list();
			if (result != null && result.size() > 0) {
				return (Long) result.get(0);
			}
		} catch (HibernateException e) {
			transaction.rollback();
		} finally {
			closeSession();
		}
		return Constant.ZERO;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getListUsernameTeacher() {
		List<String> result = new ArrayList<>();
		try {
			getInstance().creatSession();
			transaction = session.beginTransaction();
			String sql = "SELECT U.username FROM User U WHERE U.rule = :rule";
			Query<String> query = session.createQuery(sql);
			query.setParameter("rule", Constant.RULE_TEACHER);
			result = query.list();
		} catch (HibernateException e) {
			transaction.rollback();
		} finally {
			closeSession();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> searchTeacher(String keySearch) {
		List<User> users = new ArrayList<>();
		try {
			getInstance().creatSession();
			transaction = session.beginTransaction();
			String sql = "SELECT U.username, U.name FROM User U WHERE U.rule = :rule "
					+ "AND (U.username LIKE :username OR U.name LIKE :name)";
			Query<Object[]> query = session.createQuery(sql);
			query.setParameter("rule", Constant.RULE_TEACHER);
			query.setParameter("username", "%" + keySearch + "%");
			query.setParameter("name", "%" + keySearch + "%");
			List<Object[]> result = query.list();
			for (Object[] object : result) {
				String username = (String) object[0];
				String name = (String) object[1];
				User user = new User();
				user.setUsername(username);
				user.setName(name);
				users.add(user);
			}
		} catch (HibernateException e) {
			transaction.rollback();
		} finally {
			closeSession();
		}
		return users;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String getNameTeacherByUsername(String username) {
		String name = "";
		try {
			getInstance().creatSession();
			transaction = session.beginTransaction();
			String queryStatement = "SELECT U.name FROM User U WHERE U.username = :username AND U.rule = :rule";
			StringBuilder sql = new StringBuilder(queryStatement);
			Query<String> query = session.createQuery(sql.toString());
			query.setParameter("username", username);
			query.setParameter("rule", Constant.RULE_TEACHER);
			List<String> result = query.list();
			if (result != null && result.size() > 0) {
				return result.get(0);
			}
		} catch (HibernateException e) {
			transaction.rollback();
		} finally {
			closeSession();
		}
		return name;
	}

	public static void main(String[] args) {
		UserDaoImpl userDaoImpl = new UserDaoImpl();
		userDaoImpl.searchTeacher("1");
	}
}
