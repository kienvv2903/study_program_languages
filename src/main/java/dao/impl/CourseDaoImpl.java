package dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.hibernate.HibernateException;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import dao.CourseDao;
import modal.Course;
import modal.CourseDetail;
import modal.Lession;
import modal.Notification;
import modal.User;
import util.Common;
import util.Constant;

public class CourseDaoImpl extends BaseDaoImpl implements CourseDao {

	Transaction transaction = null;

	@SuppressWarnings("unchecked")
	@Override
	public List<Course> getAllCourses(String type, int start, int limit) {
		List<Course> listCourses = new ArrayList<Course>();
		try {
			getInstance().creatSession();
			transaction = session.beginTransaction();
			String queryStatement = "SELECT c.courseId, c.title, u.name,c.description,c.urlImage, c.price, SUM(COALESCE(v.score, 0)) as cn, "
					+ "c.sale, c.startSale, c.endSale, COUNT(v.voteId) FROM Course c INNER JOIN User u ON c.userTeacher = u.username "
					+ "LEFT JOIN Vote v ON c.courseId = v.courseId GROUP BY c.courseId ";
			StringBuilder sql = new StringBuilder(queryStatement);
			switch (type) {
			case Constant.HIGHT_PRICE:
				sql.append("ORDER BY (c.price - c.price * c.sale / 100) DESC");
				break;
			case Constant.LOW_PRICE:
				sql.append("ORDER BY (c.price - c.price * c.sale / 100) ASC");
				break;
			case Constant.SALE:
				sql.append("ORDER BY c.sale DESC");
				break;
			case Constant.NEW_COURSE:
				sql.append("ORDER BY c.creatAt DESC");
				break;
			default:
				sql.append("ORDER BY cn DESC");
				break;
			}
			Query<Object[]> query = session.createQuery(sql.toString());
			query.setFirstResult(start);
			query.setMaxResults(limit);
			List<Object[]> result = query.list();
			for (Object[] ob : result) {
				int index = 0;
				int courseId = (Integer) ob[index++];
				String title = ob[index++].toString();
				String teacherName = ob[index++].toString();
				String description = ob[index++].toString();
				String urlImage = Common.convertString(ob[index++].toString(), Constant.URL_IMAGE_COURSE_DEFAULT);
				int price = (Integer) ob[index++];
				long sumVote = (Long) ob[index++];
				double sale = (Double) ob[index++];
				Date startSale = (Date) ob[index++];
				Date endSale = (Date) ob[index++];
				long countVote = (Long) ob[index++];
				double rate = countVote == 0 ? 0 : sumVote / countVote;
				Course course = new Course(courseId, title, teacherName, description, urlImage, price, sumVote, sale,
						startSale, endSale, rate);
				listCourses.add(course);
			}
		} catch (HibernateException e) {
			transaction.rollback();
		} finally {
			closeSession();
		}
		return listCourses;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean checkExistCourse(int courseId) {
		try {
			getInstance().creatSession();
			transaction = session.beginTransaction();
			String sql = "SELECT c.courseId FROM Course c WHERE c.courseId = :courseId";
			Query<Object> query = session.createQuery(sql);
			query.setParameter("courseId", courseId);
			List<Object> result = query.list();
			if (!result.isEmpty()) {
				return true;
			}
		} catch (HibernateException e) {
			transaction.rollback();
		} finally {
			closeSession();
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Course getDetailCourse(int courseId) {
		try {
			getInstance().creatSession();
			transaction = session.beginTransaction();
			String sql = "SELECT C FROM Course C WHERE C.courseId = :courseId";
			Query<Course> query = session.createQuery(sql);
			query.setParameter("courseId", courseId);
			List<Course> listCourses = query.getResultList();
			if (!listCourses.isEmpty()) {
				return listCourses.get(0);
			}
		} catch (HibernateException e) {
			transaction.rollback();
		} finally {
			closeSession();
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Set<Lession> getLessionByCourseId(int courseId) {
		try {
			getInstance().creatSession();
			transaction = session.beginTransaction();
			String sql = "SELECT c FROM Course c WHERE c.courseId = :courseId";
			Query<Course> query = session.createQuery(sql);
			query.setParameter("courseId", courseId);
			List<Course> result = query.list();
			if (!result.isEmpty()) {
				Course course = result.get(0);
				Set<Lession> lessions = course.getLessions();
				return lessions;
			}
		} catch (HibernateException e) {
			transaction.rollback();
		} finally {
			closeSession();
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean checkRegistedCourse(int courseId, String username) {
		try {
			getInstance().creatSession();
			transaction = session.beginTransaction();
			String sql = "SELECT Cd FROM CourseDetail Cd WHERE Cd.courseId = :courseId AND Cd.userName = :username";
			Query<Course> query = session.createQuery(sql);
			query.setParameter("courseId", courseId);
			query.setParameter("username", username);
			List<Course> result = query.list();
			if (!result.isEmpty()) {
				return true;
			}
		} catch (HibernateException e) {
			transaction.rollback();
		} finally {
			closeSession();
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public int getTimeStudyOfCourseByCourseId(int courseId) {
		try {
			getInstance().creatSession();
			transaction = session.beginTransaction();
			String sql = "SELECT C.timeStudy FROM Course C WHERE C.courseId = :courseId";
			Query<Object> query = session.createQuery(sql);
			query.setParameter("courseId", courseId);
			List<Object> result = query.list();
			if (!result.isEmpty()) {
				int timeStudy = (int) result.get(0);
				return timeStudy;
			}
		} catch (HibernateException e) {
			transaction.rollback();
		} finally {
			closeSession();
		}
		return Constant.ERROR;
	}

	@Override
	public boolean registeCourse(CourseDetail courseDetail) {
		try {
			transaction = session.beginTransaction();
			session.save(CourseDetail.class.getName(), courseDetail);
			return true;
		} catch (HibernateException e) {
			e.printStackTrace();
			transaction.rollback();
		}
		return false;
	}

	public boolean registeCourse1(Course course) {
		try {
			getInstance().creatSession();
			Set<CourseDetail> courseDetails = course.getCourseDetails();
			transaction = session.beginTransaction();
			for (CourseDetail courseDetail : courseDetails) {
				User user = courseDetail.getUser();
				session.update(user);
				session.save(courseDetail);
			}
			Notification notification = course.getNotification();
			session.save(notification);
			transaction.commit();
			return true;
		} catch (HibernateException e) {
			e.printStackTrace();
			transaction.rollback();
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public int getPriceCourse(int courseId) {
		try {
			getInstance().creatSession();
			transaction = session.beginTransaction();
			String sql = "SELECT C.price FROM Course C WHERE C.courseId = :courseId";
			Query<Object> query = session.createQuery(sql);
			query.setParameter("courseId", courseId);
			List<Object> listResult = query.list();
			if (!listResult.isEmpty()) {
				return (int) listResult.get(0);
			}
		} catch (HibernateException e) {
			transaction.rollback();
		} finally {
			closeSession();
		}
		return Constant.ZERO;
	}

	@Override
	public boolean createCourse(Course course) {
		try {
			getInstance().creatSession();
			transaction = session.beginTransaction();
			// Tạo khóa học
			session.save(course);
			Set<Lession> lessions = course.getLessions();
			int courseId = course.getCourseId();
			Date creatAt = new Date();
			// Tạo danh sách bài học
			for (Lession lession : lessions) {
				lession.setCourseId(courseId);
				lession.setCreatAt(creatAt);
				session.save(lession);
			}
			// Thêm thông báo tạo khóa học mới
			Notification notification = course.getNotification();
			notification.setCourseId(courseId);
			session.save(notification);
			session.getTransaction().commit();
			return true;
		} catch (HibernateException e) {
			e.printStackTrace();
			transaction.rollback();
		} finally {
			closeSession();
		}
		return false;
	}

	@Override
	public boolean updateCourse(Course course) {
		try {
			getInstance().creatSession();
			transaction = session.beginTransaction();
			session.saveOrUpdate(course);
			session.getTransaction().commit();
			return true;
		} catch (HibernateException e) {
			transaction.rollback();
		} finally {
			closeSession();
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Course> getAllCourseForUser(String username) {
		List<Course> listCourses = new ArrayList<Course>();
		try {
			getInstance().creatSession();
			transaction = session.beginTransaction();
			String sql = "SELECT c FROM Course c LEFT JOIN CourseDetail cd "
					+ "ON c.courseId = cd.courseId WHERE cd.userName = :username";
			Query<Course> query = session.createQuery(sql);
			query.setParameter("username", username);
			listCourses = query.list();
		} catch (HibernateException e) {
			transaction.rollback();
		} finally {
			closeSession();
		}
		return listCourses;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Course> getListCourseConcern(Course course) {
		List<Course> listCourses = new ArrayList<Course>();
		try {
			getInstance().creatSession();
			transaction = session.beginTransaction();
			StringBuilder sql = new StringBuilder("SELECT C FROM Course C WHERE C.courseId != :courseId ");
			List<String> programLanguages = Common.getTagProgramLanguages(course.getTitle());
			if (programLanguages.size() > 0) {
				sql.append("AND ( C.userTeacher = :username ");
				for (int i = 0; i < programLanguages.size(); i++) {
					sql.append(" OR C.title LIKE :title" + i + " ");
				}
				sql.append(" ) ");
			} else {
				sql.append(" AND C.userTeacher = :username ");
			}
			Query<Course> query = session.createQuery(sql.toString());
			query.setParameter("courseId", course.getCourseId());
			query.setParameter("username", course.getUserTeacher());
			if (programLanguages.size() > 0) {
				for (int i = 0; i < programLanguages.size(); i++) {
					query.setParameter("title" + i, "%" + programLanguages.get(i) + "%");
				}
			}
			listCourses = query.list();
		} catch (HibernateException e) {
			transaction.rollback();
		} finally {
			closeSession();
		}
		return listCourses;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Course> searchCourse(String keySearh, String type, int start, int limit) {
		List<Course> listCourses = new ArrayList<Course>();
		try {
			getInstance().creatSession();
			transaction = session.beginTransaction();
			String queryStatement = "SELECT c.courseId, c.title, u.name, c.description, c.urlImage, c.price, SUM(COALESCE(v.score, 0)) as cn, "
					+ "c.sale, c.startSale, c.endSale, COUNT(v.voteId) FROM Course c INNER JOIN User u ON c.userTeacher = u.username "
					+ "LEFT JOIN Vote v ON c.courseId = v.courseId WHERE c.title LIKE :title GROUP BY c.courseId ";
			StringBuilder sql = new StringBuilder(queryStatement);
			switch (type) {
			case Constant.HIGHT_PRICE:
				sql.append("ORDER BY (c.price - c.price * c.sale / 100) DESC");
				break;
			case Constant.LOW_PRICE:
				sql.append("ORDER BY (c.price - c.price * c.sale / 100) ASC");
				break;
			case Constant.SALE:
				sql.append("ORDER BY c.sale DESC");
				break;
			case Constant.NEW_COURSE:
				sql.append("ORDER BY c.creatAt DESC");
				break;
			default:
				sql.append("ORDER BY cn DESC");
				break;
			}
			Query<Object[]> query = session.createQuery(sql.toString());
			query.setParameter("title", "%" + keySearh + "%");
			query.setFirstResult(start);
			query.setMaxResults(limit);
			List<Object[]> result = query.list();
			for (Object[] ob : result) {
				int index = 0;
				int courseId = (Integer) ob[index++];
				String title = ob[index++].toString();
				String teacherName = ob[index++].toString();
				String description = ob[index++].toString();
				String urlImage = Common.convertString(ob[index++].toString(), Constant.URL_IMAGE_COURSE_DEFAULT);
				int price = (Integer) ob[index++];
				long sumVote = (Long) ob[index++];
				double sale = (Double) ob[index++];
				Date startSale = (Date) ob[index++];
				Date endSale = (Date) ob[index++];
				long countVote = (Long) ob[index++];
				double rate = countVote == 0 ? 0 : sumVote / countVote;
				Course course = new Course(courseId, title, teacherName, description, urlImage, price, sumVote, sale,
						startSale, endSale, rate);
				listCourses.add(course);
			}
		} catch (HibernateException e) {
			transaction.rollback();
		} finally {
			closeSession();
		}
		return listCourses;
	}

	@SuppressWarnings("unchecked")
	@Override
	public long getCountCourse(String keySearch) {
		try {
			getInstance().creatSession();
			transaction = session.beginTransaction();
			String sql = "SELECT COUNT(C.courseId) FROM Course C WHERE C.title LIKE :title";
			Query<Object> query = session.createQuery(sql);
			query.setParameter("title", "%" + keySearch + "%");
			List<Object> result = query.list();
			if (result != null && result.size() > 0) {
				return (Long) result.get(0);
			}
		} catch (HibernateException e) {
			transaction.rollback();
		} finally {
			closeSession();
		}
		return Constant.ZERO;
	}

}
