package dao.impl;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import dao.LessionDao;
import modal.Lession;

public class LessionDaoImpl extends BaseDaoImpl implements LessionDao {
	Transaction transaction = null;

	@SuppressWarnings("unchecked")
	@Override
	public List<Lession> getAllLessionByCourseId(int courseId) {
		try {
			getInstance().creatSession();
			transaction = session.beginTransaction();
			String sql = "SELECT L FROM Lession L WHERE L.courseId = :courseId";
			Query<Lession> query = session.createQuery(sql);
			query.setParameter("courseId", courseId);
			List<Lession> listLessions = query.list();
			return listLessions;
		} catch (HibernateException e) {
			transaction.rollback();
		} finally {
			closeSession();
		}
		return null;
	}

}
