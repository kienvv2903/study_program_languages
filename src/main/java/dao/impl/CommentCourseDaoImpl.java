package dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import dao.CommentCourseDao;
import modal.CommentCourse;

public class CommentCourseDaoImpl extends BaseDaoImpl implements CommentCourseDao {

	Transaction transaction = null;

	@SuppressWarnings("unchecked")
	@Override
	public List<CommentCourse> getListCommentWithLimitByCourseId(int courseId) {
		List<CommentCourse> listCommentCourse = new ArrayList<>();
		try {
			getInstance().creatSession();
			transaction = session.beginTransaction();
			String sql = "SELECT cc FROM CommentCourse cc WHERE cc.courseId = :courseId ORDER BY cc.creatAt DESC";
			Query<CommentCourse> query = session.createQuery(sql);
			query.setParameter("courseId", courseId);
			query.setFirstResult(0);
			query.setMaxResults(4);
			listCommentCourse = query.list();
		} catch (HibernateException e) {
			transaction.rollback();
		} finally {
			closeSession();
		}
		return listCommentCourse;
	}

	@Override
	public boolean createCommentCourse(CommentCourse commentCourse) {
		try {
			getInstance().creatSession();
			transaction = session.beginTransaction();
			session.save(commentCourse);
			session.getTransaction().commit();
			return true;
		} catch (HibernateException e) {
			e.printStackTrace();
			transaction.rollback();
		} finally {
			closeSession();
		}
		return false;
	}

	@Override
	public boolean updateCommentCourse(CommentCourse commentCourse) {
		try {
			getInstance().creatSession();
			transaction = session.beginTransaction();
			session.saveOrUpdate(commentCourse);
			session.getTransaction().commit();
			return true;
		} catch (HibernateException e) {
			transaction.rollback();
		} finally {
			closeSession();
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public CommentCourse getCommentNewest(int courseId, String username) {
		List<CommentCourse> listCommentCourse = new ArrayList<>();
		try {
			getInstance().creatSession();
			transaction = session.beginTransaction();
			String sql = "SELECT cc FROM CommentCourse cc WHERE cc.courseId = :courseId AND cc.username = :username ORDER BY cc.creatAt DESC";
			Query<CommentCourse> query = session.createQuery(sql);
			query.setParameter("courseId", courseId);
			query.setParameter("username", username);
			listCommentCourse = query.getResultList();
		} catch (HibernateException e) {
			transaction.rollback();
		} finally {
			closeSession();
		}
		return listCommentCourse.get(0);
	}

}
