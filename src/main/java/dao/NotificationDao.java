package dao;

import java.util.List;

import modal.Notification;

public interface NotificationDao extends BaseDao {
	public List<Notification> getListNotificationsWithLimit(int start, int limit);

	public boolean createNotification(Notification notification);

	public boolean updateNotification(Notification notification);

	public long getCountNotification();

	public boolean addNoticeRegisteCourseSuccess(Notification notification);

	public long getCountNoticeUnreadForUser(String username);

	public List<Notification> getAllNoticeForUser(String username);

	public boolean updateReadNoticeByUser(String username);
}
