package dao;

import java.util.List;

import modal.CommentCourse;

public interface CommentCourseDao extends BaseDao {
	/**
	 * Phương thức lấy ra danh sách các bình luận về khóa học
	 * 
	 * @param courseId
	 *            mã khóa học
	 * @return List<CommentCourse>
	 */
	public List<CommentCourse> getListCommentWithLimitByCourseId(int courseId);

	/**
	 * Phương thức thêm bình luận khóa học
	 * 
	 * @param commentCourse
	 *            đối tượng CommentCourse
	 * @return boolean true nếu thêm thành công và ngược lại
	 */
	public boolean createCommentCourse(CommentCourse commentCourse);

	/**
	 * Phương thức chỉnh sửa bình luận khóa học
	 * 
	 * @param commentCourse
	 *            đối tượng CommentCourse
	 * @return boolean true nếu sửa thành công và ngược lại
	 */
	public boolean updateCommentCourse(CommentCourse commentCourse);

	/**
	 * Phương thức lấy ra CommentCourse của người dùng vừa bình luận ở khóa học
	 * 
	 * @param courseId
	 *            mã khóa học
	 * @param username
	 *            tên người dùng
	 * @return CommentCourse
	 */
	public CommentCourse getCommentNewest(int courseId, String username);
}
