package dao;

import java.security.NoSuchAlgorithmException;
import java.util.List;

import modal.User;

public interface UserDao extends BaseDao {
	/**
	 * Phương thức thêm 1 đối tượng User vào CSDL (Đăng ký tài khoản)
	 * 
	 * @return boolean
	 */
	public boolean insertUser(User user);

	/**
	 * Phương thức cập nhật thông tin người dùng
	 * 
	 * @param user
	 * @return
	 */
	public boolean updateInforUser(User user);

	/**
	 * Phương thức lấy thông tin người dùng bởi tên người dùng
	 * 
	 * @param username
	 *            tên tài khoản của người dùng cần lấy thông tin
	 * @return User
	 */
	public User getInforUserByUserName(String username);

	/**
	 * Phương thức kiểm tra tài khoản mật khẩu có hợp lệ không?
	 * 
	 * @param username
	 *            tên tài khoản
	 * @param password
	 *            mật khẩu
	 * @return int giá trị kiểu người dùng
	 * @throws NoSuchAlgorithmException
	 */
	public int checkAccount(String username, String password) throws NoSuchAlgorithmException;

	/**
	 * Phương thức kiểm tra tài khoản đã tồn tại trong CSDL hay chưa
	 * 
	 * @param username
	 *            tên tài khoản kiểm tra
	 * @return true nếu đã tồn tại và ngược lại
	 */
	public boolean checkExistUsername(String username);

	/**
	 * Phương thức kiểm tra email đã tồn tại trong CSDL hay chưa
	 * 
	 * @param email
	 *            email cần kiểm tra
	 * @return true nếu đã tồn tại và ngược lại
	 */
	public boolean checkExistEmail(String email);

	/**
	 * Phương thức kiểm tra email đã tồn tại trong CSDL hay chưa (không tính
	 * email của người dùng)
	 * 
	 * @param username
	 *            tên tài khoản
	 * @param email
	 *            email cần kiểm tra
	 * @return true nếu đã tồn tại và ngược lại
	 */
	public boolean checkExistEmailNotOfUser(String username, String email);

	/**
	 * Phương thức lấy số dư trong tài khoản
	 * 
	 * @param username
	 *            tên tài khoản người dùng
	 * @return int số dư trong tài khoản
	 */
	public int getBalanceAccount(String username);

	/**
	 * Phương thức cập nhật số dư tài khoản của người dùng
	 * 
	 * @param username
	 *            tên tài khoản
	 * @param balance
	 *            số dư tài khoản cần cập nhật
	 * @return boolean true nếu update thành công và ngược lại
	 */
	public boolean updateBalanceAccount(String username, int balance);

	/**
	 * Phương thức lấy ra danh sách người dùng
	 * 
	 * @param keySearch
	 *            chuỗi tìm kiếm người dùng
	 * @param start
	 *            vị trí bản ghi bắt đầu tìm kiếm
	 * @param limit
	 *            số bản ghi tối đa lấy ra
	 * @return List<User>
	 */
	public List<User> getListUser(String keySearch, int start, int limit);

	/**
	 * Phương thức lấy ra số bản ghi tìm kiếm được
	 * 
	 * @param keySearch
	 *            chuỗi tìm kiếm người dùng
	 * @return long
	 */
	public long getCountUser(String keySearch);

	/**
	 * Phương thức cập nhật tài khoản người dùng (chức năng của admin)
	 * 
	 * @param username
	 *            tài khoản người dùng
	 * @param balance
	 *            số tiền cần nạp thêm
	 * @return boolean true nếu cập nhật thành công và ngược lại
	 */
	public boolean updateBalanceAccountForAdmin(String username, int balance);

	public List<String> getListUsernameTeacher();

	public List<User> searchTeacher(String keySearch);

	public String getNameTeacherByUsername(String username);
}
