package dao;

import java.util.List;

import modal.Lession;

public interface LessionDao extends BaseDao {
	public List<Lession> getAllLessionByCourseId(int courseId);
}
