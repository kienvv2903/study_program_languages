package dao;

import org.hibernate.Session;

public interface BaseDao {

	/**
	 * Phương thức khởi tạo đối tượng BaseDao duy nhất
	 * 
	 * @return BaseDao
	 */
	public BaseDao getInstance();

	/**
	 * Phương thức lấy ra đối tượng Session
	 * 
	 * @return Session
	 */
	public Session getSession();

	/**
	 * Phương thức setSession
	 * 
	 * @param session
	 */
	public void setSession(Session session);

	/**
	 * Phương thức khởi tạo Session
	 * 
	 * @return Session
	 */
	public Session creatSession();

	/**
	 * Phương thức đóng Session
	 */
	public void closeSession();
}
