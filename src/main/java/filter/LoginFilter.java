package filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import util.Constant;

public class LoginFilter implements Filter {

	public void destroy() {
		// TODO Auto-generated method stub

	}

	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
			throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) servletRequest;
		HttpServletResponse response = (HttpServletResponse) servletResponse;
		HttpSession session = request.getSession();
		String servletPath = request.getServletPath().substring(1);
		String userLogin = (String) session.getAttribute("userLogin");
		if (userLogin == null) {
			switch (servletPath) {
			case Constant.RATING_URL:
				session.setAttribute(Constant.FROM, Constant.RATING_FROM);
				session.setAttribute(Constant.COURSE_ID_RATING, request.getParameter(Constant.COURSE_ID));
				break;
			case Constant.REGISTE_COURSE_URL:
				session.setAttribute(Constant.FROM, Constant.REGISTE_COURSE_FROM);
				session.setAttribute(Constant.COURSE_ID_REGISTE, request.getParameter(Constant.COURSE_ID));
			default:
				break;
			}
			response.sendRedirect(Constant.LOGIN_URL);
		} else {
			filterChain.doFilter(servletRequest, servletResponse);
		}
	}

	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub

	}

}
