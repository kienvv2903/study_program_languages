package filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import logic.NotificationLogic;
import logic.impl.NotificationLogicImpl;
import util.Constant;
import util.StringUtils;

public class CheckNoticeFilter implements Filter {

	public void destroy() {
		// TODO Auto-generated method stub

	}

	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub

	}

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
			throws IOException, ServletException {
		try {
			HttpServletRequest request = (HttpServletRequest) servletRequest;
			HttpSession session = request.getSession();
			String userLogin = (String) session.getAttribute("userLogin");
			if (!StringUtils.isNullOrEmpty(userLogin)) {
				NotificationLogic notificationLogicImpl = new NotificationLogicImpl();
				long countNoticeUnread = notificationLogicImpl.getCountNoticeUnreadForUser(userLogin);
				if (countNoticeUnread > Constant.ZERO) {
					session.setAttribute("countNoticeUnread", countNoticeUnread);
				} else {
					session.removeAttribute("countNoticeUnread");
				}
			} else {
				session.removeAttribute("countNoticeUnread");
			}
			filterChain.doFilter(servletRequest, servletResponse);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
