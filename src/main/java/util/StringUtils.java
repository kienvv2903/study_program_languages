package util;

public class StringUtils {
	/**
	 * Kiểm tra chuỗi rỗng không?
	 * 
	 * @param input
	 * @return
	 */
	public static boolean isEmpty(String input) {
		if (!isNull(input) && input.length() == 0) {
			return true;
		}
		return false;
	}

	/**
	 * Kiểm tra chuỗi có phải null không?
	 * 
	 * @param input
	 * @return
	 */
	public static boolean isNull(String input) {
		if (input == null) {
			return true;
		}
		return false;
	}

	public static boolean isNullOrEmpty(String input) {
		if (isNull(input) || isEmpty(input)) {
			return true;
		}
		return false;
	}
}
