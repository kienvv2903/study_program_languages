package util;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Properties;

/**
 * @author kien vu
 *
 */
@SuppressWarnings("unchecked")
public class MessageProperties {
	public static final String LOGIN_FOR_RATING = "LOGIN_FOR_RATING";
	public static final String LOGIN_FOR_REGISTE_COURSE = "LOGIN_FOR_REGISTE_COURSE";
	public static final String REGISTER_SUCCESS = "REGISTER_SUCCESS";
	public static final String CREAT_COURSE_SUCCESS = "CREAT_COURSE_SUCCESS";
	public static final String POST_NOTICE_SUCCESS = "POST_NOTICE_SUCCESS";
	private static HashMap<String, String> hashMapMessageError = new HashMap<String, String>();

	/**
	 * Đọc file properties và bỏ vào trong map
	 */
	static {
		Properties properties = new Properties();
		try {
			properties.load(new InputStreamReader(
					Thread.currentThread().getContextClassLoader().getResourceAsStream("message.properties"), "UTF-8"));
		} catch (IOException e) {
			e.getMessage();
		}
		Enumeration<String> enumeration = (Enumeration<String>) properties.propertyNames();
		while (enumeration.hasMoreElements()) {
			String key = (String) enumeration.nextElement();
			hashMapMessageError.put(key, properties.getProperty(key));
		}
	}

	/**
	 * Hàm đọc từ trong map
	 * 
	 * @param key:
	 *            key cần đọc
	 * 
	 * @return giá trị của key
	 */
	public static String getData(String key) {
		String value = "";
		if (hashMapMessageError.containsKey(key)) {
			value = hashMapMessageError.get(key);
		}
		return value;
	}
}
