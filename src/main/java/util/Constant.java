package util;

public class Constant {

	// Các servletPath của User
	public static final String HOME_URL = "home";
	public static final String ABOUT_URL = "about";
	public static final String COURSE_URL = "course";
	public static final String NOTICE_URL = "notice";
	public static final String MY_NOTICE_URL = "myNotice.us";
	public static final String MY_COURSE_URL = "myCourse.us";
	public static final String COURSE_DETAIL_URL = "course-details";
	public static final String CONTACT_URL = "contact";
	public static final String INFOR_USER_URL = "infor-user.us";
	public static final String RATING_URL = "rating.us";
	public static final String REGISTE_COURSE_URL = "registeCourse.us";
	public static final String LOGIN_URL = "login";

	public static final String ERROR_URL = "errorPage";

	public static final String HOME_VIEW = "/WEB-INF/view/user/home.jsp";
	public static final String ABOUT_VIEW = "/WEB-INF/view/user/about.jsp";
	public static final String COURSE_VIEW = "/WEB-INF/view/user/course.jsp";
	public static final String COURSE_DETAIL_VIEW = "/WEB-INF/view/user/course-details.jsp";
	public static final String CONTACT_VIEW = "/WEB-INF/view/user/contact.jsp";
	public static final String INFOR_USER_VIEW = "/WEB-INF/view/user/infor-user.jsp";
	public static final String LOGIN_USER_VIEW = "/WEB-INF/view/user/login.jsp";
	public static final String REGISTER_VIEW = "/WEB-INF/view/user/register.jsp";
	public static final String QUESTIONS_VIEW = "/WEB-INF/view/user/questions.jsp";
	public static final String COMMENT_VIEW = "/WEB-INF/view/user/comment.jsp";
	public static final String SORT_COURSE_VIEW = "/WEB-INF/view/user/sort-course.jsp";
	public static final String MY_COURSE_VIEW = "/WEB-INF/view/user/mycourse.jsp";
	public static final String NOTICE_VIEW = "/WEB-INF/view/user/notice.jsp";
	public static final String MY_NOTICE_VIEW = "/WEB-INF/view/user/my-notice.jsp";

	public static final String ERROR_VIEW = "/WEB-INF/view/error.jsp";

	// Các servletPath của Admin
	public static final String LOGIN_ADMIN_URL = "adminLogin";
	public static final String HOME_ADMIN_URL = "home.ad";
	public static final String MANAGER_USER_URL = "managerUser.ad";
	public static final String CREAT_COURSE_URL = "creatCourse.ad";
	public static final String UPDATE_COURSE_URL = "updateCourse.ad";
	public static final String POST_NOTICE_URL = "postNotice.ad";

	// Đường dẫn view admin
	public static final String LOGIN_ADMIN_VIEW = "/WEB-INF/view/admin/login.jsp";
	public static final String HOME_ADMIN_VIEW = "/WEB-INF/view/admin/home.jsp";
	public static final String MANAGER_USERS_VIEW = "/WEB-INF/view/admin/manager-user.jsp";
	public static final String MANAGER_COURSE_VIEW = "/WEB-INF/view/admin/manager-course.jsp";
	public static final String CREAT_COURSE_VIEW = "/WEB-INF/view/admin/creat-course.jsp";
	public static final String UPDATE_COURSE_VIEW = "/WEB-INF/view/admin/update-course.jsp";
	public static final String POST_NOTICE_VIEW = "/WEB-INF/view/admin/post-notice.jsp";

	public static final String ERROR_USERNAME = "ERR_USERNAME";
	public static final String ERROR_EMAIL = "ERR_EMAIL";
	public static final String ERROR_ACCOUNT = "ERR_ACCOUNT";

	public static final String REGISTER_SUCCESS = "REGISTER_SUCCESS";

	public static final String STRING_RANDOM = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

	public static final int SALT_LENGTH = 30;

	// Quyền admin
	public static final int RULE_ADMIN = 0;
	// Quyền người dùng thường
	public static final int RULE_USER = 1;
	// Quyền giáo viên
	public static final int RULE_TEACHER = 2;
	public static final int ERROR = -1;

	public static final String SEX_GIRL = "Nữ";
	public static final String SEX_BOY = "Nam";

	public static final String URL_DEFAULT_AVATAR_BOY = "https://firebasestorage.googleapis.com/v0/b/webtt-a8565.appspot.com/o/avatar%2Fboy.png?alt=media&token=2f8ee7c5-b319-40d2-9b5b-70b2c96fb352";
	public static final String URL_DEFAULT_AVATAR_GIRL = "https://firebasestorage.googleapis.com/v0/b/webtt-a8565.appspot.com/o/avatar%2Fgirl.png?alt=media&token=62f40dc2-0790-4a3d-8600-938ce397d494";

	// Các loại lỗi
	public static final String ERR_USERNAME = "ERR_USERNAME";
	public static final String ERR_EMAIL = "ERR_EMAIL";
	public static final String ERR_ACCOUNT = "ERR_ACCOUNT";
	public static final String ERR_NOT_EXIST_COURSE = "ERR_NOT_EXIST_COURSE";
	public static final String ERR_SYSTEM = "ERR_SYSTEM";
	public static final String ERR_REGISTED_COURSE = "ERR_REGISTED_COURSE";

	public static final String USER_LOGIN = "userLogin";

	// Giá trị sao rating
	public static final int ONE_STAR = 1;
	public static final int FIVE_STAR = 5;

	public static final int ZERO = 0;
	public static final int ONE = 1;

	// Các biến điều kiện
	public static final String LOGIN_FOR_RATING = "loginForRating";

	// Các biến xác định màn hình chuyển tiếp đến
	public static final String REGISTER_FROM = "REGISTER_FROM";
	public static final String RATING_FROM = "RATING_FROM";
	public static final String REGISTE_COURSE_FROM = "REGISTE_COURSE_FROM";

	// Các biến set lên request
	public static final String USERNAME = "username";
	public static final String MESSAGE = "message";
	public static final String ERROR_NOTIFI = "error";
	public static final String COURSE_ID = "courseId";
	public static final String CONTENT_COMMENT = "content";

	// Các biến set lên session
	public static final String ERR = "error";
	public static final String RULE = "rule";
	public static final String FROM = "from";
	public static final String COURSE_ID_RATING = "courseIdRating";
	public static final String COURSE_ID_REGISTE = "courseIdRegiste";
	public static final String RATING_SHOW = "ratingShow";
	public static final String REGISTE_COURSE_SUCCESS = "registeCourseSuccess";
	public static final String NOT_MONEY_PAY_COURSE = "notMoneyPayCourse";
	public static final String UPDATE_BALANCE_SUCCESS = "updateBalanceSuccess";

	// Type thông báo
	public static final int REGISTE_SUCCESS = 0;
	public static final int COURSE_SALE = 1;
	public static final int COURSE_NEW = 2;
	public static final int NORMAL = 3;

	// Tình trạng thông báo
	public static final int UNREAD = 0;
	public static final int READ = 1;

	// Kiểu sắp xếp khóa học
	public static final String MOST_INTERESTED = "most_interested";
	public static final String HIGHT_PRICE = "hight_price";
	public static final String LOW_PRICE = "low_price";
	public static final String SALE = "sale";
	public static final String NEW_COURSE = "new_course";

	// Danh sách các ngôn ngữ lập trình
	public static final String[] PROGRAM_LANGUAGES = { "C", "C++", "C/C++", "MySQL", "Javascript", "Java", "Python",
			"C#", "JSON", "AJAX", "jQuery", "ASP", "Node.js", "Ruby", "Kotlin", "HTML", "CSS", "Bootstrap", "PHP",
			"Swift", "Assembly" };

	public static final String UPLOAD_DIRECTORY = "upload";
	public static final String UPLOAD_VIDEO_DIRECTORY = "videos";
	public static final String UPLOAD_IMAGE_DIRECTORY = "images";

	// Kiểu video (Trả phí/Miễn phí)
	public static final int FREE_LESSION = 0;
	public static final int PAID_LESSION = 1;

	public static final String URL_IMAGE_COURSE_DEFAULT = "user/img/p1.jpg";

}
