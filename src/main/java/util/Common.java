package util;

import java.io.File;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import modal.Rate;

public class Common {

	/**
	 * Phương thức tạo thêm 1 chuỗi ngẫu nhiên để mã hóa kèm thêm với chuỗi cần
	 * mã hóa
	 * 
	 * @param length
	 *            Độ dài chuỗi sinh ngẫu nhiên ra
	 * @return Chuỗi sinh ngẫu nhiên
	 */
	public static String creatSalt(int length) {
		SecureRandom secureRandom = new SecureRandom();
		StringBuilder stringBuilder = new StringBuilder(length);
		String stringRandom = Constant.STRING_RANDOM;
		for (int i = 0; i < length; i++) {
			stringBuilder.append(stringRandom.charAt(secureRandom.nextInt(stringRandom.length())));
		}
		return stringBuilder.toString();
	}

	/**
	 * Phương thức mã hóa chuỗi đầu vào theo chuẩn SHA-1
	 * 
	 * @param input
	 *            Chuỗi cần mã hóa
	 * @param salt
	 *            Chuỗi thêm vào để mã hóa
	 * @return Chuỗi sau khi mã hóa
	 * @throws NoSuchAlgorithmException
	 *             Lỗi khi không thực hiện mã hóa theo chuẩn SHA-1
	 */
	public static String encrypt(String input, String salt) throws NoSuchAlgorithmException {
		MessageDigest messageDigest = null;
		try {
			messageDigest = MessageDigest.getInstance("SHA-1");
		} catch (NoSuchAlgorithmException e) {
			e.getMessage();
			throw e;
		}
		return Base64.getEncoder().encodeToString(messageDigest.digest((salt + input).getBytes()));
	}

	public static boolean checkRuleUser(int rule) {
		if (Constant.RULE_USER == rule || Constant.RULE_TEACHER == rule) {
			return true;
		} else {
			return false;
		}
	}

	public static int isInteger(String input) {
		if (input != null && input.matches("\\d+")) {
			return Integer.valueOf(input);
		}
		return Constant.ERROR;
	}

	public static double isDouble(String input) {
		if (input != null && input.matches("\\d+")) {
			return Double.valueOf(input);
		}
		return Constant.ERROR;
	}

	public static int convertStringToInteger(String input, int defaultValue) {
		int result = isInteger(input);
		if (result == Constant.ERROR) {
			return defaultValue;
		}
		return result;
	}

	public static double convertStringToDouble(String input, double defaultValue) {
		double result = isDouble(input);
		if (result == Constant.ERROR) {
			return defaultValue;
		}
		return result;
	}

	public static String convertString(String input, String defaultValue) {
		if (StringUtils.isNullOrEmpty(input)) {
			return defaultValue.trim();
		} else {
			return input.trim();
		}
	}

	public static int formatLimitRecord(int input, int defaultValue) {
		int result;
		if (input < 1) {
			result = defaultValue;
		}
		result = input;
		return result;
	}

	public static boolean isNull(String input) {
		return input == null ? true : false;
	}

	/**
	 * Phương thức kiểm tra chuỗi đầu vào có phải là số sao rating không?
	 * 
	 * @param input
	 * @return int
	 */
	public static int isScoreRate(String input) {
		if (isInteger(input) > Constant.ERROR) {
			int score = Integer.valueOf(input);
			if (score >= Constant.ONE_STAR && Constant.FIVE_STAR >= score) {
				return score;
			}
		}
		return Constant.ERROR;
	}

	/**
	 * Phương thức lấy ra danh sách các giá trị được vote (1-5)
	 * 
	 * @param listRates
	 * @return List<Integer>
	 */
	public static List<Integer> getListStars(List<Rate> listRates) {
		List<Integer> listStars = new ArrayList<Integer>();
		for (int i = 0; i < listRates.size(); i++) {
			listStars.add(listRates.get(i).getNumberStar());
		}
		return listStars;
	}

	/**
	 * Phương thức tính tổng số lượt vote
	 * 
	 * @param listRates
	 * @return long
	 */
	public static long getRatings(List<Rate> listRates) {
		long result = 0;
		for (Rate rate : listRates) {
			result += rate.getCountRate();
		}
		return result;
	}

	public static String formatObjectToString(Object object) {
		String value = "";
		if (object != null) {
			value = object.toString();
		}
		return value;
	}

	/**
	 * Phương thức tính tổng điểm vote
	 * 
	 * @param listRates
	 * @return long
	 */
	public static long calSumRatings(List<Rate> listRates) {
		long result = 0;
		for (Rate rate : listRates) {
			result += rate.getCountRate() * rate.getNumberStar();
		}
		return result;
	}

	public static String fomatPriceCourse(double price) {
		String pattern = "###,###";
		DecimalFormat myFormatter = new DecimalFormat(pattern);
		return myFormatter.format(price);
	}

	/**
	 * Phương thức tính toán ra danh sách paging
	 * 
	 * @param totalRecords
	 *            tổng số bản ghi
	 * @param currentPage
	 *            trang hiện tại
	 * @param limit
	 *            số lượng bản ghi trên 1 trang
	 * @return List<Integer>
	 */
	public static List<Integer> getListPaging(int totalRecords, int currentPage, int limit) {
		int sizePaging = convertStringToInteger(ConfigProperties.getData(ConfigProperties.SIZE_PAGING), 1);
		int totalPaning = (totalRecords % limit == 0) ? totalRecords / limit : totalRecords / limit + 1;
		int numberPaningStart = currentPage % sizePaging == 0 ? currentPage - sizePaging + 1
				: currentPage - currentPage % sizePaging + 1;
		int numberPaningEnd = (numberPaningStart + 2) * limit > totalRecords ? totalPaning : numberPaningStart + 2;
		List<Integer> result = new ArrayList<>();
		for (int i = numberPaningStart; i <= numberPaningEnd; i++) {
			result.add(i);
		}
		return result;
	}

	public static List<String> getTagProgramLanguages(String titleCourse) {
		List<String> result = new ArrayList<>();
		for (String programLanguage : Constant.PROGRAM_LANGUAGES) {
			if (titleCourse.indexOf(programLanguage) > Constant.ERROR) {
				result.add(programLanguage);
			}
		}
		return result;
	}

	public static boolean uploadVideo(HttpServletRequest request) {
		if (ServletFileUpload.isMultipartContent(request)) {
			String uploadPath = request.getServletContext().getRealPath("") + File.separator
					+ Constant.UPLOAD_DIRECTORY;

			// Tạo thư mục lưu file nếu chưa tồn tại
			File uploadDir = new File(uploadPath);
			if (!uploadDir.exists()) {
				uploadDir.mkdir();
			}

			try {
				DiskFileItemFactory factory = new DiskFileItemFactory();
				ServletFileUpload upload = new ServletFileUpload(factory);
				List<FileItem> formItems = upload.parseRequest(request);

				if (formItems != null && formItems.size() > 0) {
					for (FileItem item : formItems) {
						if (!item.isFormField()) {
							String fileName = new File(item.getName()).getName();
							String filePath = uploadPath + File.separator + fileName;
							File storeFile = new File(filePath);
							item.write(storeFile);
						}
					}
					return true;
				}
			} catch (Exception ex) {
				return false;
			}

		}
		return false;
	}

	public static void main(String[] args) {
		getTagProgramLanguages("Khóa học lập trình C cơ bản");
	}
}
