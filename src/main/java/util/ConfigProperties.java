package util;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Properties;

/**
 * @author kien vu
 *
 */
@SuppressWarnings("unchecked")
public class ConfigProperties {

	public static final String LIMIT_VOTE = "LIMIT_VOTE";
	public static final String SIZE_PAGING = "SIZE_PAGING";
	public static final String NOTIFICATION_COURSE_NEW = "NOTIFICATION_COURSE_NEW";
	public static final String NOTIFICATION_COURSE_SALE = "NOTIFICATION_COURSE_SALE";
	public static final String NOTIFICATION = "NOTIFICATION";
	private static HashMap<String, String> hashMapConfig = new HashMap<String, String>();

	/**
	 * Đọc file properties và bỏ vào trong map
	 */
	static {
		Properties properties = new Properties();
		try {
			properties.load(new InputStreamReader(
					Thread.currentThread().getContextClassLoader().getResourceAsStream("config.properties"), "UTF-8"));
		} catch (IOException e) {
			e.getMessage();
		}
		Enumeration<String> enumeration = (Enumeration<String>) properties.propertyNames();
		while (enumeration.hasMoreElements()) {
			String key = (String) enumeration.nextElement();
			hashMapConfig.put(key, properties.getProperty(key));
		}
	}

	/**
	 * Hàm đọc từ trong map
	 * 
	 * @param key:
	 *            key cần đọc
	 * 
	 * @return giá trị của key
	 */
	public static String getData(String key) {
		String value = "";
		if (hashMapConfig.containsKey(key)) {
			value = hashMapConfig.get(key);
		}
		return value;
	}
}
